# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2013 Alessandro Camilli
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _


class wizard_hr_holidays_allocation_change(models.TransientModel):
    _name = "wizard.hr.holidays.allocation.change"
    _description = "Wizard to change allocation holiday"
    
    @api.model
    def _default_number_of_days(self):
        rec_id = self._context.get('active_id', False)
        holiday = self.env['hr.holidays'].browse(rec_id)
        return holiday.number_of_days
    
    number_of_days = fields.Float(string='Days', default=_default_number_of_days)

    @api.multi
    def change(self):
        for wiz_obj in self:
            h_ids = self.env.context.get('active_ids')
            for holiday in self.env['hr.holidays'].browse(h_ids):
                holiday.write({'number_of_days_temp' : wiz_obj.number_of_days})
        
        return {'type': 'ir.actions.act_window_close'}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
