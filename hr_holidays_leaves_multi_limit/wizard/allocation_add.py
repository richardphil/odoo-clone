# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2013 Alessandro Camilli
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _


class wizard_hr_holidays_allocation_add(models.TransientModel):
    _name = "wizard.hr.holidays.allocation.add"
    _description = "Wizard to add allocation holiday"
    
    holiday_status_id = fields.Many2one(
        'hr.holidays.status', string="Leave Type", required=True)
    number_of_days = fields.Float(string='Days', required=True)
    
    @api.onchange('holiday_status_id')
    def change_holiday_status_id(self):
        self.number_of_days = self.holiday_status_id.limit_leaves

    @api.multi
    def add(self):
        for wiz_obj in self:
            e_ids = self.env.context.get('active_ids')
            self.pool['hr.employee'].add_allocation_holiday(
                self.env.cr, self.env.uid, e_ids, wiz_obj.holiday_status_id.id, 
                wiz_obj.number_of_days)
            
        return {'type': 'ir.actions.act_window_close'}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
