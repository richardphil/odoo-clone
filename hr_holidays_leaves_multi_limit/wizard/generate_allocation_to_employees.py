# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2013 Alessandro Camilli
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _


class wizard_generate_allocation_employees(models.TransientModel):
    _name = "wizard.generate.allocation.employees"
    _description = "Wizard to generato allocations on employees"
    
    
    generate_option = fields.Selection([
        ('add', "Add allocation only if doesn't exist"),
        ('add_replace', "Add allocation and Replace if exists"),
        ], 'Allocation Option for each employee', required=True)

    @api.multi
    def execute(self):
        for wiz_obj in self:
            h_status_ids = self.env.context.get('active_ids')
            for holiday_status in self.env['hr.holidays.status'].browse(h_status_ids):
                
                domain = [('active', '=', True)]
                for employee in self.env['hr.employee'].search(domain):
                    domain = [('holiday_status_id', '=', holiday_status.id),
                              ('employee_id', '=', employee.id),
                              ('type', '=', 'add')]
                    holiday = self.env['hr.holidays'].search(domain, limit=1)
                    
                    if not holiday:
                        if wiz_obj.generate_option in ['add', 'add_replace'] :
                            self.pool['hr.employee'].add_allocation_holiday(
                                self.env.cr, self.env.uid, [employee.id], 
                                holiday_status.id, 
                                holiday_status.limit_leaves)
                    elif holiday:
                        if wiz_obj.generate_option in ['add_replace'] :
                            holiday.number_of_days_temp = holiday_status.limit_leaves 
        
        return {'type': 'ir.actions.act_window_close'}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
