# -*- coding: utf-8 -*-
#
##############################################################################
#
#    Author(s): Alessandro Camilli (alessandrocamilli@openforce.it)
#               Andrea Colangelo (andreacolangelo@openforce.it)
#
#    Copyright © 2016 Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see:
#    http://www.gnu.org/licenses/agpl-3.0.txt.
#
##############################################################################


from openerp.osv import fields, orm, osv
from openerp.tools.translate import _


class hr_employee(orm.Model):
    _inherit = "hr.employee"
    
    def _get_allocation_ids(self, cr, uid, ids, field_name, arg, context=None):
        result = {}
        for record in self.browse(cr, uid, ids, context=context):
            domain = [('employee_id', '=', record.id), ('type', '=', 'add')]
            h_ids = self.pool['hr.holidays'].search(cr, uid, domain)
            if h_ids:
                if not record.id in result:
                    result[record.id] = []
                for holiday in self.pool['hr.holidays'].browse(cr, uid, h_ids):
                    result[record.id].append(holiday.id)
        return result
    
    _columns = {
        'holidays_allocation_ids': fields.function(
            _get_allocation_ids, type='one2many', relation="hr.holidays", 
            string="Holidays Allocation"),
    }
    
    def add_allocation_holiday(self, cr, uid, ids, 
                               holiday_status_id, 
                               number_of_days, 
                               context=None):
        for employee in self.browse(cr, uid, ids):
            vals = {
                'name': _('Allocation for %s') % employee.name, 
                'employee_id': employee.id, 
                'holiday_status_id': holiday_status_id, 
                'type': 'add', 
                'holiday_type': 'employee', 
                'number_of_days_temp': number_of_days
                }
            holiday_id = self.pool['hr.holidays'].create(cr, uid, vals)
            holiday = self.pool['hr.holidays'].browse(cr, uid, holiday_id)
            for sig in ('confirm', 'validate', 'second_validate'):
                holiday.signal_workflow(sig)
                
            
class hr_holidays(orm.Model):
    _inherit = "hr.holidays"
    
    def remove_allocation_holiday(self, cr, uid, ids, context=None):
        for holiday in self.pool['hr.holidays'].browse(cr, uid, ids):
            for sig in ('refuse', 'reset'):
                holiday.signal_workflow(sig)
        self.pool['hr.holidays'].unlink(cr, uid, ids)
    

class hr_holidays_status(orm.Model):
    _inherit = "hr.holidays.status"
    
    def name_get(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if not context.get('employee_id',False):
            # leave counts is based on employee_id, would be inaccurate if not based on correct employee
            return super(hr_holidays_status, self).name_get(cr, uid, ids, context=context)

        res = []
        for record in self.browse(cr, uid, ids, context=context):
            name = record.name
            if not record.limit:
                name = name + ('  (%g/%g)' % (record.leaves_taken or 0.0, record.max_leaves or 0.0))
            res.append((record.id, name))
        return res
    
    def _user_left_days(self, cr, uid, ids, name, args, context=None):
        employee_id = False
        if context and 'employee_id' in context:
            employee_id = context['employee_id']
        else:
            employee_ids = self.pool.get('hr.employee').search(cr, uid, [('user_id', '=', uid)], context=context)
            if employee_ids:
                employee_id = employee_ids[0]
        if employee_id:
            res = self.get_days(cr, uid, ids, employee_id, context=context)
        else:
            res = dict((res_id, {'leaves_taken': 0, 'remaining_leaves': 0, 'max_leaves': 0}) for res_id in ids)
        return res
    
    
    _columns = {
        'limit_leaves': fields.float("Limit Leaves"),
        'date_from': fields.date('Start Date'),
        'date_to': fields.date('End Date'),
        'max_leaves': fields.function(_user_left_days, string='Maximum Allowed', help='This value is given by the sum of all holidays requests with a positive value.', multi='user_left_days'),
        'leaves_taken': fields.function(_user_left_days, string='Leaves Already Taken', help='This value is given by the sum of all holidays requests with a negative value.', multi='user_left_days'),
        'remaining_leaves': fields.function(_user_left_days, string='Remaining Leaves', help='Maximum Leaves Allowed - Leaves Already Taken', multi='user_left_days'),
        'virtual_remaining_leaves': fields.function(_user_left_days, string='Virtual Remaining Leaves', help='Maximum Leaves Allowed - Leaves Already Taken - Leaves Waiting Approval', multi='user_left_days'),
    }
    
