# -*- coding: utf-8 -*-
#
##############################################################################
#
#    Author(s): Alessandro Camilli (alessandrocamilli@openforce.it)
#               Andrea Colangelo (andreacolangelo@openforce.it)
#
#    Copyright © 2016 Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see:
#    http://www.gnu.org/licenses/agpl-3.0.txt.
#
##############################################################################

{
    'name': 'HR Holidays Leaves Multi Limit',
    'version': '0.1',
    'category': 'Human Resources',
    'description': """
                   It Manage multi limits for more holidays type
                   """,
    'author': 'Openforce di Alessandro Camilli',
    'website': 'http://www.openforce.it',
    'license': 'AGPL-3',
    "depends": ['base', 'hr_holidays', ],
    "data": [
        'wizard/allocation_change_view.xml',
        'wizard/allocation_add_view.xml',
        'wizard/generate_allocation_to_employees_view.xml',
        'views/hr_holidays.xml',
    ],
    "demo": [],
    "active": False,
    "installable": True
}
