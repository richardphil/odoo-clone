# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@yahoo.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, orm
from openerp.tools.translate import _


class res_company(orm.Model):
    
    _inherit = "res.company"
    
    _columns = {
        'rea_office': fields.many2one('res.province', 'Office'),
        'rea_number': fields.char('Number', size=20),
        'rea_capital_social': fields.float('Capital Social'),
        'rea_type_member_company': fields.selection((
                                   ('SU','One member'),
                                   ('SM','More members')),
                                   'Type Member Company'),
        'rea_liquidation_state': fields.selection((
                                   ('LS','In Liquidation'),
                                   ('LN','Not In Liquidation')),
                                   'Liquidation State'),
        }
    
    _defaults = {
        'rea_liquidation_state': 'LN'
        }
    