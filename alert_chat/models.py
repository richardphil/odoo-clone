# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2014
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api
from openerp.tools.translate import _
from datetime import datetime, time, timedelta
import openerp

class alert_chat(models.TransientModel):
    _name = 'alert.chat'
    
    @api.model
    def _prepare_message(self):
        
        return True
    
    @api.one
    def execute(self):
        self._prepare_message()
        # test
        #messages = []
        #messages.append('ecco un messaggio')
        #self.env.context = {'messages': messages}
        
        # Get messages
        domain = [('state', '=', 'to_send')]
        msgs = self.env['alert.chat.message'].search(domain)
        msgs.send()
        
        #self.show_chat()
        return self
    
class alert_chat(models.Model):
    _name = 'alert.chat.message'
    _description = 'Alert Chat Message'
    
    model = fields.Char('Model')
    res_id = fields.Integer('Resource')
    message = fields.Text('Message', required=True)
    type = fields.Char('Type', default="message")
    user_from = fields.Many2one('res.users', 'User From')
    user_to = fields.Many2one('res.users', 'User To', required=True)
    state = fields.Selection([('to_send', 'To Send'), ('sent', 'Sent')], 
                             default='to_send')
    
    @api.multi
    def send(self):
        
        for msg in self:
            context = self.env.context
            # session
            session = self.env["im_chat.session"].session_get( 
                                                      msg.user_to.id, 
                                                      context=context)
            # Post message
            uuid = session.get('uuid')
            message_id = self.env["im_chat.message"].post(self.env.uid, 
                                                          uuid, 
                                                          msg.type, 
                                                          msg.message, 
                                                          context=context)
            # Sign how sent
            if message_id:
                self.state = 'sent'
        
        return self
    