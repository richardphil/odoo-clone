# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import tools
from openerp.osv import fields,osv
import openerp.addons.decimal_precision as dp

class account_treasury_report(osv.osv):
    _name = "account.treasury.report.payments"
    _description = "Treasury Analysis"
    _auto = False

    _columns = {
        'payment_account_id': fields.many2one('account.account', 'Payment Account', readonly=True),
        'payment_debit': fields.float('Payment Debit', readonly=True),
        'payment_credit': fields.float('Payment Credit', readonly=True),
        'payment_period_id': fields.many2one('account.period', 'Period', readonly=True),
        'payment_fiscalyear_id': fields.many2one('account.fiscalyear', 'Fiscalyear', readonly=True),
        'payment_date': fields.date('Date Payment', readonly=True),
        'payment_month': fields.integer('Month of Payment', readonly=True),
        'payment_year': fields.integer('Year of Payment', readonly=True),
        'partner_id': fields.many2one('res.partner', 'Partner', readonly=True),
        'company_id': fields.many2one('res.company', 'Company', readonly=True),
        'type': fields.selection([
            ('view', 'View'),
            ('other', 'Regular'),
            ('receivable', 'Receivable'),
            ('payable', 'Payable'),
            ('liquidity','Liquidity'),
            ('consolidation', 'Consolidation'),
            ('closed', 'Closed'),
        ], 'Internal Type', readonly=True, help="The 'Internal Type' is used for features available on "\
            "different types of accounts: view can not have journal items, consolidation are accounts that "\
            "can have children accounts for multi-company consolidations, payable/receivable are for "\
            "partners accounts (for debit/credit computations), closed for depreciated accounts."),
        'competence_account_id': fields.many2one('account.account', 'Competence Account', readonly=True),
        'competence_name': fields.char('Name', readonly=True),
        'competence_debit': fields.float('Amount Debit', readonly=True),
        'competence_credit': fields.float('Amount Credit', readonly=True),
        'amount': fields.float('Amount', readonly=True)
        # 'ending_balance': fields.function(_compute_balances, digits_compute=dp.get_precision('Account'), string='Ending Balance', multi='balance'),
    }

    _order = 'payment_date asc'


    def init(self, cr):
        tools.drop_view_if_exists(cr, 'account_treasury_report_payments')
        cr.execute("""
            create or replace view account_treasury_report_payments as (
            SELECT 
            l.id,
            l.account_id as payment_account_id,
            sum(l.debit) as payment_debit,
            sum(l.credit) as payment_credit,
            p_p.id as payment_period_id,
            p_p.fiscalyear_id as payment_fiscalyear_id,
            p_m.date as payment_date,
            Extract(month from p_m.date) as payment_month,
            Extract(year from p_m.date) as payment_year,
            p_m.partner_id as partner_id,
            p_m.company_id as company_id,
            c_a.type as type,
            max(inv.amount_untaxed) as invoice_amount_untaxed,
            max(inv.amount_total) as invoice_amount_total,
            c_line.account_id as competence_account_id,
            c_line.name as competence_name,
            sum(inv_tax.amount) as amount_vat,
            sum(c_line.debit) as competence_debit,
            sum(c_line.credit) as competence_credit,
            sum(
            CASE WHEN (c_a.type = 'payable' AND c_line.debit > 0 AND inv.amount_total > 0) 
                THEN round(-1 * c_line.debit * (l.credit/inv.amount_total), 2)
                 WHEN (c_a.type = 'payable' AND c_line.credit > 0 AND inv.amount_total > 0) 
                THEN round(c_line.credit * (l.debit/inv.amount_total), 2)
                 WHEN (c_a.type = 'receivable' AND c_line.credit > 0 AND inv.amount_total > 0) 
                THEN round(c_line.credit * (l.debit/inv.amount_total), 2)
                 WHEN (c_a.type = 'receivable' AND c_line.debit > 0 AND inv.amount_total > 0) 
                THEN round(-1 * c_line.debit * (l.credit/inv.amount_total), 2)
                    ELSE 
                    0
                   END
                   ) as amount
            
            FROM account_move_line l
            left join account_account a on (l.account_id = a.id)
            left join account_move_line p_ml on (l.move_id = p_ml.move_id)
            left join account_move p_m on (p_m.id=l.move_id)
            left join account_period p_p on (p_m.period_id=p_p.id)
            
            left join account_move_line c_ml on (p_ml.reconcile_ref = c_ml.reconcile_ref AND p_ml.reconcile_ref != '' AND p_ml.id != c_ml.id)
            left join account_account c_a on (c_ml.account_id = c_a.id)
            
            left join account_move_line c_line on (c_line.move_id = c_ml.move_id)
            left join account_account c_line_a on (c_line.account_id = c_line_a.id)
            left join account_invoice inv on (c_line.move_id=inv.move_id)
            left join account_invoice_tax inv_tax on ( inv.id = inv_tax.invoice_id AND inv_tax.account_id = c_line.account_id) /* per eliminare righe iva*/
            
            WHERE
            a.type = 'liquidity'
            AND c_a.type in ('receivable', 'payable')
            AND c_line_a.type not in ('receivable', 'payable')
            AND c_line.account_id != l.account_id 
            /*AND inv_tax.id is null*/
            group by l.id, l.account_id, p_p.id, p_p.fiscalyear_id, p_m.date, Extract(month from p_m.date), Extract(year from p_m.date), p_m.partner_id, p_m.company_id, c_a.type, c_line.account_id, c_line.name, c_line_a.code
            order by c_line_a.code
            )
        """)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
