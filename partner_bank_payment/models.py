# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@yahoo.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _
from openerp.tools.translate import _


class res_partner(models.Model):
    _inherit = 'res.partner'
    
    bank_payment_customer = fields.Many2one(
        'res.partner.bank', string='Bank Payment Customer', 
        domain="[('company_id', '=', 1)]")
    bank_payment_supplier = fields.Many2one(
        'res.partner.bank', string='Bank Payment Supplier', 
        domain="[('company_id', '=', 1)]")


class sale_order(models.Model):
    _inherit = 'sale.order'
    
    @api.v7
    def _make_invoice(self, cr, uid, order, lines, context=None):
        inv_id = super(sale_order, self)._make_invoice(cr, uid, 
                                                       order, 
                                                       lines, 
                                                       context=None)
        bank_id = False
        if inv_id:
            invoice = self.pool.get('account.invoice').browse(cr, uid, inv_id)
            if invoice.type in ('out_invoice', 'out_refund'):
                if invoice.partner_id.bank_payment_customer:
                    bank_id = invoice.partner_id.bank_payment_customer.id
            else:
                if invoice.partner_id.bank_payment_supplier:
                    bank_id = invoice.partner_id.bank_payment_supplier.id
        if bank_id:
            val = {
                'partner_bank_id': bank_id
            }
            self.pool.get('account.invoice').write(cr, uid, [inv_id], val)
        return inv_id  


class account_invoice(models.Model):
    _inherit = 'account.invoice'
    
    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice=False,
            payment_term=False, partner_bank_id=False, company_id=False, context=None):
        
        res = super(account_invoice, self).onchange_partner_id(type,\
                                partner_id, date_invoice=date_invoice, \
                                payment_term=payment_term, \
                                partner_bank_id=partner_bank_id, \
                                company_id=company_id)
        if partner_id:
            partner = self.env['res.partner'].browse(partner_id)
            if type in ('out_invoice', 'out_refund'):
                if partner.bank_payment_customer:
                    self.partner_bank_id  = partner.bank_payment_customer.id
            else:
                if partner.bank_payment_supplier:
                    self.partner_bank_id  = partner.bank_payment_supplier.id
        return res
            