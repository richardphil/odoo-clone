# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2014
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, orm
from openerp.tools.translate import _
from datetime import datetime, time, timedelta
from openerp import netsvc

class product_template(orm.Model):
    _inherit="product.template"
    
    _columns = {
        'product_option': fields.boolean(
            'Option Product',
            help="Option Product"
        ),
    }
    

class sale_order_line(orm.Model):
    _inherit="sale.order.line"
    
    _columns = {
        'product_option': fields.boolean(
            'Option Product',
            help="Option Product"),
        'product_option_ref_to_id': fields.many2one('product.product',
            'Product Ref', domain="[('product_option','=',False)]",
            help="Product ref to link at this product"
        ),
    }
    
    def product_id_change(self, cr, uid, ids, pricelist, product_id, qty=0,
            uom=False, qty_uos=0, uos=False, name='', partner_id=False,
            lang=False, update_tax=True, date_order=False, packaging=False, 
            fiscal_position=False, flag=False, context=None):
        product_obj = self.pool['product.product']
        user_obj = self.pool['res.users']
        res = super(sale_order_line, self).product_id_change(cr, uid, 
                ids, pricelist, product_id, qty, uom, qty_uos, 
                uos, name, partner_id, lang, update_tax, date_order, 
                packaging, fiscal_position, flag, context)
        
        if product_id:
            product = product_obj.browse(cr, uid, product_id)
            user = user_obj.browse(cr, uid, uid, context)
            company = user.company_id
            res['value'].update({'product_option': 
                                  product.product_option})
        return res