# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2013 Alessandro Camilli
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, orm
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp


class res_partner(orm.Model):
    
    _inherit = 'res.partner'
    
    _columns = {
        'person_type' : fields.selection([
            ('legal','Legal'),
            ('individual','Individual')], 'Person Type'),
        'person_name': fields.char('Name', size=64),
        'person_surname': fields.char('Surname', size=64),
        'person_date_of_birth': fields.date('Date of birth'),
        'person_city_of_birth': fields.char('City of birth', size=64),
        'person_country_of_birth': fields.many2one('res.country', string='Country'),
        'person_state_of_birth': fields.many2one('res.country.state', string='Province'),
        'person_profession': fields.char('Profession'),
        'person_gender' : fields.selection([
            ('male','Male'),
            ('female','Female')], 'Gender'),
        'person_marital_status' : fields.selection([
            ('single','Single'),
            ('married','Married'),
            ('widower','Widower/Widow'),
            ('divorced','Divorced')
            ], 'Marital Status'),
    }
    
    def on_change_person_city(self, cr, uid, ids, city):
        better_zip_obj = self.pool['res.better.zip']
        res = {'value': {}}
        if city:
            domain.append(('name', '=', city))
            zip_ids = better_zip_obj.search(cr, uid, domain)
            if len(zip_ids) > 0:
                bzip = better_zip_obj.browse(cr, uid, zip_ids[0])
                res = {'value': {
                                 'person_country_of_birth' : bzip.country_id.id \
                                    if bzip.country_id else False,
                                'person_city_of_birth' : bzip.city,
                                'person_state_of_birth' : bzip.state_id.id \
                                    if bzip.state_id else False
                                }
                        }
                
                #bzip = zip_ids[0]
                address['zip'] = bzip.name  
                address['city'] = bzip.city  
                address['country_id'] = bzip.country_id.id \
                    if bzip.country_id else False
                address['country_state_id'] = bzip.state_id.id \
                    if bzip.state_id else False
        return res