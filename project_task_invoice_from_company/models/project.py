# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _


class project_task(models.Model):
    _inherit = 'project.task'
    
    @api.model
    def _default_company(self):
        company_id = self._context.get('company_id',
                                       self.env.user.company_id)
        partner_id = self.env.user.company_id.partner_id.id
        domain = [('partner_id', '=', partner_id)]
        ci = self.env['project.company.invoicing'].search(domain, limit=1) 
        if ci :
            return ci.id
        else:
            return False
    
    company_invoice_id = fields.Many2one("project.company.invoicing", 
                                         string="Company for invoice",
                                         default=_default_company)
    

class project_company_invoicing(models.Model):
    _name = 'project.company.invoicing'
    
    name = fields.Char(string="Name")
    partner_id = fields.Many2one("res.partner", string="Partner")
    
