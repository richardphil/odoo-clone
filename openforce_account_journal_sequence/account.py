# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2013 Alessandro Camilli
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api
from openerp.tools.translate import _


class account_journal(models.Model):
    _inherit = "account.journal"
    
    selection_sequence = fields.Integer(
        'Selection sequence', help="Order to choose the default journal", 
        default=5)
    _order = 'selection_sequence, code'


class account_fiscal_position(models.Model):
    _inherit = "account.fiscal.position"
    
    sale_invoice_journal_id = fields.Many2one(
        'account.journal', string='Default Journal Sale Invoice')
    purchase_invoice_journal_id = fields.Many2one(
        'account.journal', string='Default Journal Purchase Invoice')


class account_invoice(models.Model):
    '''
    Select default journal second the right sequence
    '''
    _inherit = "account.invoice"
    
    def _get_journal(self):
        journal_id = False
        type_inv = self.env.context.get('type', 'out_invoice')
        user = self.env.user
        company_id = user.company_id.id
        type2journal = {
                        'out_invoice': 'sale', 
                        'in_invoice': 'purchase', 
                        'out_refund': 'sale_refund', 
                        'in_refund': 'purchase_refund'
        }
        journal_obj = self.env['account.journal']
        domain = [('type', '=', type2journal.get(type_inv, 'sale')), 
                  ('company_id', '=', company_id)]
        res = journal_obj.search(domain, order='selection_sequence', 
                                 limit=1).id
        return res or False
    
    @api.onchange('fiscal_position')
    def onchange_fiscal_position_id(self):
        result = {}
        default = {}
        fiscal_position_obj = self.env['account.fiscal.position']
        journal_id = False
        if self.fiscal_position:
            if self.env.context.get('type') in ['out_invoice', 'out_refund'] \
                and self.fiscal_position.purchase_invoice_journal_id:
                journal_id = self.fiscal_position.purchase_invoice_journal_id.id
            elif self.env.context.get('type') in ['in_invoice', 'in_refund'] \
                and self.fiscal_position.sale_invoice_journal_id:
                journal_id = self.fiscal_position.sale_invoice_journal_id.id
        # default with second
        if not journal_id:
            journal_id = self._get_journal()
        if journal_id:
            self.journal_id  = journal_id
    
