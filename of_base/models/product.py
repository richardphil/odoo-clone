# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@yahoo.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from openerp.osv import fields, orm

class product_template(orm.Model):
    _inherit = "product.template"

    def default_get(self, cr, uid, fields, context=None):
        config_obj = self.pool['openforce.config.settings']
        
        res = super(product_template, self).default_get(cr, uid, fields, context=context)
        if not res:
            res = {}
        config = config_obj.get_default_product(cr, uid, fields, context=context)
        if config['of_product_default_uom_id']:
            res.update({'uom_id':  config['of_product_default_uom_id']})
        
        return res

class product_packaging(orm.Model):
    _inherit = "product.packaging"

    def default_get(self, cr, uid, fields, context=None):
        config_obj = self.pool['openforce.config.settings']
        
        res = super(product_packaging, self).default_get(cr, uid, fields, context=context)
        if not res:
            res = {}
        config = config_obj.get_default_product(cr, uid, fields, context=context)
        if config['of_product_default_ul_id']:
            res.update({'ul':  config['of_product_default_ul_id']})
        if config['of_product_default_ul_container_id']:
            res.update({'ul_container':  config['of_product_default_ul_container_id']})
        
        return res