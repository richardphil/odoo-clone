# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@yahoo.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, orm

class res_company(orm.Model):
    
    _inherit = "res.company"
    
    _columns = {
        'of_partner_default_company': fields.boolean('Default Company', 
                    help="For new partner, set how company"),
        'of_partner_ctrl_vat_unique': fields.boolean('Vat unique', 
                    help="Not more partners with the same vat code"),
        'of_partner_ctrl_fiscalcode_unique': fields.boolean('Fiscalcode \
                    unique', 
                    help="Not more partners with the same Fiscalcode"),
        'of_product_default_uom_id': fields.many2one('product.uom',
                        'Default Product Uom'),
        'of_product_default_ul_id': fields.many2one('product.ul',
                        'Default Product ul'),
        'of_product_default_ul_container_id': fields.many2one('product.ul',
                        'Default Product ul container'),
        }
    