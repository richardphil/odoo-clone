# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@yahoo.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from openerp.osv import fields, orm

class res_partner(orm.Model):
    _inherit = "res.partner"

    def default_get(self, cr, uid, fields, context=None):
        
        res = super(res_partner, self).default_get(cr, uid, fields, context=context)
        
        of_config_obj = self.pool['openforce.config.settings']
        partner_config = of_config_obj.get_default_partner( 
                                                cr, uid, fields, 
                                                context=None)
        
        res.update({'is_company':  
                    partner_config.get('of_partner_default_company', False)})
        
        return res