# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2014
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, orm, osv
from openerp.tools.translate import _

class openfoce_config_setting(osv.osv_memory):
    
    _name = "openforce.config.settings"
    _inherit = 'res.config.settings'
    
    _columns = {
        'company_id': fields.many2one('res.company', 'Company', 
                                      required=True),
        'has_default_company': fields.boolean('Has default company', 
                                              readonly=True),
        
        'of_partner_default_company': fields.boolean('Default Company', 
                    help="For new partner, set how company"),
                
        'of_partner_ctrl_vat_unique': fields.boolean('Vat unique', 
                    help="Not more partners with the same vat code"),
        'of_partner_ctrl_fiscalcode_unique': fields.boolean('Fiscalcode \
                        unique', 
                    help="Not more partners with the same Fiscalcode"),
        'of_product_default_uom_id': fields.many2one('product.uom',
                        'Default Product Uom'),
        'of_product_default_ul_id': fields.many2one('product.ul',
                        'Default Product Unit Logistic'),
        'of_product_default_ul_container_id': fields.many2one('product.ul',
                        'Default Product Unit Logistic container'),
        }
    
    def _default_company(self, cr, uid, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, 
                                                 context=context)
        return user.company_id.id

    def _default_has_default_company(self, cr, uid, context=None):
        count = self.pool.get('res.company').search_count(cr, uid, [], 
                                                          context=context)
        return bool(count == 1)
    
    _defaults = {
        'company_id': _default_company,
        'has_default_company': _default_has_default_company,
    }
    
    
    def onchange_company_id(self, cr, uid, ids, company_id, context=None):
        # update related fields
        values = {}
        
        return values
    
    def get_default_partner(self, cr, uid, fields, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, 
                                                 context=context)
        return {
            'of_partner_default_company': 
                    user.company_id.of_partner_default_company,
            'of_partner_ctrl_vat_unique': 
                    user.company_id.of_partner_ctrl_vat_unique,
            'of_partner_ctrl_fiscalcode_unique': 
                    user.company_id.of_partner_ctrl_fiscalcode_unique,
        }
    def set_default_partner(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        user.company_id.write({
            'of_partner_default_company': 
                    config.of_partner_default_company,
            'of_partner_ctrl_vat_unique': 
                    config.of_partner_ctrl_vat_unique,
            'of_partner_ctrl_fiscalcode_unique': 
                    config.of_partner_ctrl_fiscalcode_unique,
        })
        
    def get_default_product(self, cr, uid, fields, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, 
                                                 context=context)
        return {
            'of_product_default_uom_id': 
                    user.company_id.of_product_default_uom_id.id,
            'of_product_default_ul_id': 
                    user.company_id.of_product_default_ul_id.id,
            'of_product_default_ul_container_id': 
                    user.company_id.of_product_default_ul_container_id.id,
        }
    def set_default_product(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        user.company_id.write({
            'of_product_default_uom_id': 
                    config.of_product_default_uom_id.id,
            'of_product_default_ul_id': 
                    config.of_product_default_ul_id.id,
            'of_product_default_ul_container_id': 
                    config.of_product_default_ul_container_id.id,
        })
