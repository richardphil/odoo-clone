# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields
from openerp.addons.connector.unit.mapper import mapping
from openerp.addons.magentoerpconnect.product import ProductProductAdapter
from .backend import magento_openforce


@magento_openforce
class OpenforceProductProductAdapter(ProductProductAdapter):
    _model_name = 'magento.product.product'
    
    def read(self, id, storeview_id=None, attributes=None):
        """ Returns the information of a record

        :rtype: dict
        """
        res = super(OpenforceProductProductAdapter, self).read(\
            id, storeview_id=None, attributes=None)
        # Call Api to get configurable product ( see key 'dropdata')
        magento_product_configurable_id = self._call(
            'simpleconfigrelation_api.getrelationship', 
            [int(id)])["dropdata"][0]
        if product_configurable_id:
            res['product_configurable_id'] = magento_product_configurable_id
        
        return res
        '''
        return self._call('ol_catalog_product.info',
                          [int(id), storeview_id, attributes, 'id'])'''