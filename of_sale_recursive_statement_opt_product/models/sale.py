# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2014
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, orm
from openerp.tools.translate import _
from datetime import datetime, time, timedelta


class sale_order_line(orm.Model):
    _inherit="sale.order.line"
    
    _columns = {
        'chained_to_recursive_product_id': fields.many2one(
            'sale.recursive.statement.product',
            string='Chained To/Variation Of',
            help="Product ref to link at this product"
        ),
    }
    def chained_to_recursive_product_id_change(self, cr, uid, ids,
                                               chained_to_recursive_product_id, 
                                               context=None):
        '''
        Set same frequency of parent
        '''
        res = {}
        vals = {}
        res['value'] = vals
        if chained_to_recursive_product_id:
            rp = self.pool['sale.recursive.statement.product'].browse(
                cr, uid, chained_to_recursive_product_id)
            val = {
                'recursive_statement_frequency' : rp.frequency,
                'recursive_statement_frequency_uom' : rp.frequency_uom.id
            }
            res['value'] = val
        
        return res
    
    def _prepare_recursive_statement_product(self, cr, uid, line, context=None):
        '''
        Setting parent_id form chained_to_recursive_product_id and product_ref
        of option
        '''
        res = super(sale_order_line, self).\
            _prepare_recursive_statement_product(cr, uid, line, context)
        res.update({'parent_id' : 
                    line.chained_to_recursive_product_id.id or False})
        # Only product ref search product ref in the same order
        if not line.chained_to_recursive_product_id \
            and line.product_option_ref_to_id:
            domain = [('product_id', '=', line.product_option_ref_to_id.id),
                      ('order_id', '=', line.order_id.id)]
            parent_line_ids = self.search(cr, uid, domain)
            if parent_line_ids:
                # Search where is it in recursive products
                domain = [('line_order_id', '=', parent_line_ids[0])]
                p_rec_ids = self.pool['sale.recursive.statement.product'].\
                    search(cr, uid, domain)
                if p_rec_ids:
                    res.update({'parent_id' : p_rec_ids[0]})
        
        return res
    
    