# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2013 Alessandro Camilli
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import logging

from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
from openerp.tools import email_split
from openerp import SUPERUSER_ID

_logger = logging.getLogger(__name__)

WELCOME_EMAIL_SUBJECT = _("Your Odoo account at %(company)s")
WELCOME_EMAIL_BODY = _("""Dear %(name)s,

You have been given access to %(company)s's %(portal)s.

Your login account data is:
  Username: %(login)s
  Portal: %(portal_url)s
  Database: %(db)s 

You can set or change your password via the following url:
   %(signup_url)s

%(welcome_message)s

--
Odoo - Open Source Business Applications
http://www.openerp.com
""")


class wizard_user(orm.TransientModel):
    """
        A model to configure users in the portal wizard.
    """
    _inherit = 'portal.wizard.user'
    
    
    def _get_email_template(self, cr, uid, ids, context=None):
        '''
        Extend this method for change default email template
        '''
        ir_model_data = self.pool.get('ir.model.data')
        template_id = False
        try:
            template_id = ir_model_data.get_object_reference(cr, uid, 
                                    'portal_access_email_template', 
                                    'of_portal_access_email_template')[1]
        except ValueError:
            template_id = False
        return template_id

    def _send_email(self, cr, uid, wizard_user, context=None):
        """ send notification email to a new portal user
            @param wizard_user: browse record of model portal.wizard.user
            @return: the id of the created mail.mail record
        """
        res_partner = self.pool['res.partner']
        this_context = context
        this_user = self.pool.get('res.users').browse(cr, SUPERUSER_ID, uid, context)
        if not this_user.email:
            raise osv.except_osv(_('Email Required'),
                _('You must have an email address in your User Preferences to send emails.'))

        # determine subject and body in the portal user's language
        user = self._retrieve_user(cr, SUPERUSER_ID, wizard_user, context)
        context = dict(this_context or {}, lang=user.lang)
        ctx_portal_url = dict(context, signup_force_type_in_url='')
        portal_url = res_partner._get_signup_url_for_action(cr, uid,
                                                            [user.partner_id.id],
                                                            context=ctx_portal_url)[user.partner_id.id]
        res_partner.signup_prepare(cr, uid, [user.partner_id.id], context=context)

        data = {
            'company': this_user.company_id.name,
            'portal': wizard_user.wizard_id.portal_id.name,
            'welcome_message': wizard_user.wizard_id.welcome_message or "",
            'db': cr.dbname,
            'name': user.name,
            'login': user.login,
            'signup_url': user.signup_url,
            'portal_url': portal_url,
        }
        # E-mail template
        template_id = self._get_email_template(cr, uid, [], context)
        if template_id:
            template = self.pool['email.template'].browse(cr, uid, template_id)
            mail_subject = template.subject % data
            mail_body = template.body_html % data
        else:
            mail_subject = _(WELCOME_EMAIL_SUBJECT) % data
            mail_body = _(WELCOME_EMAIL_BODY) % data
        
        mail_mail = self.pool.get('mail.mail')
        mail_values = {
            'email_from': this_user.email,
            'email_to': user.email,
            #'subject': _(WELCOME_EMAIL_SUBJECT) % data,
            'subject': mail_subject,
            #'body_html': '<pre>%s</pre>' % (_(WELCOME_EMAIL_BODY) % data),
            'body_html': '<pre>%s</pre>' % (mail_body),
            'state': 'outgoing',
            'type': 'email',
        }
        mail_id = mail_mail.create(cr, uid, mail_values, context=this_context)
        return mail_mail.send(cr, uid, [mail_id], context=this_context)
    
    