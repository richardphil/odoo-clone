# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 Alessandro Camilli
#    (<http://www.openforce.it>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api
from openerp.tools.translate import _
from openerp import netsvc
#from scipy.weave.converters import default
class stock_move(models.Model):
    _inherit = "stock.move"
    trip_date_start = fields.Date(string='Date Start',
                               store=True,
                               related='picking_id.trip_date_start')
    trip_date_end = fields.Date(string='Date End',
                               store=True,
                               related='picking_id.trip_date_end')
    trip_state = fields.Selection(string='Trip Status',
                               store=True,
                               related='picking_id.trip_state')
    trip_origin_country_id = fields.Many2one(string="Origin Country",
                                             store=True,
                                             related='picking_id.trip_origin_country_id')
    size_code_id = fields.Many2one(string="Size", store=True,
                                   related='product_id.product_tmpl_id.size_code_id')
    icing = fields.Integer(string="Icing", store=True,
                           related="product_id.product_tmpl_id.icing")
    price_subtotal = fields.Float(string="Total price", compute="_price_subtotal")
    trip_container_number = fields.Char(string="Container Nr", store=True,
                                        related="picking_id.trip_container_number")
    trip_broker_id = fields.Many2one(string="Broker", store=True,
                                     related="picking_id.trip_broker_id")
    trip_arrival_port_id = fields.Many2one(string="Arrival port", store=True,
                                           related="picking_id.trip_arrival_port_id")

    @api.depends('price_unit', 'product_uom_qty')
    def _price_subtotal(self):
        for record in self:
            record.price_subtotal = record.price_unit * record.product_uom_qty


class stock_picking(models.Model):
    _inherit = "stock.picking"

    trip_date_start = fields.Date('Date Start', readonly=True, states={'draft':[('readonly',False)]})
    trip_date_end = fields.Date('Date End', readonly=True, states={'draft':[('readonly',False)]})
    trip_state = fields.Selection([
            ('none', 'None'),
            ('travel', 'Travel'),
            ('arrived', 'Arrived'),
            ], 'Trip Status', default='none',
                        select=True,
                        track_visibility='onchange')

    trip_start_port_id = fields.Many2one('stock.trip.port', 'Start Port')
    trip_arrival_port_id = fields.Many2one('stock.trip.port', 'Arrival Port ')

    trip_container_number = fields.Char('Container Nr')
    trip_packaging = fields.Many2one('product.packaging', 'Packaging')
    trip_origin_country_id = fields.Many2one('res.country', 'Origin Country ')

    trip_shipping_company_id = fields.Many2one('res.partner', 'Shipping Company')
    trip_forwarding_agency_id = fields.Many2one('res.partner', 'Forwarding Agency')
    trip_broker_id = fields.Many2one('res.partner', 'Broker')


    @api.cr_uid_ids_context
    def do_transfer(self, cr, uid, picking_ids, context=None):
        '''
        If in travel, it records the arrival. The trip datas will be
        copied in case of partial transfer in the new backorder.
        '''
        for picking in self.browse(cr, uid, picking_ids, context=context):
            if picking.trip_state == 'travel' \
                    and picking.trip_date_end > fields.Date.today():
                picking.with_context(trip_date_end = fields.Date.today())\
                    .trip_stop()

        return super(stock_picking, self).do_transfer(cr, uid, picking_ids, context)

    @api.one
    def trip_start(self):
        '''
        Add document in all contracts or in one contract the ref is
        in the context
        '''
        contract_id = self.env.context.get('contract_id', False)
        for picking in self:
            ## Info from wizard
            trip_date_start = self.env.context.get('trip_date_start')
            trip_date_end = self.env.context.get('trip_date_end')

            picking.trip_date_start = trip_date_start
            picking.trip_date_end = trip_date_end

            self.trip_state = 'travel'

    @api.one
    def trip_stop(self):
        '''
        Add document in all contracts or in one contract the ref is
        in the context
        '''
        contract_id = self.env.context.get('contract_id', False)
        for picking in self:
            ## Info from wizard
            trip_date_end = self.env.context.get('trip_date_end')

            picking.trip_date_end = trip_date_end

            self.trip_state = 'arrived'

class stock_trip_port(models.Model):
    _name = "stock.trip.port"

    name = fields.Char('Port')
