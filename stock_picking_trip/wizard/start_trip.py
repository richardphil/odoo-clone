# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2014
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import orm, fields
from openerp.tools.translate import _
from datetime import datetime, time, timedelta

class wizard_start_trip(orm.TransientModel):
    
    _name = "wizard.start.trip"
    
    _columns = {
        'trip_date_start': fields.date("Date Start", required=True),
        'trip_date_end': fields.date("On arrival", required=True),
    }
    
    _defaults = {
        'trip_date_start' : lambda *a:(datetime.now()).strftime('%Y-%m-%d'),
    }

    def start_trip(self, cr, uid, ids, context=None):
        picking_obj = self.pool['stock.picking']
        picking_ids = context.get('active_ids')
        # Wizard data
        wiz = self.browse(cr, uid, ids[0])
        context = {
            'trip_date_start' : wiz.trip_date_start,
            'trip_date_end' : wiz.trip_date_end
            }
        picking_obj.trip_start(cr, uid, picking_ids, context)
        
        return {
            'type': 'ir.actions.act_window_close',
        }

