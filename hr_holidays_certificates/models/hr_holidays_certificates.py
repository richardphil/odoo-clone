# -*- coding: utf-8 -*-
#
##############################################################################
#
#    Author(s): Alessandro Camilli (alessandrocamilli@openforce.it)
#               Andrea Colangelo (andreacolangelo@openforce.it)
#
#    Copyright © 2016 Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see:
#    http://www.gnu.org/licenses/agpl-3.0.txt.
#
##############################################################################

from openerp.osv import fields, orm, osv


class HRCertificateNeeded(osv.osv):
    _inherit = "hr.holidays.status"

    _columns = {
        'certificate_needed': fields.boolean("Ask certificate number",
                                             help="If you select this "
                                                  "checkbox, you will be "
                                                  "asked for a certificate "
                                                  "number")
    }


class HRCertificateNumber(osv.osv):
    _inherit = "hr.holidays"

    _columns = {
        'certificate_number': fields.char("Certificate number", size=30),
        'certificate': fields.binary("Certificate"),
        'certificate_needed': fields.related('holiday_status_id',
                                             'certificate_needed',
                                             type='boolean',
                                             relation='hr.holidays.status',
                                             string="Mandatory certificate "
                                                    "Number",
                                             store=True),
    }

    def holiday_status_id_change(self, cr, uid, ids, holiday_status_id,
                                 context=None):
        if holiday_status_id:
            certificate_needed = self.pool.get("hr.holidays.status") \
                .browse(cr, uid, holiday_status_id) \
                .certificate_needed
            res = {'value': {"certificate_needed": certificate_needed, }}
            return res
