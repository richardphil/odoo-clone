# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _


class project_project(models.Model):
    _inherit = 'project.project'
    
    issue_user_id = fields.Many2one('res.users', string='Issue Assigned to')
    
    
class project_issue(models.Model):
    _inherit = 'project.issue'
    
    @api.model
    def _default_user_id(self):
        user_id = False
        # Employee for issue
        domain = [('issue_user_default', '=', True), 
                  ('user_id', '!=', False)]
        emp = self.env['hr.employee'].search(domain, order='id desc', limit=1)
        if emp.user_id:
            user_id = emp.user_id.id
        # Default User
        if not user_id:
            user_id = self.env.user.id
        return user_id
    
    user_id = fields.Many2one('res.users', string='Assigned to', 
                              default=_default_user_id)
    
    @api.v7
    def on_change_project(self, cr, uid, ids, project_id, context=None):
        res = super(project_issue, self).on_change_project(cr, uid, ids,
                                                           project_id, 
                                                           context=None)
        if project_id:
            project = self.pool.get('project.project').browse(cr, uid, 
                                                              project_id, 
                                                              context=context)
            if project and project.issue_user_id:
                res['value']['user_id'] = project.issue_user_id.id
            else:
                res['value']['user_id'] = self._default_user_id(cr, uid, 
                                                                context)
        return res
