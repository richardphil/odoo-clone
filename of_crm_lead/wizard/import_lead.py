# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2013 Alessandro Camilli
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from lxml import etree

from openerp.osv import fields, osv
from openerp.osv.orm import setup_modifiers
from openerp.tools.translate import _

class wizard_of_import_lead(osv.osv_memory):
    _name = "wizard.of.import.lead"
    _description = "Wizard to import Leads"

    _columns={
        'file_to_import': fields.binary('File CSV to import', 
                                            required=True),
        'field_separator_csv': fields.char('Field separator for csv'),
        'user_id': fields.many2one('res.users', 'User competence'),
        'referred': fields.char('Referred'),
        'description': fields.text('Description'),
    }

    def import_lead(self, cr, uid, ids, data, context=None):
        
        for wiz_obj in self.read(cr,uid,ids):
            if 'form' not in data:
                data['form'] = {}
            #data['form']['type'] = wiz_obj['type']
            data['form']['file_to_import'] = wiz_obj['file_to_import']
            data['form']['field_separator_csv'] = wiz_obj['field_separator_csv']
            data['form']['user_id'] = False
            if wiz_obj['user_id']:
                data['form']['user_id'] = wiz_obj['user_id'][0]
            data['form']['description'] = wiz_obj['description']
            data['form']['referred'] = wiz_obj['referred']
        
            self.pool.get('crm.lead').import_from_csv(cr, uid, ids, data, context=None)

        return {'type': 'ir.actions.act_window_close'}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
