# -*- coding: utf-8 -*-
#################################################################################
#    Author: Alessandro Camilli a.camilli@yahoo.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, orm
from openerp.tools.translate import _
import time
import base64
import csv
import os

class crm_lead(orm.Model):
    
    _inherit = 'crm.lead'
    _columns = {
        'website': fields.char('Website'),
    }
    
    def _import_prepare_row(self, cr, uid, ids, data, context=None):
        '''
        To extend for redifine row values 
        '''
        return data
    
    def _import_csv_prepare_column_name(self, cr, uid, row_col, context=None):
        '''
        To extend for redifine columns name
        '''
        
        return row_col
    
    def import_from_csv(self, cr, uid, ids, data, context=None):
        '''
        convenzioni:
        le intestaizione dii colonne devono essere le stesse dei campi di OE
        
        COLONNA no_import:
        se non è vuota, non importa l'elemento. Ci si scrive la motivazione per cui il record
        non va importato
        '''
        lead_obj = self.pool['crm.lead']
        better_zip_obj = self.pool['res.better.zip']
        
        file_to_import = base64.decodestring(data['form']['file_to_import'])
        
        if os.path.exists("of-addons/of_crm_lead/"):
            #path = "of-addons/of_crm_lead/"
            path = "/opt/odoo/of-addons/of_crm_lead/"
        else:
            path = "/opt/odoo/of-addons/of_crm_lead/"
        
        #f = open("openforce-addons/openforce_utility/partner/file_to_import.csv", "w")
        f = open( path + "file_to_import.csv", "w")
        f.write(file_to_import)
        f.close()
        #
        # Setup Cols
        #
        iFile = open( path + "file_to_import.csv", "rb")
        if data['form']['field_separator_csv'] \
                and data['form']['field_separator_csv'][:1] == ';':
            reader = csv.reader(iFile, delimiter=';')
        else:
            reader = csv.reader(iFile)
        
        partner = {}
        partner2 = {}
        partners = []
        rownum = 0
        
        # Set columns
        for row in reader:
            if rownum == 0:
                row = self._import_csv_prepare_column_name(cr, uid, row)
                header = row
            else:
                colnum = 0
                partner2 = partner.copy()
                for col in row:
                    idPartner = rownum -1
                    field = header[colnum]
                    partner2[field] = col
                    colnum += 1
                partners.append(partner2)
            rownum += 1
        
        for row in partners:
            # Non importare
            if 'no_import' in row and row['no_import']:
                continue
            if not 'name' in row:
                continue
            
            if 'name2' in row and row['name2']:
                row['name'] += row['name2']
            print row['name']
            
            setup_user_id = data['form']['user_id']
            setup_description = data['form']['description']
            setup_referred = data['form']['referred']
            
            
            # city
            domain = [('city', '=', row['city'])]
            if 'zip' in row and row['zip']:
                domain.append(('name', '=', row['zip']))
            zip_ids = better_zip_obj.search(cr, uid, domain)
            if len(zip_ids)==1:
                bzip = better_zip_obj.browse(cr, uid, zip_ids[0])
                setup_zip = bzip.name 
                setup_country_id = bzip.country_id.id if bzip.country_id else False
                setup_state_id = bzip.state_id.id if bzip.state_id else False
            else:
                setup_zip = row['zip']
                setup_country_id = False
                setup_state_id = False
                
            vals = {
                    'name' : row['name'],
                    'partner_name' : row['name'],
                    'street' : 'street' in row and row['street'] or False,
                    'city' : row['city'],
                    'zip' : setup_zip,
                    'state_id' : setup_state_id or False,
                    'country_id' : setup_country_id or False,
                    'phone' : 'phone' in row and row['phone'] or False,
                    'mobile' : 'mobile' in row and row['mobile'] or False,
                    'fax' : 'fax' in row and row['fax'] or False,
                    'email' : 'email' in row and row['email'] or False,
                    'website' : 'website' in row and row['website'] or False,
                    
                    'user_id' : setup_user_id or False,
                    'description' : setup_description or False,
                    'referred' : setup_referred or False,
                    }
            
            lead_id = lead_obj.create(cr, uid, vals)
        iFile.close()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: