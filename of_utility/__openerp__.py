# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2010 Associazione OpenERP Italia
#    (<http://www.openerp-italia.org>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Openforce - Utility',
    'version': '0.2',
    'category': 'Localisation/Italy',
    'description': """Openforce utility

Functionalities:
- Import products from another db
- Import italian bank from ufficial file abicab.txt
""",
    'author': 'Openforce',
    'website': 'http://www.openerp-italia.org',
    'license': 'AGPL-3',
    #"depends" : ['openforce_italian_bank'],
    "depends" : ['l10n_it_fiscalcode', 'product', 'account',
                 'openforce_withholding_tax'],
    "data" : [
        'wizard/import_product_from_db_view.xml',
        'wizard/import_italian_bank_view.xml',
        'wizard/import_partner_view.xml',
        'wizard/import_product_view.xml',
        'wizard/import_account_view.xml',
        'wizard/utility_product_view.xml',
        'wizard/set_draft_invoice_view.xml',
        'wizard/stock_view.xml',
        'wizard/withholding_tax_view.xml',
        'wizard/account_view.xml',
        'wizard/account_align_period_view.xml',
        'wizard/migration_view.xml',
        ],
    "demo" : [],
    "active": False,
    "installable": True
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

