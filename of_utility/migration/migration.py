# -*- coding: utf-8 -*-
#################################################################################
#    Author: Alessandro Camilli a.camilli@yahoo.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, orm
from openerp.tools.translate import _
import time
import base64
import csv
import os


class migration(orm.TransientModel):
    
    _name = "openforce.utility.migration"
    
    _description = 'Use this wizard to work with db migrations'
    
    def _geonames_prepare_address(self, cr, uid, address):
        # address = super(migration, self)._geonames_prepare_address(cr, uid, 
        #                                                           address)
        better_zip_obj = self.pool['res.better.zip']
        domain = []
        if 'zip' in address and address['zip']:
            domain.append(('name', '=', address['zip']))
            zip_ids = better_zip_obj.search(cr, uid, domain)
            if len(zip_ids) > 0:
                bzip = better_zip_obj.browse(cr, uid, zip_ids[0])
                #bzip = zip_ids[0]
                address['zip'] = bzip.name  
                address['city'] = bzip.city  
                address['country_id'] = bzip.country_id.id \
                    if bzip.country_id else False
                address['country_state_id'] = bzip.state_id.id \
                    if bzip.state_id else False
        return address
    
    def _get_partner_account(self, cr, uid, partner, data, type='receivable'):
        account_id = False
        # cli-for
        if partner.customer and partner.supplier:
            if type=='payable':
                account_id = data['partner_acc_customer_supplier_payable_id']
            else:
                account_id = data['partner_acc_customer_supplier_receivable_id']
        # cli
        elif partner.customer:
            if type=='payable':
                account_id = data['partner_acc_customer_payable_id']
            else:
                account_id = data['partner_acc_customer_receivable_id']
        # for
        elif partner.supplier:
            if type=='payable':
                account_id = data['partner_acc_supplier_payable_id']
            else:
                account_id = data['partner_acc_supplier_receivable_id']
        return account_id
        
    
    def from_7_to_8(self, cr, uid, data, context=None):
        # Partner
        self.from_7_to_8_partner(cr, uid, data, context)
        # banking
        # self.from_7_to_8_banking(cr, uid, data, context)
        
        
                    
    def from_7_to_8_banking(self, cr, uid, data, context=None):
        # mandate_obj = self.pool['sdd.mandate']
        mandate8_obj = self.pool['account.banking.mandate']
        partner_bank_obj = self.pool['res.partner.bank']
        sql = "SELECT * \
                FROM SDD_MANDATE \
                WHERE partner_bank_id > 0"
        cr.execute(sql)
        # cursor to dict
        columns = [i[0] for i in cr.description]
        # mandati di pagamento
        for mandate7 in cr.fetchall():
            mandate7 = dict(zip(columns, mandate7))
            # skip if exists
            domain = [('unique_mandate_reference', '=', mandate7['unique_mandate_reference'])]
            mandate8_ids = mandate8_obj.search(cr, uid, domain)
            if mandate8_ids:
                continue
            # Create new version of mandate
            mandate_id = mandate8_obj.create(cr, uid, mandate7)
            print mandate7['unique_mandate_reference']
            ## Tutte le banche con questo mandato x aggiornare le linee di pagamento
            domain = [('mandate_ids', 'in', [mandate_id])]
            pb_ids = partner_bank_obj.search(cr, uid, domain)
            # payment lines
            domain = [('bank_id', 'in', pb_ids)]
            pl_ids = self.pool['payment.line'].search(cr, uid, domain)
            if pl_ids:
                self.pool['payment.line'].write(cr, uid, pl_ids,
                                                {'mandate_id': mandate_id})
        # Mandati su tutte le righe di pagamento
        domain = [('mandate_id', '=', False)]
        pl_ids = self.pool['payment.line'].search(cr, uid, domain)
        for line in self.pool['payment.line'].browse(cr, uid, pl_ids):
            mandate_line_id = False
            if line.bank_id and line.bank_id.mandate_ids:
                mandate_line_id = line.bank_id.mandate_ids[0].id
            elif line.partner_id.bank_ids:
                mandate_line_id = line.partner_id.bank_ids[0].mandate_ids[0].id
            if mandate_line_id:
                self.pool['payment.line'].write(cr, uid, line.id,
                                                {'mandate_id': mandate_line_id})
            
    def from_7_to_8_partner(self, cr, uid, data, context=None):
        partner_obj = self.pool['res.partner']
        domain = []
        partner_ids = partner_obj.search(cr, uid, domain, context)
        # count = 0
        for partner in partner_obj.browse(cr, uid, partner_ids):
            # count += 1
            # if count > 1:
            #    break
            # New Addres Format
            print 'state_id for %s (id %s)' % (partner.name, str(partner.id)) 
            # Address --> 
            address = {
                'city' : partner.city or '',
                'zip' : partner.zip or '',
                'country_id' : partner.country_id and partner.country_id.id 
                    or False,
                }
            address = self._geonames_prepare_address(cr, uid, address)
            if 'country_state_id' in address and address['country_state_id']:
                partner_obj.write(cr, uid, [partner.id], 
                                  {'state_id': address['country_state_id']})
            
            # account property payable and receivalbe if doesn't exist
            account_id = False
            if not partner.property_account_receivable:
                account_id = self._get_partner_account(
                    cr, uid, partner, data['form'], 'receivable')
                if account_id:
                    partner_obj.write(cr, uid, [partner.id], 
                                      {'property_account_receivable': account_id})
            if not partner.property_account_payable:
                account_id = self._get_partner_account(
                    cr, uid, partner, data['form'], 'payable')
                if account_id:
                    partner_obj.write(cr, uid, [partner.id], 
                                      {'property_account_payable': account_id})
                    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: