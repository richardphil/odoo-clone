# -*- coding: utf-8 -*-
#################################################################################
#    Author: Alessandro Camilli a.camilli@yahoo.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, orm
from openerp.tools.translate import _
import time
import base64
import csv
import os

class withholding_tax(orm.TransientModel):
    
    _name = "openforce.utility.withholding.tax"
    
    _description = 'Use this wizard to work with withholding tax'
    
    def align_wt_statement(self, cr, uid, ids, data, context=None):
        wt_inv_obj = self.pool['account.invoice.withholding.tax']
        domain = [('id', '>' , 0)]
        wt_inv_ids = wt_inv_obj.search(cr, uid, domain)
        # Align with WT statement
        for wt_inv_id in wt_inv_ids:
            wt_inv_obj._align_statement(cr, uid, [wt_inv_id])
        
        # Align with WT Moves
        wt_vl_obj = self.pool['withholding.tax.voucher.line']
        domain = [('id', '>' , 0)]
        wt_vl_ids = wt_vl_obj.search(cr, uid, domain)
        for wt_vl_id in wt_vl_ids:
            wt_vl_obj._align_wt_move(cr, uid, [wt_vl_id])
        
        return True
    
    '''
    def aligns_price_from_sale_order(self, cr, uid, ids, data, context=None):
         move_ids = self.pool.get('stock.move').search(cr, uid, [('sale_line_id', '!=',  False)])
         if move_ids:
             for move in self.pool.get('stock.move').browse(cr, uid, move_ids):
                 #print move.id 
                 if move.sale_line_id:
                     line = move.sale_line_id
                     new_price = round(line.price_unit *  (1-(line.discount or 0.0)/100.0) * (1-(line.discount2 or 0.0)/100.0) , self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Price'))
                     
                     self.pool.get('stock.move').write(cr, uid, [move.id], {'price_unit' : new_price})'''
                     
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: