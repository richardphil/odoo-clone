# -*- coding: utf-8 -*-
#################################################################################
#    Author: Alessandro Camilli a.camilli@yahoo.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, orm
from openerp.tools.translate import _
import time
import psycopg2
import xmlrpclib

from StringIO import StringIO


class wizard_utility_product(orm.TransientModel):
    
    _name = "wizard.openforce.utility.product"
    
    _description = 'Use this wizard to import product'
    
    _defaults={
    }
    
    def default_code_from_supplier_code(self, cr, uid, ids, data, context=None):
        '''
        username = 'admin' #the user
        pwd = 'qawsed45'      #the password of the user
        dbname = 'test01'    #the database
        
        # Get the uid
        sock_common = xmlrpclib.ServerProxy ('http://localhost:8069/xmlrpc/common')
        uid = sock_common.login(dbname, username, pwd)
        
        #replace localhost with the address of the server
        sock = xmlrpclib.ServerProxy('http://localhost:8069/xmlrpc/object')
        
        partner = {
           'name': 'Fabien Pinckaers',
           'login': 'fabien',
           'company_id': 1,
           'active': True,
           'menu_id': 1,
           'notification_email_send': 'none',
        }
        user_id = sock.execute(dbname, uid, pwd, 'res.users', 'create', partner)
        '''
        for wiz_obj in self.read(cr,uid,ids):
            if 'form' not in data:
                data['form'] = {}
            self.pool.get('openforce.utility.product').default_code_from_supplier_code(cr, uid, ids, data, context=None)
        
        return {'type': 'ir.actions.act_window_close'}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: