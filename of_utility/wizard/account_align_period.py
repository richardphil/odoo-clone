# -*- coding: utf-8 -*-
#################################################################################
#    Author: Alessandro Camilli a.camilli@yahoo.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, orm
from openerp.tools.translate import _
import time
import psycopg2
from StringIO import StringIO


class wizard_utility_account_align_period(orm.TransientModel):
    
    _name = "of.utility.account.align.period"
    
    _description = 'Use this wizard to align period with date move'
    
    _columns={
        'fiscalyear_id': fields.many2one('account.fiscalyear', string='Year'),
        'move_ids': fields.many2many('account.move', string='Moves', 
                      help="Only the moves in the list will be aligned"),
    }
    
    _defaults={
    }
    '''
    def onchange_fiscalyear_id(self, cr, uid, ids, fiscalyear_id, context=None):
        res = {}
        if not fiscalyear_id:
            return res
        # Moves
        moves_to_align = []
        moves_to_align_ids= self.pool['openforce.utility.account'].get_moves_not_aligned_period_to_date(
                                        cr, uid, fiscalyear_id, period_ids=False, context=context
                                                )
        for m in self.pool['account.move'].browse(cr, uid, moves_to_align_ids):
            #moves_to_align.append(m)
            moves_to_align.append((0,0, {'move_id': m.id}))
            #moves_to_align.append((0,0, {'account_move_id': it['id']}))
            #moves_to_align.append(it['id'])
            #moves_to_align.append((6, 0, [it['id']]))
        if moves_to_align:
            val = {'move_ids': moves_to_align}
            #val = {'move_ids': (6, 0, moves_to_align)}
        
        return {'value': val}'''
    
    def execute(self, cr, uid, ids, data, context=None):
        
        for wiz_obj in self.read(cr,uid,ids):
            if 'form' not in data:
                data['form'] = {}
            #data['form']['type'] = wiz_obj['type']
            data['form']['fiscalyear_id'] = wiz_obj['fiscalyear_id'][0]
            data['form']['move_ids'] = wiz_obj['move_ids']
            
            self.pool.get('openforce.utility.account').align_period_to_date(cr, uid, 
                                                                wiz_obj['fiscalyear_id'][0], 
                                                                context=None)

        return {'type': 'ir.actions.act_window_close'}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: