# -*- coding: utf-8 -*-
#################################################################################
#    Author: Alessandro Camilli a.camilli@yahoo.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, orm
from openerp.tools.translate import _
import time
import psycopg2
from StringIO import StringIO


class wizard_utility_account(orm.TransientModel):
    
    _name = "wizard.openforce.utility.account"
    
    _description = 'Use this wizard to work with account'
    
    _columns={
        'elaboration_type': fields.selection([('align_period_move_from_date', 'Align Period Move From date')],
            'Elaboration type', readonly=False, required = True
        ),
    }
    
    _defaults={
    }
    
    def execute(self, cr, uid, ids, data, context=None):
        
        view_module = 'openforce_utility'
        view_id = False
        
        for wiz_obj in self.read(cr,uid,ids):
            if 'form' not in data:
                data['form'] = {}
            data['form']['elaboration_type'] = wiz_obj['elaboration_type']
            
            if wiz_obj['elaboration_type'] == 'align_period_move_from_date':
                view_id = 'openforce_utility_account_align_period'
                view_res_model = 'of.utility.account.align.period'
            
            
        if view_id:
            view_ref = self.pool.get('ir.model.data').get_object_reference(
                                                            cr, uid, 
                                                            view_module, 
                                                            view_id)
            return {
                'name': _('Account'),
                'view_type': 'form',
                'view_mode': 'form',
                'view_id': view_ref[1] or False,
                'res_model': view_res_model,
                'res_id': False,
                #'context': "{'type':'out_invoice'}",
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'current',
                'domain': '[]',
            }
        else:
            return {'type': 'ir.actions.act_window_close'}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: