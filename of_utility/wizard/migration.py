# -*- coding: utf-8 -*-
#################################################################################
#    Author: Alessandro Camilli a.camilli@yahoo.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, orm
from openerp.tools.translate import _
import time
import psycopg2
from StringIO import StringIO


class wizard_utility_migration(orm.TransientModel):
    
    _name = "wizard.openforce.utility.migration"
    
    _description = 'Use this wizard to work with migration'
    
    _columns={
        'version': fields.selection([('7_to_8', 'From 7 to 8')],
            'Migration Version', readonly=False, required=True),
        'partner_acc_customer_supplier_payable_id' : fields.many2one(
            'account.account', 'Customer-Supplier Payable Account'),
        'partner_acc_customer_supplier_receivable_id' : fields.many2one(
            'account.account', 'Customer-Supplier Receivable Account'),
        'partner_acc_customer_payable_id' : fields.many2one(
            'account.account', 'Customer Payable Account'),
        'partner_acc_customer_receivable_id' : fields.many2one(
            'account.account', 'Customer Receivable Account'),
        'partner_acc_supplier_payable_id' : fields.many2one(
            'account.account', 'Supplier Payable Account'),
        'partner_acc_supplier_receivable_id' : fields.many2one(
            'account.account', 'Supplier Receivable Account'),
    }
    
    _defaults={
        'version' : '7_to_8'
    }
    
    def execute(self, cr, uid, ids, data, context=None):
        
        migration_obj = self.pool['openforce.utility.migration']
        view_module = 'openforce_utility'
        view_id = False
        
        for wiz_obj in self.read(cr,uid,ids):
            if 'form' not in data:
                data['form'] = {}
            data['form']['version'] = wiz_obj['version']
            data['form']['partner_acc_customer_supplier_payable_id'] = \
                wiz_obj['partner_acc_customer_supplier_payable_id'] and \
                wiz_obj['partner_acc_customer_supplier_payable_id'][0] or \
                False
            data['form']['partner_acc_customer_supplier_receivable_id'] = \
                wiz_obj['partner_acc_customer_supplier_receivable_id'] and \
                wiz_obj['partner_acc_customer_supplier_receivable_id'][0] or \
                False
            data['form']['partner_acc_customer_payable_id'] = \
                wiz_obj['partner_acc_customer_payable_id'] and \
                wiz_obj['partner_acc_customer_payable_id'][0] or \
                False
            data['form']['partner_acc_customer_receivable_id'] = \
                wiz_obj['partner_acc_customer_receivable_id'] and \
                wiz_obj['partner_acc_customer_receivable_id'][0] or \
                False
            data['form']['partner_acc_supplier_payable_id'] = \
                wiz_obj['partner_acc_supplier_payable_id'] and \
                wiz_obj['partner_acc_supplier_payable_id'][0] or \
                False
            data['form']['partner_acc_supplier_receivable_id'] = \
                wiz_obj['partner_acc_supplier_receivable_id'] and \
                wiz_obj['partner_acc_supplier_receivable_id'][0] or \
                False
            
            if wiz_obj['version'] == '7_to_8':
                migration_obj.from_7_to_8(cr, uid, data, context)
        
        return {'type': 'ir.actions.act_window_close'}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: