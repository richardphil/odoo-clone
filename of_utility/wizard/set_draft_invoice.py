# -*- coding: utf-8 -*-
#################################################################################
#    Author: Alessandro Camilli a.camilli@yahoo.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, orm
from openerp.tools.translate import _
import time
import psycopg2
from StringIO import StringIO

class wizard_set_draft_invoice(orm.TransientModel):
    
    _name = "wizard.openforce.utility.set.draft.invoice"
    
    _description = 'Use this wizard to set draft invoice'
    
    _columns={
        'invoice_type' : fields.selection([
            ('sale','Sale'),
            ('purchase','Purchase'),
            ], 'Invoice type', required=True),
        'operation' : fields.selection([
            ('draft','Set To draft'),
            ('delete','Delete'),
            ], 'Operation type', required=True),
        'journal_id' : fields.many2one('account.journal', 'Journal', required=True),
        'partner_id' : fields.many2one('res.partner', 'Partner')
    }
    
    _defaults={
    }
    
    def execute(self, cr, uid, ids, data, context=None):
        
        for wiz_obj in self.read(cr,uid,ids):
            if 'form' not in data:
                data['form'] = {}
            
            invoice_obj = self.pool['account.invoice']
            # Supplier invoice
            if wiz_obj['invoice_type'] == 'purchase':
                domain = [('type', 'like', 'in_%')]
            # Customer invoice
            if wiz_obj['invoice_type'] == 'sale':
                domain = [('type', 'like', 'out_%')]
            
            domain.append(('journal_id','=', wiz_obj['journal_id'][0]) )
            if wiz_obj['partner_id']:
                domain.append(('partner_id', '=', wiz_obj['partner_id'][0]))
            
            inv_ids = invoice_obj.search(cr, uid, domain)
            #invoice_obj.action_cancel(cr, uid, inv_ids)
            # Set in Cancel
            for inv in invoice_obj.browse(cr, uid, inv_ids):
                if inv.number:
                    print inv.number + ' cancel'
                    invoice_obj.action_cancel(cr, uid, [inv.id])
            # Set in Draft
            for inv in invoice_obj.browse(cr, uid, inv_ids):
                print inv.internal_number + ' draft'
                invoice_obj.action_cancel_draft(cr, uid, [inv.id])
            if wiz_obj['operation'] == 'delete':
                invoice_obj.write(cr, uid, inv_ids, {'internal_number': ''})
                invoice_obj.unlink(cr, uid, inv_ids)
                

        return {'type': 'ir.actions.act_window_close'}
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: