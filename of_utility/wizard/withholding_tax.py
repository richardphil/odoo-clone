# -*- coding: utf-8 -*-
#################################################################################
#    Author: Alessandro Camilli a.camilli@yahoo.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, orm
from openerp.tools.translate import _
import time
import psycopg2
from StringIO import StringIO


class wizard_utility_withholding_tax(orm.TransientModel):
    
    _name = "wizard.openforce.utility.withholding.tax"
    
    _description = 'Use this wizard to operate with WT'
    
    _columns={
        'elaboration_type': fields.selection(
            [('align_wt_statement', 'Align WT statements and WT moves')],
            'Elaboration type', readonly=False, required = True
        ),
    }
    
    _defaults={
    }
    
    def execute(self, cr, uid, ids, data, context=None):
        
        wt_obj = self.pool['openforce.utility.withholding.tax']
        
        for wiz_obj in self.read(cr,uid,ids):
            if 'form' not in data:
                data['form'] = {}
            #data['form']['type'] = wiz_obj['type']
            data['form']['elaboration_type'] = wiz_obj['elaboration_type']
            
            if wiz_obj['elaboration_type'] == 'align_wt_statement':
                wt_obj.align_wt_statement(cr, uid, ids, data, context=None)

        return {'type': 'ir.actions.act_window_close'}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: