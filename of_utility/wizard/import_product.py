# -*- coding: utf-8 -*-
#################################################################################
#    Author: Alessandro Camilli a.camilli@yahoo.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, orm
from openerp.tools.translate import _
import time
import psycopg2
from StringIO import StringIO


class wizard_import_product(orm.TransientModel):
    
    _name = "wizard.openforce.utility.import.product"
    
    _description = 'Use this wizard to import product'
    
    _columns={
        'file_txt_to_import': fields.binary('File TXT to import', required=True),
        'uom_id': fields.many2one('product.uom', 'Unit of measure', required=True),
        'type' : fields.selection([
            ('product','Prodotto Stoccabile'),
            ('consu','Consumabile'),
            ('service','Servizio')], 'Tipo prodotto',
            required=True),
        'categ_id': fields.many2one('product.category', 'Categoria', required=True),
        'join_by_default_code': fields.boolean('Join by default code', 
                help="Join products with same Default Code"),
        'user_id': fields.many2one('res.users', 'User'),
        'field_separator_csv': fields.char('Field separator for csv', required=True),
    }
    
    _defaults={
        'type' : 'product',
        'field_separator_csv': ';',
    }
    
    def import_product(self, cr, uid, ids, data, context=None):
        
        for wiz_obj in self.read(cr,uid,ids):
            if 'form' not in data:
                data['form'] = {}
            #data['form']['type'] = wiz_obj['type']
            data['form']['file_txt_to_import'] = wiz_obj['file_txt_to_import']
            data['form']['uom_id'] = wiz_obj['uom_id'][0]
            data['form']['type'] = wiz_obj['type']
            data['form']['categ_id'] = wiz_obj['categ_id'][0]
            data['form']['field_separator_csv'] = wiz_obj['field_separator_csv']
            data['form']['join_by_default_code'] = wiz_obj['join_by_default_code']
        
            self.pool.get('openforce.utility.product').import_from_csv(cr, uid, ids, data, context=None)

        return {'type': 'ir.actions.act_window_close'}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: