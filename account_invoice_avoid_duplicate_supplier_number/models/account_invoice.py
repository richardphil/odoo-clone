# -*- coding: utf-8 -*-
#
##############################################################################
#
#    Author(s): Alessandro Camilli (alessandrocamilli@openforce.it)
#
#    Copyright © 2016 Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see:
#    http://www.gnu.org/licenses/agpl-3.0.txt.
#
##############################################################################


from openerp import models, fields, api, _
from openerp.exceptions import except_orm, ValidationError


class account_invoice(models.Model):
    _inherit = "account.invoice"
 
    @api.multi
    def invoice_validate(self):
        
        for inv in self:
            if inv.supplier_invoice_number:
                domain = [('partner_id', '=', inv.partner_id.id),
                          ('supplier_invoice_number', '=', 
                           inv.supplier_invoice_number),
                          ('date_invoice', '=', inv.date_invoice),
                          ('state', 'not in', ['draft']),]
                inv_alredy_exists = self.search(domain, limit=1)
                if inv_alredy_exists:
                    raise ValidationError(
                        _('Supplier Invoice Number already exists'))
        return super(account_invoice, self).invoice_validate()
