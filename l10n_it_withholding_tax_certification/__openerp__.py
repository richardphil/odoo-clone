# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2015 Alessandro Camilli 
#    (<http://www.openforce.it>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Withholding tax Certification',
    'version': '0.2',
    'category': 'Account',
    'description': """Withholding tax Certification

""",
    'author': 'Alessandro Camilli',
    'website': 'http://www.openforce.it',
    'license': 'AGPL-3',
    "depends" : ['account', 'l10n_it_withholding_tax', 
                 'l10n_it_withholding_tax_payment',
                 'l10n_it_fiscalcode'],
    "data" : [
        'certification_workflow.xml',
        'security/ir.model.access.csv',
        'views/certification_view.xml',
        'data/mail_template.xml',
        ],
    "demo" : [],
    "active": False,
    "installable": True
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: