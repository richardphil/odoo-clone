# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _


class project_task(models.Model):
    _inherit = 'project.task.type'
    
    stage_change_no_mail = fields.Boolean(string='No e-mail if became this \
        stage')
 

class project(models.Model):
    _inherit = 'project.task'
    
    @api.multi
    def write(self, vals):
        '''
        No send mail in thread for same stages
        '''
        if 'stage_id' in vals:
            stage_id = vals.get('stage_id')
            type = self.env['project.task.type'].browse(stage_id)
            if type.stage_change_no_mail:
                self = self.with_context(mail_track_log_only = True)
        return super(project, self).write(vals)
    

class project_issue(models.Model):
    _inherit = 'project.issue'
    
    @api.multi
    def write(self, vals):
        '''
        No send mail in thread for same stages
        '''
        if 'stage_id' in vals:
            stage_id = vals.get('stage_id')
            type = self.env['project.task.type'].browse(stage_id)
            if type.stage_change_no_mail:
                self = self.with_context(mail_track_log_only = True)
        return super(project_issue, self).write(vals)

