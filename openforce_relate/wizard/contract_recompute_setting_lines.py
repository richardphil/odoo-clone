# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@yahoo.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


import time
from openerp.tools.translate import _
from openerp.osv import fields, orm, osv


class relate_wizard_contract_recompute_setting(orm.TransientModel):

    _name = "relate.wiz.contract.recompute.line"
    _description = "Wizard Contract recompute setting lines"

    _columns = {
        'journal_ids': fields.many2many('account.analytic.journal', 
                    'relate_wiz_contract_recompute_line_journal_rel', 'wiz_id', 'journal_id', 
                    string='Journals', help="Only selected journals will be recomputed. "),
        'recompute_costs': fields.boolean('Recompute Costs'),
        'all_contracts': fields.boolean('All contracts')
    }
    
    def recompute(self, cr, uid, ids, context=None):
        
        analytic_account_obj = self.pool['account.analytic.account']
        
        if context is None:
            context = {}
        # Controlli
        for form in self.browse(cr, uid, ids):
            
            if not form.journal_ids:
                raise orm.except_orm(_('Errore!'),_("Inserire ALMENO un giornale"))
            # Set journal
            j_ids = []
            for j in form.journal_ids:
                j_ids.append(j.id)
            
            if form.all_contracts:
                domain = [('id', '>', 0)]
                contract_ids = analytic_account_obj.search(cr, uid, domain)
            else:
                contract_ids = context.get('contract_ids')
            data = {
                'analytic_journal_ids' : j_ids,
                'recompute_costs' : form.recompute_costs,
                }
            analytic_account_obj.recompute_setting_invoice_datas(cr, uid, 
                                                             contract_ids, 
                                                             data, 
                                                             context=None)
                                 
        return True
