# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2013 Alessandro Camilli
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields,orm
from openerp import tools

class report_relate_line(orm.Model):
    _name = "report.relate.analytic.line"
    _description = "Relate Analytic line "
    _auto = False
    _columns = {
        'id': fields.integer('# of lines', readonly=True),
        'account_id': fields.many2one('account.analytic.account', 'Project', required=True),
        'date': fields.date('Date', readonly=True),
        'day': fields.char('Day', size=128, readonly=True),
        'year': fields.char('Year', size=64, required=False, readonly=True),
        'month':fields.selection([('01','January'), ('02','February'), ('03','March'), ('04','April'), ('05','May'), ('06','June'), ('07','July'), ('08','August'), ('09','September'), ('10','October'), ('11','November'), ('12','December')], 'Month', readonly=True),
        'name': fields.char('Name', size=256, readonly=True),
        'journal_id': fields.many2one('account.analytic.journal', 'Journal', required=True),
        'user_id': fields.many2one('res.users', 'User', required=True),
        'partner_id': fields.many2one('res.partner', 'Partner', required=True),
        'amount': fields.float('Cost'),
        'amount_invoiced': fields.float('Invoiced'),
        'amount_to_invoice': fields.float('Value'),
        'price': fields.float('Price'),
        'balance': fields.float('Balance'),
        'qty': fields.float('qty'),
        'product_uom_id': fields.many2one('product.uom', 'Uom'),
        'product_to_invoice_id': fields.many2one('product.product', 'Product to invoice'),
        'vehicle_id': fields.many2one('fleet.vehicle', 'Vehicle', readonly=True),
        'partner_account_id': fields.many2one('res.partner', 'Partner Account'),
        'relate_task_id': fields.many2one('relate.task', 'Task Ref'),
    }
    _order = 'date desc'

    def init(self, cr):
        tools.sql.drop_view_if_exists(cr, 'report_relate_analytic_line')
        cr.execute("""
            CREATE view report_relate_analytic_line as
              
            SELECT 
                line.id AS id,
                line.account_id AS account_id,
                line.date AS date,
                to_char(line.date, 'YYYY') as year,
                to_char(line.date, 'MM') as month,
                to_char(line.date, 'YYYY-MM-DD') as day,
                line.name AS name,
                line.journal_id AS journal_id,
                rt.user_id AS user_id,
                con.partner_id AS partner_id, 
                CASE WHEN (j.type = 'sale') THEN 0 ELSE line.amount END as amount,
                CASE WHEN (j.type = 'sale') THEN line.amount ELSE 0 END as amount_invoiced,
                line.amount_to_invoice AS amount_to_invoice,
                CASE WHEN (line.unit_amount > 0) THEN ROUND(CAST(line.amount_to_invoice AS NUMERIC) / CAST(line.unit_amount AS NUMERIC) , 3) ELSE 0 END as price,
                line.balance AS balance,
                line.unit_amount AS qty,
                pruom.id as product_uom_id,
                prinv.id as product_to_invoice_id,
                ve.id as vehicle_id,
                pm.id as partner_account_id,
                rt.id as relate_task_id
            FROM account_analytic_line line
            LEFT JOIN account_analytic_journal j ON j.id = line.journal_id
            LEFT JOIN account_analytic_account con ON con.id = line.account_id 
            LEFT JOIN res_partner p ON p.id = con.partner_id 
            LEFT JOIN res_users u ON u.id = line.user_id
            LEFT JOIN res_partner pu ON pu.id = u.partner_id
            LEFT JOIN product_product pr ON pr.id = line.product_id 
            LEFT JOIN product_uom pruom ON pruom.id = line.product_uom_id 
            LEFT JOIN relate_task_line rtl ON rtl.id = line.relate_task_line_id
            LEFT JOIN relate_task rt ON rt.id = rtl.task_id
            LEFT JOIN fleet_vehicle ve ON ve.id = rt.vehicle
            LEFT JOIN product_product prinv ON prinv.id = line.product_to_invoice
            LEFT JOIN account_move_line aml ON aml.id = line.move_id
            LEFT JOIN account_move am ON am.id = aml.move_id
            LEFT JOIN res_partner pm ON pm.id = am.partner_id
        """)

