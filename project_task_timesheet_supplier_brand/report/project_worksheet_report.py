# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (alessndrocamilli@openforce.it)
#
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, osv
from openerp import tools


class report_project_worksheet_project(osv.osv):
    _inherit = "report.project.worksheet"
    _columns = {
        'brand_id': fields.many2one('res.partner.brand', 'Brand', readonly=True),
    }
    
    def init(self, cr):
        tools.sql.drop_view_if_exists(cr, 'report_project_worksheet')
        cr.execute("""
            CREATE view report_project_worksheet as
              SELECT
                    (select 1 ) AS nbr,
                    ws.id as id,
                    ws.date as date,
                    ws.type as type,
                    ws.product_id as product_id,
                    ws.description as description,
                    ws.quantity as quantity,
                    ws.uom_id as uom_id,
                    ws.price_supplier as price_supplier,
                    /* Quote */
                    CASE WHEN ws.is_quote is true 
                        THEN ws.subtotal_supplier
                        ELSE 0
                        END as quote_amount_costs,
                    CASE WHEN ws.is_quote is true 
                        THEN ws.subtotal
                        ELSE 0
                        END as quote_amount_to_invoice,
                    CASE WHEN ws.is_quote is true 
                        THEN ws.amount_mark_up
                        ELSE 0
                        END as quote_amount_mark_up,
                    /* NO quote */
                    CASE WHEN ws.is_quote is not true 
                        THEN ws.subtotal_supplier
                        ELSE 0
                        END as amount_costs,
                    CASE WHEN ws.is_quote is not true 
                        THEN ws.subtotal
                        ELSE 0
                        END as amount_to_invoice,
                    CASE WHEN ws.is_quote is not true 
                        THEN ws.amount_mark_up
                        ELSE 0
                        END as amount_mark_up,
                    ws.list_price as list_price,          
                    ws.to_invoice as to_invoice,
                    CASE WHEN SUM(ws.subtotal_supplier) > 0 
                        THEN round(SUM(ws.subtotal) / SUM(ws.subtotal_supplier), 2)
                        ELSE 1
                        END as mark_up,
                    t.name as name,
                    t.user_id as user_id,
                    t.project_id as project_id,
                    p.brand_id as brand_id,
                    t.partner_id as partner_id,
                    p.country_id as country_id,
                    t.date_deadline as date_deadline,
                    t.issue_ticket_ref as issue_ticket_ref,
                    t.intervention_type_id as intervention_type_id,
                    t.stage_id as stage_id,
                    t.supplier_invoice_receipt as supplier_invoice_receipt
                    
              FROM project_task_work_supplier ws
                LEFT JOIN project_task t on t.id = ws.task_id
                LEFT JOIN res_partner p on p.id = t.partner_id
                WHERE t.active = 'true'
                GROUP BY
                    ws.id,
                    ws.date,
                    ws.type,
                    ws.product_id,
                    ws.description,
                    ws.quantity,
                    ws.uom_id,
                    ws.price_supplier,
                    ws.subtotal_supplier,
                    ws.list_price,
                    ws.subtotal,
                    ws.to_invoice,
                    ws.amount_mark_up,
                    t.name,
                    t.user_id,
                    p.brand_id,
                    t.project_id,
                    t.partner_id,
                    p.country_id,
                    t.date_deadline,
                    t.issue_ticket_ref,
                    t.intervention_type_id,
                    t.stage_id,
                    t.supplier_invoice_receipt
        """)