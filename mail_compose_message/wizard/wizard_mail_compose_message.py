# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2014
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv
from openerp.osv.orm import setup_modifiers
from openerp.tools.translate import _

class wizard_mail_compose_message(osv.TransientModel):
    
    _inherit = 'mail.compose.message'
    _description = "Wizard Mail Compose Message"

    def default_get(self, cr, uid, fields, context=None):
        if not context:
            context = {}
            
        if 'compose_new_message' in context \
                and context.get('compose_new_message'):
            try:
                context['message_id'] = False
                context['parent_id'] = False
                
            except Exception, e:
                print e[0]
        
        return super(wizard_mail_compose_message, self).default_get(cr, uid, 
                                                        fields, context=context)


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
