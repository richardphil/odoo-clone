# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2014
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api
from openerp.tools.translate import _
from datetime import datetime, time, timedelta
import openerp

class alert_chat(models.TransientModel):
    _inherit = 'alert.chat'
    
    @api.model
    def _prepare_message(self):
        super(alert_chat, self)._prepare_message()
        
        messages_to_send = []
        # for all users
        domain = [('active', '=', True)]
        users = self.env['res.users'].search(domain)
        for user in users:
            all_notify = self.env['calendar.alarm_manager']\
                .sudo(user.id).get_next_notif()
            for notify in all_notify:
                messages_to_send.append(notify)
        # create messages
        for msg in messages_to_send:
            event = self.env['calendar.event'].browse(msg['event_id'])
            val = {
                'model' : self.env['calendar.event']._model,
                'res_id' : event.id,
                'message' : event.name,
                'user_to' : event.user_id.id,
            }
            self.env['alert.chat.message'].create(val)
        
        return True
    