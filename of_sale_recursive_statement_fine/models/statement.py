# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2014
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, orm
from openerp.tools.translate import _
from datetime import datetime, time, timedelta
from dateutil.relativedelta import relativedelta
    
    
class sale_recursive_statement_product(orm.Model):
    _inherit="sale.recursive.statement.product"
    
    _columns = {
        'fine_to_product_id': fields.many2one(
            'sale.recursive.statement.product', 'Fine applied to product'),
        'fine_to_role_id': fields.many2one(
            'sale.recursive.statement.fine', 'Fine Role'),
    }
    

class sale_recursive_statement_fine(orm.Model):
    _name="sale.recursive.statement.fine"
    _columns = {
        'product_id': fields.many2one('product.product', 'Subject Product',
                                      required=True),
        'days': fields.integer('Expired Days', help="days expired", 
                               required=True),
        'valid_state': fields.selection([
            ('draft', 'Draft'),
            ('sent', 'Sent'),
            ('paid', 'Paid'),
            ('invoiced', 'Invoiced'),
            ], 'Valid Status', help="Statement State for fine apply"),
        'fine_product_id': fields.many2one('product.product', 'Fine Product',
                                           required=True),
        'fine': fields.float('Fine (%)'),
        'min_amount': fields.float('Minimum Amount'),
        'email_tmpl_id': fields.many2one(
                    'email.template', 'E-Mail template to send',
                    help="Template to use when it sends mail to notify Fine.\
                    For any template, Nothig to will be sent."
                    ),
        'description': fields.text('Note'),
        'active': fields.boolean('Active'),
        
    }
    _defaults = {
        'active': True
    }
    
    def _prepare_recursive_poduct_fine(self, cr, uid, line_expired, role, 
                                       context=None):
        dp_obj = self.pool['decimal.precision']
        date_today_obj = datetime.today()
        # Price
        if role.fine:
            price_unit = line_expired.price_unit * ( role.fine/ 100)
        if role.min_amount \
            and ((price_unit * line_expired.qty) < role.min_amount):
            price_unit = round((role.min_amount / line_expired.qty), 
                               dp_obj.precision_get(cr, uid, 'Product UoS'))
        # Name
        name = _('%s %s') % (role.fine_product_id.name, line_expired.name)
        
        vals = {
                'fine_to_product_id' : line_expired.recursive_product_id.id,
                'fine_to_role_id' : role.id,
                'contract_id' : line_expired.statement_id.contract_id.id \
                    or False,
                'partner_id' : line_expired.statement_id.partner_id.id or False,
                'product_id' : role.fine_product_id.id or False,
                #'line_order_id' : line.id,
                'name' : name or False,
                'price_unit' : price_unit or False,
                'qty' : line_expired.qty or False,
                'uos' : line_expired.uos.id or False,
                'frequency' : 
                    line_expired.recursive_product_id.frequency or False,
                'frequency_uom' : 
                    line_expired.recursive_product_id.frequency_uom.id or False,
                'line_policy' : 
                    line_expired.recursive_product_id.line_policy or False,
                'date_start' : 
                    datetime.strftime(date_today_obj, "%Y-%m-%d")
                    or False,
                'date_stop' :
                    datetime.strftime(date_today_obj, "%Y-%m-%d") 
                    or False,
                'tax_id': [(6, 0, [x.id for x in line_expired.tax_id])]
            }
        
        return vals

    def execute_all(self, cr, uid, ids, context=None):
        domain = [('active', '=', True)]
        role_ids = self.search(cr, uid, domain, order='days')
        
        self.execute(cr, uid, role_ids, context)
        
        return True
    
    def execute(self, cr, uid, ids, context=None):
        '''
        Excution of ALL ROLES
        '''
        if not context:
            context={}
        statement_obj = self.pool['sale.recursive.statement']
        statement_line_obj = self.pool['sale.recursive.statement.line']
        date_today_obj = datetime.today()
        
        for role in self.browse(cr, uid, ids):
            # Compute Date
            now_obj = datetime.now()
            now = datetime.strftime(now_obj, "%Y-%m-%d")
            date_competence = datetime.strptime(now,"%Y-%m-%d")\
                    + timedelta(days=role.days * -1)
            # Statement line expired
            domain = [('statement_id.date', '=', date_competence),
                      ('product_id', '=', role.product_id.id)]
            if role.valid_state:
                domain.append( ('statement_id.state', '=', role.valid_state) )
            line_ids = statement_line_obj.search(cr, uid, domain)
            
            for line_expired in statement_line_obj.browse(cr, uid, line_ids):
                # Skip if fine line already exists
                domain = [('product_id', '=', role.fine_product_id.id),
                          ('statement_id.partner_id', '=', 
                           line_expired.statement_id.partner_id.id),
                          ('recursive_product_id.fine_to_product_id', '=', 
                           line_expired.recursive_product_id.id),
                          ('recursive_product_id.fine_to_role_id', '=', 
                           role.id),
                          ('period_date_start', '=', 
                           datetime.strftime(date_today_obj, "%Y-%m-%d"))]
                fine_line_ids = statement_line_obj.search(cr, uid, domain)
                if fine_line_ids:
                    continue
                # Create fine thru recursive product
                vals = self._prepare_recursive_poduct_fine(cr, uid, 
                                                           line_expired, role)
                rec_id = self.pool['sale.recursive.statement.product'].create(
                    cr, uid, vals)
                fine_rec_product = \
                    self.pool['sale.recursive.statement.product'].browse(
                        cr, uid, rec_id)
                # Recompute statement
                context.update({'contract_id': 
                                line_expired.statement_id.contract_id.id})
                statement_obj.recompute_statement(cr, uid, 
                                                  line_expired.statement_id.id,
                                                  context)
                # Send e-mail for new statement created
                # search new statement to send throught the fine_to field
                if role.email_tmpl_id:
                    domain = [('statement_id.partner_id', '=', 
                               fine_rec_product.contract_id.partner_id.id),
                              ('period_date_start', '=', 
                               fine_rec_product.date_start),
                              ('period_date_end', '=', 
                               fine_rec_product.date_stop),
                              ('recursive_product_id.fine_to_role_id', '=', 
                               role.id),
                              ('product_id', '=', 
                               fine_rec_product.product_id.id),]
                    st_line_ids = statement_line_obj.search(cr, uid, domain)
                    for st_line in statement_line_obj.browse(cr, uid, 
                                                             st_line_ids):
                        statement_obj.statement_mail_send(
                            cr, uid, [st_line.statement_id.id], 
                            role.email_tmpl_id)
                
            
        return True