# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _


class project_issue(models.Model):
    _inherit = 'project.issue'
    
    
    def onchange_stage_id(self, cr, uid, ids, stage_id, context=None):
        # To avoid clear data_close
        '''
        if not stage_id:
            return {'value': {}}
        stage = self.pool['project.task.type'].browse(cr, uid, stage_id, context=context)
        if stage.fold:
            return {'value': {'date_closed': fields.datetime.now()}}
        return {'value': {'date_closed': False}} '''
        
        res = super(project_issue, self).onchange_stage_id(cr, uid, ids, 
                                                           stage_id, context)
        # return res
        return {'value': {}}
        
    
    @api.multi
    def write(self, vals):
        '''
        Sync with issue if required
        '''
        for issue in self:
            if 'stage_id' in vals:
                stage = self.env['project.task.type'].browse(vals['stage_id'])
                if stage.update_date_end:
                    date_work = fields.Datetime.now()
                    vals['date_closed'] = date_work
            
        return super(project_issue,self).write(vals)