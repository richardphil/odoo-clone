# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _


class product_pricelist(models.Model):
    _inherit = 'product.pricelist.item'
    
    @api.one
    @api.onchange('price_discount', 'price_surcharge', 'price_round', 
                  'price_min_margin', 'price_max_margin')
    @api.depends('price_discount', 'price_surcharge', 'price_round', 
                 'price_min_margin', 'price_max_margin')
    def _compose_formula(self):
        formula = ''
        # Discount
        if self.price_discount and not self.price_discount == -1:
            formula += '* ( 1 + %s ) ' % str(self.price_discount)
        # Surchange
        if self.price_surcharge:
            if self.price_discount == -1:
                formula += '%s' % str(self.price_surcharge)
            else:
                formula += ' + %s' % str(self.price_surcharge)
        # Round
        if self.price_round:
            formula += ' %s %s' % (_("Round by:"), str(self.price_round))
        # Min Margin
        if self.price_min_margin:
            formula += ' %s %s' % (_("Min Margin:"), str(self.price_min_margin))
        # Max Margin
        if self.price_max_margin:
            formula += ' %s %s' % (_("Max Margin:"), str(self.price_max_margin))
        self.description_formula = formula

    
    description_formula = fields.Char(string='Price', 
                                      compute='_compose_formula')

