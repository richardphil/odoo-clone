# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2015 Alessandro Camilli 
#    (<http://www.openforce.it>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api
from openerp.tools.translate import _
from openerp import netsvc
#from scipy.weave.converters import default

class account_analytic_account(models.Model):
    _inherit="account.analytic.account"
    
    term_ids = fields.One2many('contract.term.contract', 'contract_id', 
        'Terms and Documents')
    
    @api.model
    def create(self, vals):
        '''
        Add documentent terms
        '''
        res = super(account_analytic_account, self).create(vals)
        domain = [('active', '=', True)]
        self.env['contract.term.document']\
            .with_context(contract_id = res.id)\
            .search(domain)\
            .add_to_contracts()
        return res
    

class contract_term_document(models.Model):
    _name="contract.term.document"
    
    name = fields.Char('Title')
    body = fields.Html()
    to_accept = fields.Boolean('To Accept', default=True)
    active = fields.Boolean('Active', default=True)
    
    @api.multi
    def add_to_contracts(self):
        '''
        Add document in all contracts or in one contract the ref is 
        in the context
        '''
        contract_id = self.env.context.get('contract_id', False)
        for doc in self:
            domain = [('state', 'in', ['draft','open','pending'])]
            if contract_id:
                domain.append(('id', '=', contract_id))
            contracts = self.env['account.analytic.account'].search(domain)
            for contract in contracts:
                # test if docs already exists
                domain = [('contract_id', '=', contract.id),
                          ('document_id','=',doc.id)]
                doc_exists = self.env['contract.term.contract'].search(domain)
                if not doc_exists:
                    val = {
                        'contract_id': contract.id,
                        'document_id': doc.id
                    }
                    term_id = self.env['contract.term.contract'].create(val)
                    # set to accept
                    if doc.to_accept:
                        wf_service = netsvc.LocalService("workflow")
                        wf_service.trg_validate(self.env.uid, 
                                'contract.term.contract', 
                                term_id.id, 'to_accept', self.env.cr)
                        
    @api.multi
    def remove_from_contracts(self):
        for doc in self:
            domain = [('document_id', '=', doc.id)]
            self.env['contract.term.contract'].search(domain).unlink()
    
class contract_term_contract(models.Model):
    _name="contract.term.contract"
    
    contract_id = fields.Many2one('account.analytic.account', 'Contract', 
                                  required=True, readonly=True)
    state = fields.Selection(selection=[('draft', 'Draft'),
                                        ('to_accept', 'To Accept'),
                                        ('accepted', 'Accepted')],
                             default='to_accept')
    document_id = fields.Many2one('contract.term.document', 'Document', 
                                  required=True)
    document_body = fields.Html(related='document_id.body', readonly=True)
    document_name = fields.Char(related='document_id.name', readonly=True)
    date_accepted = fields.Datetime('Date Accepted', readonly=True)
    
    @api.one
    def act_draft(self):
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(self.env.uid, self._name, 
                                self.id, 'cancel', self.env.cr)
    @api.one
    def act_accepted(self):
        self.sudo()
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(self.env.uid, self._name, 
                                self.id, 'accepted', self.env.cr)
    @api.one
    def act_to_accept(self):
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(self.env.uid, self._name, 
                                self.id, 'to_accept', self.env.cr)
    
    @api.one
    def term_draft(self):
        self.state = 'draft'
    
    @api.one
    def term_accepted(self):
        self.sudo()
        self.write({
            'state' : 'accepted',
            'date_accepted' : fields.Datetime.now()
            })
    
    @api.one
    def term_to_accept(self):
        self.state = 'to_accept'    
        self.date_accepted = False    
    
    @api.model
    def remind(self):
        domain = [('state', '=', 'to_accept')]
        terms = self.search(domain, order='contract_id')\
            .with_context(term_state = 'to_accept')\
            .mail_send()
    
    def _get_email_template(self):
        '''
        Extend this method for change default email template
        '''
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference(
                                #self.env.cr, 
                                #self.env.uid, 
                                'contract_term', 
                                'contract_term_reminder')[1]
        except ValueError:
            template_id = False
        return template_id    
    
    @api.multi
    def mail_send(self):
        '''
        Send e-mail directly
        '''
        res ={}
        mail_obj = self.env['mail.mail']
        mail_message_obj = self.env['mail.message']
        email_template_obj = self.env['email.template']
        email_compose_message_obj = self.env['mail.compose.message']
        partner_obj = self.env['res.partner']
        #user_obj = self.env['res.users']
        server_mail_obj = self.env['ir.mail_server']
        
        # template
        template_id = self._get_email_template()
        email_template = email_template_obj.browse(template_id)
        # old api user = user_obj.browse(cr, uid, uid)
        user = self.env.user
        for term in self:
            # user's mail server
            user_server_mail = False
            serv_ids = server_mail_obj.search([('smtp_user', '=', 
                                email_template.email_from)], 
                                order='sequence', limit=1)
            if serv_ids:
                user_server_mail = serv_ids
            # user's mail server
            '''
            if email_from_user_id.email:
                serv_ids = server_mail_obj.search(cr, uid, 
                        [('smtp_user', '=', 
                          email_from_user_id.email)], 
                        order='sequence', limit=1)
                if serv_ids:
                    user_server_mail = server_mail_obj.browse(cr, uid, 
                                                              serv_ids[0])'''
            # Recipients
            recipient_ids = []
            # ... partner
            recipient_ids.append(term.contract_id.partner_id.id)
            # ... followers
            ## >>>> non gestito x ora
            #for follower in node.message_follower_ids:
            #    recipient_ids.append(follower.id)
            # Compose E-mail
            for recipient in partner_obj.browse(recipient_ids):
                mail_subject = email_compose_message_obj.render_template(
                                    _(email_template.subject),
                                self._name, term.id)
                mail_body = email_compose_message_obj.render_template(
                                    _(email_template.body_html),
                                self._name, term.id)
                mail_to = email_compose_message_obj.render_template(
                                    _(email_template.email_to),
                                self._name, term.id)
                reply_to = email_compose_message_obj.render_template(
                                    _(email_template.reply_to),
                                self._name, term.id)
                #if email_from_user_id:
                #    mail_from = email_from_user_id.email
                #else:
                mail_from = email_compose_message_obj.render_template(
                                _(email_template.email_from),
                                'contract.term.contract', 
                                term.id)
                if not mail_from:
                    continue
                
                # body with data line
                message_vals = {
                        'type': 'email',
                        'email_from': mail_from,
                        'subject' : mail_subject,
                        'model' : self._name,
                        'res_id' : term.id,
                        'partner_ids' : recipient_ids 
                                        and [(6,0,recipient_ids)] or False,
                        'notified_partner_ids' : recipient_ids 
                                        and [(6,0,recipient_ids)] or False,
                        'type' : 'comment',
                        'body' : mail_body,
                        }
            
                message = mail_message_obj.create(message_vals, 
                                                     context=self.env.context)
                
                mail_id = mail_obj.create({
                        'mail_message_id' : message.id,
                        'mail_server_id' : user_server_mail 
                            and user_server_mail.id 
                            or email_template.mail_server_id 
                            and email_template.mail_server_id.id 
                            or False,
                        'state' : 'outgoing',
                        'auto_delete' : email_template.auto_delete,
                        'email_from' : mail_from,
                        'email_to' : recipient.email,
                        #'reply_to' : reply_to,
                        'body_html' : mail_body,
                        })
                if mail_id:
                    mail_obj.send([mail_id])
            
