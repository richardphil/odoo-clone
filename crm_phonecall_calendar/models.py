# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2014
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
from datetime import datetime, time, timedelta
import openerp

class calendar_event(models.Model):
    
    _inherit = 'calendar.event'
    
    phonecall_id = fields.Many2one('crm.phonecall', 'Phonecall',
        readonly=True,
        help="Event generated from phonecall")
    
    
class crm_phonecall(models.Model):
    
    _inherit = 'crm.phonecall'
    
    def _prepare_calendar(self, phonecall):
        data = {}
        # alarm
        alarms = [] 
        domain = [('duration_minutes', '=', 15), ('type', '=', 'notification')]
        alarm_ids = self.env['calendar.alarm'].search(domain, limit=1)
        if alarm_ids:
            alarms.append( (6, 0, [alarm_ids.id]) )
        # partners
        partners = [(6, 0, [phonecall.user_id.partner_id.id])]
        # type
        categs = []
        domain = [('name', '=', 'Phonecall')]
        type_ids = self.env['calendar.event.type'].search(domain, limit=1)
        if type_ids:
            categs.append( (6, 0, [type_ids.id]) )
        # All datas
        data = {
            'phonecall_id': phonecall.id,
            'name': phonecall.name,
            'description': phonecall.partner_phone,
            'partner_ids': partners,
            'categ_ids': categs,
            'alarm_ids': alarms,
            'user_id': phonecall.user_id.id,
            'start_datetime': phonecall.date,
            #'start_date': phonecall.date,
            'stop_datetime': phonecall.date,
            #'stop_date': phonecall.date,
            'allday': False,
            'duration': phonecall.duration or 10,
            'class': 'public',
        }
        return data
        
    @api.model
    #@api.one
    def create(self, values):
        '''
        Create event in calendar
        '''
        new_id = super(crm_phonecall, self).create(values)
        data = self._prepare_calendar(new_id)
        self.env['calendar.event'].create(data)
        return new_id

    @api.multi
    def write(self, values):
        '''
        Update event in calendar
        '''
        result = super(crm_phonecall, self).write(values)
        for pc in self:
            data = self._prepare_calendar(pc)
            domain = [('phonecall_id', '=', pc.id)]
            event_ids = self.pool['calendar.event'].search(
                                                self.env.cr,
                                                self.env.uid,
                                                domain)
            if not event_ids :
                self.pool['calendar.event'].create(
                                                self.env.cr,
                                                self.env.uid,
                                                data,
                                                context=None)
            else:
                self.pool['calendar.event'].write(
                                                 self.env.cr,
                                                 self.env.uid,
                                                 event_ids, 
                                                 data, 
                                                 context=None)
            
        return result
    