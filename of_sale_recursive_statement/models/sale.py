# -*- coding: utf-8 -*-
##############################################################################
#
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2014
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, orm
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime, time, timedelta
from dateutil.relativedelta import relativedelta
from openerp import netsvc
import openerp.addons.decimal_precision as dp
from openerp import workflow
import calendar

class product_uom(orm.Model):
    _inherit ="product.uom"

    _columns = {
        'method_delta_compute' : fields.selection([
            ('day', 'Compute delta with Days'),
            ('month', 'Compute delta with Months'),
            ('year', 'Compute delta with Years')
            ], 'Method delta Compute'),
    }

class sale_recursive_statement_product(orm.Model):
    _name="sale.recursive.statement.product"

    _columns = {
        'contract_id': fields.many2one('account.analytic.account',
            'Contract', readonly=True, ondelete='cascade'),
        'partner_id': fields.many2one('res.partner', 'Partner'),
        'product_id': fields.many2one('product.product', 'Product'),
        'parent_id': fields.many2one('sale.recursive.statement.product',
                                     'Parent'),
        'line_order_id': fields.many2one('sale.order.line',
            'Sale Order Line', readonly=True, ondelete='cascade'),
        'name': fields.text('Description'),
        'price_unit': fields.float('Unit Price', required=True,
                        digits_compute= dp.get_precision('Product Price')),
        'discount': fields.float('Discount (%)',
                        digits_compute= dp.get_precision('Discount')),
        'discount_number_lines': fields.integer('Discount Nr Lines'),
        'qty': fields.float('Quantity (UoS)',
                        digits_compute=dp.get_precision('Product UoS')),
        'uos': fields.many2one('product.uom', 'Product UoS'),
        'tax_id': fields.many2many('account.tax',
                'sale_recursive_statement_product_tax', 'statement_product_id',
                'tax_id', 'Taxes'),
        'frequency': fields.integer('Frequency'),
        'frequency_uom': fields.many2one('product.uom', 'Frequency UoM'),
        'date_start': fields.date('Date Start', required=True),
        'date_stop': fields.date('Date Stop'),
        'line_policy': fields.selection([
            ('fwoh', 'First Whole, Other Hook'),
            ('fwsroh', 'First Whole, Second proportional refund, Other hook'),
            ('indi', 'Indipendent'),
            ], 'Line Policy', required=True),
        'ref_to_invoice': fields.boolean('Product ref for invoice'),
    }

    _defaults = {
        'line_policy' :'fwoh'
    }

    def onchange_product_id(self, cr, uid, ids, product_id, context=None):
        product_obj = self.pool['product.product']
        user_obj = self.pool['res.users']
        res = {}
        res['value'] = {}

        if product_id:
            product = product_obj.browse(cr, uid, product_id)
            user = user_obj.browse(cr, uid, uid, context)
            company = user.company_id
            partner_id = context.get('partner_id')
            partner = self.pool['res.partner'].browse(cr, uid, partner_id)

            # Price
            price_unit = 0
            so_line_ids = []
            pricelist = partner.property_product_pricelist.id
            so_qty = 1
            so_uom = product.uom_id.id
            so_lang = partner.lang
            so_date = datetime.today().strftime('%Y-%m-%d')
            so_name = product.name
            update_tax =False
            packaging =False
            fiscal_position = partner.property_account_position.id
            flag = False
            pl = self.pool['sale.order.line'].product_id_change(
                cr, uid, so_line_ids, pricelist, product_id, so_qty, so_uom,
                so_qty, so_uom, so_name, partner_id, so_lang, update_tax,
                so_date, packaging, fiscal_position, flag, context)
            if pl['value'] and pl['value']['price_unit']:
                price_unit = pl['value']['price_unit']
            else:
                price_unit = product.list_price
            # Product
            res['value'].update({'name': product.name})
            res['value'].update({'price_unit': price_unit})
            res['value'].update({'uos': product.uom_id.id})
            res['value'].update({'qty': 1})
            # Default frequency
            if product.recursive_statement_line_policy:
                res['value'].update({'line_policy':
                    product.recursive_statement_line_policy})
            else:
                res['value'].update({'line_policy':
                    company.of_rec_statement_line_policy})
            if product.recursive_statement_frequency:
                res['value'].update({'frequency':
                    product.recursive_statement_frequency})
            else:
                res['value'].update({'frequency':
                    company.of_rec_statement_default_frequency})

            if product.recursive_statement_frequency_uom:
                res['value'].update({'frequency_uom':
                    product.recursive_statement_frequency_uom.id})
            else:
                res['value'].update({'uom':
                    company.of_rec_statement_default_frequency_uom_id.id})
        return res

    def onchange_parent_id(self, cr, uid, ids, parent_id, context=None):
        '''
        set same frequency datas from parent
        '''
        res = {}
        res['value'] = {}
        if parent_id:
            parent = self.browse(cr, uid, parent_id)
            '''
            if parent.line_policy:
                res['value'].update({'line_policy': parent.line_policy})
            if parent.frequency:
                res['value'].update({'frequency': parent.frequency})
            if parent.frequency_uom:
                res['value'].update({'frequency_uom': parent.frequency_uom.id})
            '''
        return res

    def onchange_rec_discount(self, cr, uid, ids, discount, context=None):
        '''
        Control limit discount
        '''
        user = self.pool['res.users'].browse(cr, uid, uid, context)
        company = user.company_id
        if discount and company.of_rec_statement_limit_discount:
            if discount > company.of_rec_statement_limit_discount:
                raise orm.except_orm(_('Error!'),
                                         _('The discount exceeds the limit'))

        return True

    def onchange_rec_discount_number_lines(self, cr, uid, ids,
                                           nr_lines, context=None):
        '''
        Control limit discount nr lines
        '''
        user = self.pool['res.users'].browse(cr, uid, uid, context)
        company = user.company_id

        if nr_lines and company.of_rec_statement_limit_discount_number_lines:
            if nr_lines > company.of_rec_statement_limit_discount_number_lines:
                raise orm.except_orm(_('Error!'),
                                         _('The lines of discount exceeds the \
                                         limit'))
        return True

    def _prepare_statement_line(self, cr, uid, line, line_product,line_sequence,
                               date_line,
                               next_date_line,
                               context=None):

        return line

    def prepare_statement_line(self, cr, uid, line_product,
                               line_sequence,
                               date_line,
                               next_date_line,
                               context=None):
        product_uom_obj = self.pool['product.uom']
        dp_obj = self.pool['decimal.precision']
        statement_line_obj = self.pool['sale.recursive.statement.line']
        res ={}

        # Compute sequence from lines existing
        domain = [('contract_id', '=', line_product.contract_id.id),
                  ('recursive_product_id', '=', line_product.id)]
        nr_line_ids = statement_line_obj.search(cr, uid, domain)
        if len(nr_line_ids) > 0:
            line_sequence = len(nr_line_ids) + 1
        else:
            line_sequence = 1

        # Date competence
        date_competence_start = date_line
        date_competence_stop = datetime.strptime(next_date_line,"%Y-%m-%d")\
                + timedelta(days=-1)
        date_competence_stop = date_competence_stop.strftime('%Y-%m-%d')

        # Rate far date stop before last period
        days_freq_coeff = 1.0
        date_end_rate = False

        if line_product.contract_id.date:
            date_end_rate = line_product.contract_id.date
        elif line_product.date_stop:
            date_end_rate = line_product.date_stop
        if date_end_rate \
                and date_competence_start <= date_end_rate\
                and date_end_rate <= date_competence_stop:
            date_start = datetime.strptime(date_line, "%Y-%m-%d")
            date_end_rate = datetime.strptime(date_end_rate, "%Y-%m-%d")
            delta_date = date_end_rate - date_start
            days_freq_coeff = round(delta_date.days * 1.0 /
                        line_product.frequency,
                        dp_obj.precision_get(cr, uid, 'Product UoS'))
            date_competence_stop = date_end_rate

        # Qty second frequency
        line_qty = line_product.qty
        if not line_product.uos.id == line_product.frequency_uom.id:
            line_qty = product_uom_obj._compute_qty(cr, uid,
                line_product.frequency_uom.id, line_product.qty,
                line_product.uos.id,
                dict(context.items() + [('raise-exception', False)]))
        line_qty = line_qty * line_product.frequency

        # Refund in the second line ( first line to group)
        # Rec Product ref (first in order to create)
        amount_to_refund = 0
        refund_descritption = ''
        rec_product_ref = self.get_recursive_product_ref(
            cr, uid, line_product.contract_id.id, context)
        if line_product.line_policy in ['fwsroh']\
                    and rec_product_ref\
                    and line_sequence == 2\
                    and not line_product.id == rec_product_ref.id:
            # first date for proportional amount
            domain=[('contract_id', '=', \
                     line_product.contract_id.id),
                    ('recursive_product_id', '=', \
                     line_product.id),
                    ('line_sequence', '=', 1)]
            first_line_ids = statement_line_obj.search(
                cr, uid, domain, limit=1)
            if first_line_ids:
                first_line = statement_line_obj.browse(
                    cr, uid, first_line_ids[0])
                # Days used from first line
                rfl_date_line = datetime.strptime(date_line,
                                                "%Y-%m-%d")
                rfl_last_date_used = rfl_date_line - timedelta(days=1)
                # Days of period
                rfl_date = datetime.strptime(first_line.date,
                                                "%Y-%m-%d")
                # Date start precedent period
                delta_type = line_product.uos.method_delta_compute
                if delta_type == 'day':
                    rfl_date_start = rfl_date_line - timedelta(
                        days=line_product.frequency)
                elif delta_type == 'month':
                    rfl_date_start = rfl_date_line - relativedelta(
                        months=line_product.frequency)
                elif delta_type == 'year':
                    rfl_date_start = rfl_date_line - relativedelta(
                        years=line_product.frequency)
                rfl_period_days = (rfl_date_line - rfl_date_start).days
                # Used/Unused days
                used_days = (rfl_date_line - rfl_date).days
                unused_days = rfl_period_days - used_days
                # Price ref to month
                price_unit = first_line.price_unit
                if line_product.uos.method_delta_compute == 'year':
                    price_unit = round(price_unit / 12,
                                       dp_obj.precision_get(cr, uid, 'Account'))
                elif line_product.uos.method_delta_compute == 'day':
                    price_unit = round(price_unit * rfl_period_days,
                                       dp_obj.precision_get(cr, uid, 'Account'))
                # Quote to refund
                amount_to_refund = round(unused_days * \
                    (price_unit / rfl_period_days),
                    dp_obj.precision_get(cr, uid, 'Account'))
                if amount_to_refund:
                    refund_descritption = _(" (%s with Refund of %s for used service \
                    only from %s to %s)" % (str(first_line.price_unit),
                                            str(amount_to_refund),
                                            rfl_date.strftime("%d-%m-%Y"),
                                            rfl_last_date_used.strftime(\
                                                "%d-%m-%Y")
                                             ))
        # Description line
        line_description = line_product.name
        if refund_descritption:
            line_description += ' ' + refund_descritption
        # Discount: to control limit nr lines
        discount = line_product.discount
        if line_product.discount_number_lines \
            and line_sequence > line_product.discount_number_lines:
            discount = False

        res = {
            'recursive_product_id': line_product.id,
            'date': date_line,
            'line_sequence': line_sequence,
            'period_date_start': date_competence_start,
            'period_date_end': date_competence_stop,
            'product_id': line_product.product_id.id,
            'name': line_description,
            'price_unit': line_product.price_unit - amount_to_refund,
            'uos': line_product.uos.id,
            'discount': discount,
            #'qty': line_product.qty * qty_freq_coeff * days_freq_coeff,
            'qty': line_qty,
            'tax_id': [(6, 0, [x.id for x in line_product.tax_id])]
        }
        res = self._prepare_statement_line(cr, uid, res, line_product,
                                           line_sequence,
                                           date_line,
                                           next_date_line,
                                           context=None)

        return res

    def compute_statement_line(self, cr, uid, ids, context=None):
        '''
        Compute for fiscal year active
        '''
        period_obj = self.pool['account.period']
        product_uom_obj = self.pool['product.uom']
        statement_line_obj = self.pool['sale.recursive.statement.line']
        users_obj = self.pool['res.users']

        user = users_obj.browse(cr, uid, uid, context=context)
        company = user.company_id

        if not context:
            context = {}
        contract_id = context.get('contract_id',False)
        if not ids:
            domain = [('id', '>', 0)]
            if contract_id :
                domain.append(('contract_id', '=', contract_id))
                ids = self.search(cr, uid, domain)

        p_id = period_obj.search(cr, uid,
                                 [('special', '=', False)],
                                 limit=1,
                                 order='date_stop desc')
        period = period_obj.browse(cr, uid, p_id)

        # Unlink lines to recompute
        # NB: The lines chained to draft statment are deleted in
        #     recompute_statement
        domain = [('statement_id', '=', False)]
        line_to_recompute_ids = statement_line_obj.search(cr, uid, domain)
        statement_line_obj.unlink(cr, uid, line_to_recompute_ids)
        count = 1
        for line_product in self.browse(cr, uid, ids):
            # print line_product.name
            # print line_product.id
            # if line_product.id == 76:
            #    import pdb
            #    pdb.set_trace()
            count += 1
            # Rec Product ref (first in order to create)
            rec_product_ref = self.get_recursive_product_ref(cr, uid,
                                                        contract_id, context)
            date_line = line_product.date_start
            date_line_start = line_product.date_start
            # Setting date limit
            date_limit = period.date_stop
            if line_product.date_stop:
                date_limit = line_product.date_stop
            # Setting line sequence number
            line_sequence = 1
            # Loop date for creation lines of statement
            while (date_line <= date_limit) :
                # Date to hook from product ref
                if line_product.line_policy in ['fwoh', 'fwsroh'] \
                    and rec_product_ref\
                    and line_sequence >= 2 \
                    and not line_product.id == rec_product_ref.id:
                        # To avoid date hook is the same of product rec's first line
                        lp_first_date_line = False
                        domain=[('contract_id', '=', line_product.contract_id.id),
                                ('recursive_product_id', '=', line_product.id)]
                        lp_first_line_ids = statement_line_obj.search(
                            cr, uid, domain, order='date, id', limit=1)
                        if lp_first_line_ids:
                            lp_first_line = statement_line_obj.browse(
                                cr, uid, lp_first_line_ids[0])
                            lp_first_date_line = lp_first_line.date
                        # Search Date to hook
                        p_date_line = date_line
                        # ... Set first date to hook the second date
                        if line_sequence == 2:
                            p_date_line = line_product.date_start
                        data_line_ref = self.get_product_ref_date_next(
                            cr, uid, rec_product_ref, line_product, p_date_line,
                            context)
                        if data_line_ref:
                            date_line = data_line_ref

                # Compute next line date
                # ...days-month-year delta
                delta_type = False
                if not line_product.frequency_uom.method_delta_compute:
                    raise orm.except_orm(_('Error!'),
                                         _('Delta Method compute missing in \
                                         UOM: "%s" ') % \
                                         (line_product.frequency_uom.name,))
                delta_type = line_product.frequency_uom.method_delta_compute
                start_date = datetime.strptime(date_line, "%Y-%m-%d")
                if delta_type == 'day':
                    end_date = start_date + timedelta(
                        days=line_product.frequency)
                elif delta_type == 'month':
                    end_date = start_date + relativedelta(
                        months=line_product.frequency)
                elif delta_type == 'year':
                    end_date = start_date + relativedelta(
                        years=line_product.frequency)
                next_date_line = end_date.strftime('%Y-%m-%d')
                # Skip if
                to_skip = False
                # ..already exists (lines not in draft)
                domain = [('contract_id', '=', line_product.contract_id.id),
                          ('recursive_product_id', '=', line_product.id),
                          ('date', '=', date_line)]
                rs_line_ids = self.pool['sale.recursive.statement.line'].search(
                    cr, uid, domain)
                if rs_line_ids:
                    to_skip = True
                # Create Line
                if not to_skip \
                    and date_line >= date_line_start:
                    vals = self.prepare_statement_line(cr, uid, line_product,
                                                       line_sequence,
                                                       date_line,
                                                       next_date_line,
                                                       context)
                    st_line_id = statement_line_obj.create(cr, uid, vals)
                # Next line
                date_line = next_date_line
                line_sequence += 1

        return True

    def get_recursive_product_ref(self, cr, uid, contract_id, context=None):
        '''
        Return the recursive product or ref to compute line of other
        product in the contract
        '''
        # Rec Product ref (from ref to invoice)
        domain=[('contract_id', '=', contract_id),
                ('ref_to_invoice', '=', True)]
        rec_product_ref_ids = self.search(cr, uid,
                                domain, order='id', limit=1)
        # Rec Product ref (first in order to create)
        if not rec_product_ref_ids:
            domain=[('contract_id', '=', contract_id)]
            rec_product_ref_ids = self.search(cr, uid,
                                    domain, order='id', limit=1)
        rec_product_ref = False
        if rec_product_ref_ids:
            rec_product_ref = self.browse(cr, uid, rec_product_ref_ids[0])
        return rec_product_ref

    def get_product_ref_date_next(self, cr, uid, product_ref,
                                            line_product, date, context=None):
        '''
        First valid date of product ref after date passed
        '''
        if not date:
            return False
        date_new = False

        date_line = product_ref.date_start
        while (date_line < date) :
            # Increase with frequency of line product
            # Compute next line date
            # ...days-month-year delta
            delta_type = False
            if not line_product.frequency_uom.method_delta_compute:
                raise orm.except_orm(_('Error!'),
                                     _('Delta Method compute missing in \
                                     UOM: "%s" ') % \
                                     (line_product.frequency_uom.name,))
            delta_type = line_product.frequency_uom.method_delta_compute
            start_date = datetime.strptime(date_line, "%Y-%m-%d")
            if delta_type == 'day':
                end_date = start_date + timedelta(
                    days=line_product.frequency)
            elif delta_type == 'month':
                end_date = start_date + relativedelta(
                    months=line_product.frequency)
            elif delta_type == 'year':
                end_date = start_date + relativedelta(
                    years=line_product.frequency)
            next_date_line = end_date.strftime('%Y-%m-%d')

            date_line = next_date_line

        return date_line


class sale_recursive_statement_line(orm.Model):
    _name="sale.recursive.statement.line"

    def _calc_line_base_price(self, cr, uid, line, context=None):
        return line.price_unit * (1 - (line.discount or 0.0) / 100.0)

    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        res = {}
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            price = self._calc_line_base_price(cr, uid, line, context=context)
            price = price * line.qty
            taxes = tax_obj.compute_all(cr, uid, line.tax_id, price,
                line.qty, line.product_id, line.recursive_product_id.partner_id)
            cur = False
            if line.recursive_product_id.line_order_id and \
                line.recursive_product_id.line_order_id.order_id:
                sale_order = line.recursive_product_id.line_order_id.order_id
                cur = sale_order.pricelist_id.currency_id
            else:
                cur = line.contract_id.currency_id
            #res[line.id] = cur_obj.round(cr, uid, cur, taxes['total'])
            res[line.id] = cur_obj.round(cr, uid, cur, price)
        return res

    _columns = {
        'statement_id': fields.many2one('sale.recursive.statement',
                            'Statement', readonly=True, ondelete="cascade"),
        'recursive_product_id':
                        fields.many2one('sale.recursive.statement.product',
                            'Statement', readonly=True),
        'date': fields.date('Date'),
        'period_date_start': fields.date('Date Start'),
        'period_date_end': fields.date('Date End'),
        'product_id': fields.many2one('product.product', 'Product'),
        'line_order_id': fields.related('recursive_product_id',
                'line_order_id', type='many2one', relation='sale.order.line',
                string="Sale Order Line", readonly=True, store=True),
        'order_id': fields.related('recursive_product_id', 'line_order_id',
                'order_id', type='many2one', relation='sale.order',
                string="Sale Order", readonly=True, store=True),
        'line_sequence': fields.integer('Line sequence', readonly=True),
        'line_policy': fields.related('recursive_product_id', 'line_policy',
                type='char', string="Line Policy", readonly=True, store=True),
        'contract_id': fields.related('recursive_product_id', 'contract_id',
                type='many2one', relation='account.analytic.account',
                string="Contract", readonly=True, store=True),
        'name': fields.text('Description'),
        'price_unit': fields.float('Unit Price', required=True,
                        digits_compute= dp.get_precision('Product Price')),
        'discount': fields.float('Discount (%)',
                        digits_compute= dp.get_precision('Discount')),
        'qty': fields.float('Quantity (UoS)',
                        digits_compute=dp.get_precision('Product UoS')),
        'uos': fields.many2one('product.uom', 'Product UoS'),
        'tax_id': fields.many2many('account.tax',
            'sale_recursive_statement_line_tax', 'statement_product_id',
            'tax_id', 'Taxes'),
        'subtotal': fields.function( _amount_line, string='Subtotal',
                        digits_compute= dp.get_precision('Account')),
    }

    _order = "date"

    def _prepare_statement_line_invoice_line(self, cr, uid, line,
                                             account_id=False, context=None):
        """Prepare the dict of values to create the new invoice line for a
           sales order line. This method may be overridden to implement custom
           invoice generation (making sure to call super() to establish
           a clean extension chain).

           :param browse_record line: sale.order.line record to invoice
           :param int account_id: optional ID of a G/L account to force
               (this is used for returning products including service)
           :return: dict of values to create() the invoice line
        """
        res = {}
        if not line.statement_id.invoice_id:
            if not account_id:
                if line.product_id:
                    account_id = line.product_id.property_account_income.id
                    if not account_id:
                        account_id = line.product_id.categ_id.property_account_income_categ.id
                    if not account_id:
                        raise osv.except_osv(_('Error!'),
                                _('Please define income account for this product: "%s" (id:%d).') % \
                                    (line.product_id.name, line.product_id.id,))
                else:
                    prop = self.pool.get('ir.property').get(cr, uid,
                            'property_account_income_categ', 'product.category',
                            context=context)
                    account_id = prop and prop.id or False
            '''
            uosqty = self._get_line_qty(cr, uid, line, context=context)
            uos_id = self._get_line_uom(cr, uid, line, context=context)
            pu = 0.0
            if uosqty:
                pu = round(line.price_unit * line.product_uom_qty / uosqty,
                        self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Price'))
            '''
            fpos = line.order_id.fiscal_position or False
            account_id = self.pool.get('account.fiscal.position').map_account(cr, uid, fpos, account_id)
            if not account_id:
                raise osv.except_osv(_('Error!'),
                            _('There is no Fiscal Position defined or Income category account defined for default properties of Product categories.'))
            res = {
                'name': line.name,
                'sequence': line.line_sequence,
                'origin': line.order_id.name,
                'account_id': account_id,
                #'price_unit': pu,
                'price_unit': line.price_unit,
                #'quantity': uosqty,
                'quantity': line.qty,
                'discount': line.discount or 0,
                'uos_id': line.uos.id,
                'product_id': line.product_id.id or False,
                'invoice_line_tax_id': [(6, 0, [x.id for x in line.tax_id])],
                ##'account_analytic_id': line.order_id.project_id and line.order_id.project_id.id or False,
                'account_analytic_id': line.statement_id.contract_id and line.statement_id.contract_id.id or False,
            }

        return res

    def invoice_line_create(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        create_ids = []
        sales = set()
        for line in self.browse(cr, uid, ids, context=context):
            vals = self._prepare_statement_line_invoice_line(cr, uid, line,
                                                             False, context)
            if vals:
                inv_id = self.pool.get('account.invoice.line').create(cr, uid, vals, context=context)
                self.write(cr, uid, [line.id], {'invoice_lines': [(4, inv_id)]}, context=context)
                sales.add(line.order_id.id)
                create_ids.append(inv_id)
        # Trigger workflow events
        ##for sale_id in sales:
        ##    workflow.trg_write(uid, 'sale.order', sale_id, cr)
        return create_ids

class sale_recursive_statement(orm.Model):
    _name="sale.recursive.statement"
    _inherit = ['mail.thread']
    def _get_children(self, cr, uid, ids, context=None):
        # Get statemnt of line with variations
        statement_line_obj = self.pool['sale.recursive.statement.line']
        st_ids = []
        for st_line in statement_line_obj.browse(cr, uid, ids):
            if not st_line.statement_id:
                continue
            st_ids.append(st_line.statement_id.id)
        return st_ids

    def _is_first_recursive_sale_order(self, cr, uid, ids, field_name, arg, context=None):
        '''
        First statement created by sale order
        '''
        res = {}
        first_statement = False
        for statement in self.browse(cr, uid, ids):
            so_save = False
            # Even one ref to sale order
            for stl in statement.line_ids:
                if stl.order_id and stl.line_sequence == 1:
                    first_statement = True
                    so_save = stl.order_id.id
                if not stl.order_id.id == so_save:
                    first_statement = False
                    break
            res[statement.id] = first_statement
        return res

    def _compute_total(self, cr, uid, ids, field_name, arg, context=None):
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        dp_obj = self.pool['decimal.precision']
        res = {}
        if context is None:
            context = {}
        for statement in self.browse(cr, uid, ids, context=context):
            amount_total = amount_untaxed = amount_tax = 0
            for line in statement.line_ids:
                #price = line.price_unit * line.qty
                taxes = tax_obj.compute_all(cr, uid, line.tax_id, line.price_unit,
                    line.qty, line.product_id, line.recursive_product_id.partner_id)
                for tax in taxes['taxes']:
                    amount_tax += round( tax['amount'],
                                        dp_obj.precision_get(cr, uid, 'Account'))
                amount_untaxed += line.subtotal
            amount_total = amount_untaxed + amount_tax
            res[statement.id] = {
                    'amount_untaxed' : amount_untaxed,
                    'amount_tax' : amount_tax,
                    'amount_total' : amount_total,
                    }
        return res

    _columns = {
        'state': fields.selection([
            ('draft', 'Draft'),
            ('sent', 'Sent'),
            ('paid', 'Paid'),
            ('invoiced', 'Invoiced'),
            ], 'Status', readonly=True, copy=False, select=True,
                                  track_visibility='onchange'),
        'date': fields.date('Date', readonly=True, states={'draft':[('readonly',False)]}),
        'number':  fields.char('Number', size=64, readonly=True, states={'draft':[('readonly',False)]}),
        'line_ids': fields.one2many('sale.recursive.statement.line',
            'statement_id', 'Recursive Products', readonly=True, ondelete='cascade',
            states={'draft':[('readonly',False)]}),
        'contract_id': fields.many2one('account.analytic.account',
            'Contract', readonly=True, ondelete='cascade'),
        'partner_id': fields.many2one('res.partner',
            'Partner', readonly=True),
        'payment_term': fields.many2one('account.payment.term', 'Payment Term'),
        'amount_untaxed': fields.function(_compute_total, string='Amount Untaxed',
                                       multi='total',
            store={
                    'sale.recursive.statement.line':
                        (_get_children, ['statement_id','price_unit', 'qty'], 10),
                   }),
        'amount_tax': fields.function(_compute_total, string='Amount Tax',
                                      multi='total',
            store={
                    'sale.recursive.statement.line':
                        (_get_children, ['statement_id','price_unit', 'qty'], 10),
                   }),
        'amount_total': fields.function(_compute_total, string='Amount Total',
                                        multi='total', track_visibility='onchange',
            store={
                    'sale.recursive.statement.line':
                        (_get_children, ['statement_id','price_unit', 'qty'], 10),
                   }),
        'invoice_id': fields.many2one('account.invoice',
                        'Invoice', readonly=True),
        'invoice_date': fields.related('invoice_id', 'date_invoice',
            type="date", string="Date invoice", readonly=True, store=True),
        'first_recursive_sale_order': fields.function(
                    _is_first_recursive_sale_order, string='Fist Recursive Sale order',
                    type='boolean'),
        'company_id': fields.many2one('res.company', 'Company'),
        'user_id': fields.many2one('res.users', 'User'),
        }
    _defaults = {
        'state': 'draft'
        }

    def create(self, cr, uid, vals, *args, **kwargs):
        sequence_obj = self.pool['ir.sequence']
        if not 'number' in vals or not vals['number']:
            vals['number'] = sequence_obj.get(cr, uid, 'sale.recursive.statement')
        res_id = super(sale_recursive_statement,self).create(cr, uid, vals, *args, **kwargs)
        return res_id

    def action_invoice_create(self, cr, uid, ids, grouped=False, states=None, date_invoice = False, context=None):
        if states is None:
            states = ['confirmed', 'done', 'exception']
        res = False
        invoices = {}
        invoice_ids = []
        invoice = self.pool.get('account.invoice')
        ##obj_sale_order_line = self.pool.get('sale.order.line')
        statement_line_obj = self.pool.get('sale.recursive.statement.line')
        partner_currency = {}
        # If date was specified, use it as date invoiced, usefull when invoices are generated this month and put the
        # last day of the last month as invoice date
        if date_invoice:
            context = dict(context or {}, date_invoice=date_invoice)
        for o in self.browse(cr, uid, ids, context=context):
            ##currency_id = o.pricelist_id.currency_id.id
            currency_id = o.contract_id.pricelist_id.currency_id.id
            if (o.partner_id.id in partner_currency) and (partner_currency[o.partner_id.id] <> currency_id):
                raise orm.except_orm(
                    _('Error!'),
                    _('You cannot group sales having different currencies for the same partner.'))

            partner_currency[o.partner_id.id] = currency_id
            lines = []
            ##for line in o.order_line:
            for line in o.line_ids:
                ##if line.invoiced:
                ##    continue
                ##elif (line.state in states):
                ##    lines.append(line.id)
                lines.append(line.id)
            ##created_lines = obj_sale_order_line.invoice_line_create(cr, uid, lines)
            created_lines = statement_line_obj.invoice_line_create(cr, uid, lines)
            if created_lines:
                invoices.setdefault(o.partner_id.id, []).append((o, created_lines))
        if not invoices:
            for o in self.browse(cr, uid, ids, context=context):
                return o.invoice_id.id
                ##for i in o.invoice_ids:
                ##    if i.state == 'draft':
                ##        return i.id
        for val in invoices.values():
            '''
            if grouped:
                res = self._make_invoice(cr, uid, val[0][0], reduce(lambda x, y: x + y, [l for o, l in val], []), context=context)
                invoice_ref = ''
                origin_ref = ''
                for o, l in val:
                    invoice_ref += (o.client_order_ref or o.name) + '|'
                    origin_ref += (o.origin or o.name) + '|'
                    self.write(cr, uid, [o.id], {'state': 'progress'})
                    cr.execute('insert into sale_order_invoice_rel (order_id,invoice_id) values (%s,%s)', (o.id, res))
                    self.invalidate_cache(cr, uid, ['invoice_ids'], [o.id], context=context)
                #remove last '|' in invoice_ref
                if len(invoice_ref) >= 1:
                    invoice_ref = invoice_ref[:-1]
                if len(origin_ref) >= 1:
                    origin_ref = origin_ref[:-1]
                invoice.write(cr, uid, [res], {'origin': origin_ref, 'name': invoice_ref})
            else:
                for order, il in val:
                    res = self._make_invoice(cr, uid, order, il, context=context)
                    invoice_ids.append(res)
                    self.write(cr, uid, [order.id], {'state': 'progress'})
                    cr.execute('insert into sale_order_invoice_rel (order_id,invoice_id) values (%s,%s)', (order.id, res))
                    self.invalidate_cache(cr, uid, ['invoice_ids'], [order.id], context=context)'''
            for statement, il in val:
                    res = self._make_invoice(cr, uid, statement, il, context=context)
                    invoice_ids.append(res)
                    ##self.write(cr, uid, [statement.id], {'state': 'progress'})
                    val = {
                        'state' : 'invoiced',
                        'invoice_id' : res
                    }
                    self.write(cr, uid, [statement.id], val)
                    ##cr.execute('insert into sale_order_invoice_rel (order_id,invoice_id) values (%s,%s)', (statement.id, res))
                    ##self.invalidate_cache(cr, uid, ['invoice_ids'], [statement.id], context=context)
        return res

    def _prepare_invoice(self, cr, uid, statement, lines, context=None):
        """Prepare the dict of values to create the new invoice for a
           sales order. This method may be overridden to implement custom
           invoice generation (making sure to call super() to establish
           a clean extension chain).

           :param browse_record order: sale.order record to invoice
           :param list(int) line: list of invoice line IDs that must be
                                  attached to the invoice
           :return: dict of value to create() the invoice
        """
        if context is None:
            context = {}
        journal_ids = self.pool.get('account.journal').search(cr, uid,
            [('type', '=', 'sale'), ('company_id', '=', statement.company_id.id)],
            limit=1)
        if not journal_ids:
            raise orm.except_orm(_('Error!'),
                _('Please define sales journal for this company: "%s" (id:%d).') % (statement.company_id.name, statement.company_id.id))
        invoice_vals = {
            ##'name': order.client_order_ref or '',
            'name': statement.number or '',
            'origin': statement.number,
            'type': 'out_invoice',
            ##'reference': order.client_order_ref or order.name,
            'reference': statement.number,
            'account_id': statement.partner_id.property_account_receivable.id,
            #'partner_id': order.partner_invoice_id.id,
            'partner_id': statement.partner_id.id,
            'journal_id': journal_ids[0],
            'invoice_line': [(6, 0, lines)],
            'currency_id': statement.contract_id.pricelist_id.currency_id.id,
            ##'comment': order.note,
            ##'payment_term': order.payment_term and order.payment_term.id or False,
            'payment_term': statement.payment_term and statement.payment_term.id or False,
            ##'fiscal_position': order.fiscal_position.id or order.partner_id.property_account_position.id,
            'fiscal_position': statement.partner_id.property_account_position.id,
            'date_invoice': context.get('date_invoice', False),
            'company_id': statement.company_id.id,
            'user_id': statement.user_id and statement.user_id.id or False,
            ##'section_id' : order.section_id.id
        }

        # Care for deprecated _inv_get() hook - FIXME: to be removed after 6.1
        ##invoice_vals.update(self._inv_get(cr, uid, statement, context=context))
        return invoice_vals

    def _make_invoice(self, cr, uid, statement, lines, context=None):
        inv_obj = self.pool.get('account.invoice')
        obj_invoice_line = self.pool.get('account.invoice.line')
        if context is None:
            context = {}
        '''
        invoiced_sale_line_ids = self.pool.get('sale.order.line').search(cr, uid, [('order_id', '=', order.id), ('invoiced', '=', True)], context=context)
        from_line_invoice_ids = []
        for invoiced_sale_line_id in self.pool.get('sale.order.line').browse(cr, uid, invoiced_sale_line_ids, context=context):
            for invoice_line_id in invoiced_sale_line_id.invoice_lines:
                if invoice_line_id.invoice_id.id not in from_line_invoice_ids:
                    from_line_invoice_ids.append(invoice_line_id.invoice_id.id)
        for preinv in order.invoice_ids:
            if preinv.state not in ('cancel',) and preinv.id not in from_line_invoice_ids:
                for preline in preinv.invoice_line:
                    inv_line_id = obj_invoice_line.copy(cr, uid, preline.id, {'invoice_id': False, 'price_unit': -preline.price_unit})
                    lines.append(inv_line_id)
        '''
        inv = self._prepare_invoice(cr, uid, statement, lines, context=context)
        inv_id = inv_obj.create(cr, uid, inv, context=context)
        data = inv_obj.onchange_payment_term_date_invoice(cr, uid, [inv_id],
              ##inv['payment_term'], time.strftime(DEFAULT_SERVER_DATE_FORMAT))
              inv['payment_term'], False)
        if data.get('value', False):
            inv_obj.write(cr, uid, [inv_id], data['value'], context=context)
        inv_obj.button_compute(cr, uid, [inv_id])
        return inv_id

    def _get_email_template(self, cr, uid, ids, context=None):
        '''
        Extend this method for change default email template
        '''
        ir_model_data = self.pool.get('ir.model.data')
        template_id = False
        try:
            template_id = ir_model_data.get_object_reference(cr, uid,
                                    'of_sale_recursive_statement',
                                    'of_recursive_statement_email_template')[1]
        except ValueError:
            template_id = False
        return template_id

    def action_send_statement(self, cr, uid, ids, context=None):

        #self.signal_workflow(cr, uid, ids, 'statement_send')
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        '''
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        ir_model_data = self.pool.get('ir.model.data')

        template_id = self._get_email_template(cr, uid, ids, context)

        try:
            compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'sale.recursive.statement',
            'default_res_id': ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True
        })

        for statement in self.browse(cr, uid, ids):
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, 'sale.recursive.statement', statement.id, 'statement_send', cr)

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

        return True

    def action_set_to_draft(self, cr, uid, ids, *args):
        # Controls to avoid reset to draft
        for st in self.browse(cr, uid, ids):
            if st.invoice_id:
                raise orm.except_orm(_('Error!'),
                        _('Before. You must delete the invoice'))
        self.write(cr, uid, ids, {'state': 'draft'})
        wf_service = netsvc.LocalService('workflow')
        for id in ids:
            wf_service.trg_create(uid, self._name, id, cr)
        return True

    def action_paid(self, cr, uid, ids, context=None):
        for pt in self.browse(cr, uid, ids):
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, self._name,
                                    pt.id, 'paid', cr)
        return True

    def statement_paid(self, cr, uid, ids, *args):
        for statement in self.browse(cr, uid, ids):
            if statement.state in ['draft', 'sent']:
                self.write(cr, uid, [statement.id], {'state': 'paid'})
        return True

    def statement_sent(self, cr, uid, ids, *args):
        for statement in self.browse(cr, uid, ids):
            if statement.state in ['draft']:
                self.write(cr, uid, [statement.id], {'state': 'sent'})
        return True

    def statement_invoice_create(self, cr, uid, ids, *args):
        sale_order_obj = self.pool['sale.order']

        for statement in self.browse(cr, uid, ids):
            #
            # First statement linked to sale order -> create invoice from
            # sale order
            if statement.first_recursive_sale_order:
                order_ids = []
                order_ids.append(statement.line_ids[0].order_id.id)
                grouped = False
                invoice_id = sale_order_obj.action_invoice_create(cr, uid,
                                                     order_ids,
                                                     grouped,
                                                     date_invoice=None)
                sale_order_obj.action_done(cr, uid, order_ids, context=None)
            # ELSE create invoice from statement
            else:
                invoice_id = self.action_invoice_create(cr, uid,
                                        [statement.id],
                                        grouped = None,
                                        states = None,
                                        date_invoice = None,
                                        context = None)
            # Validate Invoice
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, 'account.invoice', invoice_id,
                                                'invoice_open', cr)
            #
            vals = {
                'state' : 'invoiced',
                'invoice_id' : invoice_id,
            }
            self.write(cr, uid, [statement.id], vals)
        return True

    def statement_invoice_paid(self, cr, uid, ids, *args):
        '''
        Create Account Move Payement for invoice
        '''
        account_move_obj = self.pool['account.move']
        account_move_line_obj = self.pool['account.move.line']
        period_obj = self.pool['account.period']
        account_journal_obj = self.pool['account.journal']

        of_config_obj = self.pool['openforce.config.settings']
        rec_stm_config = of_config_obj.get_default_recursive_statement(
                                                        cr, uid, fields,
                                                        context=None)
        now = datetime.now()
        for statement in self.browse(cr, uid, ids):
            if not statement.invoice_id :
                continue
            if not statement.invoice_id.move_id \
                    or not statement.invoice_id.residual:
                continue
            invoice = statement.invoice_id
            # Move invoice to pay/reconcile
            lines_to_pay = []
            for line in invoice.move_id.line_id:
                if line.account_id.type == 'receivable':
                    lines_to_pay.append(line.id)
            # Create Payment Move
            lines_to_reconcile = []
            if lines_to_pay:
                # Period
                period_ids = period_obj.find(cr, uid, now.isoformat(),
                                             context=None)
                period_id = period_ids and period_ids[0] or False
                # Ref
                ref = '%s - %s' % ( _("Payment Invoice"), invoice.number)
                # Journal
                if not rec_stm_config['of_rec_statement_acc_payment_journal_id']:
                    raise orm.except_orm(_('Error!'),
                        _('Missing jornal on Recursive Statement config'))
                journal = account_journal_obj.browse(cr, uid,
                    rec_stm_config['of_rec_statement_acc_payment_journal_id'])
                # Move
                move_id= account_move_obj.create(cr, uid, {
                    'ref': ref,
                    'journal_id': \
                        rec_stm_config['of_rec_statement_acc_payment_journal_id'],
                    'date': now.isoformat(),
                    'period_id': period_id
                }, context=None)
                # Lines
                for line_to_pay in account_move_line_obj.browse(cr, uid, lines_to_pay):
                    # ... Line refund invoice
                    line_refund_id = account_move_line_obj.create(cr, uid, {
                        'name': ref,
                        'account_id': line_to_pay.account_id.id,
                        'credit': line_to_pay.debit,
                        'debit': line_to_pay.credit,
                        'move_id': move_id,
                        'partner_id': line_to_pay.partner_id.id or False,
                    }, context=None)
                    # ... Line bank
                    line_bank_id = account_move_line_obj.create(cr, uid, {
                        'name': ref,
                        'account_id': journal.default_credit_account_id.id,
                        'credit': line_to_pay.credit,
                        'debit': line_to_pay.debit,
                        'move_id': move_id,
                        #'partner_id': line_to_pay.partner_id.id or False,
                    }, context=None)
                    # for reconcile
                    lines_to_reconcile.append([line_to_pay.id, line_refund_id])
            # Reconciliation
            for lines_rec_ids in lines_to_reconcile:
                account_move_line_obj.reconcile(cr, uid, lines_rec_ids,
                                                    context=None)
        return True

    def statement_invoice_send(self, cr, uid, ids, *args):

        res ={}

        mail_obj = self.pool.get('mail.mail')
        mail_message_obj = self.pool.get('mail.message')
        email_template_obj = self.pool.get('email.template')
        email_compose_message_obj = self.pool.get('mail.compose.message')
        partner_obj = self.pool['res.partner']
        user_obj = self.pool['res.users']

        server_mail_obj = self.pool.get('ir.mail_server')
        of_config_obj = self.pool['openforce.config.settings']
        rec_stm_config = of_config_obj.get_default_recursive_statement(
                                                        cr, uid, fields,
                                                        context=None)

        template_id  = rec_stm_config['of_rec_statement_email_tmpl_invoice']
        email_template = email_template_obj.browse(cr, uid, template_id)
        user = user_obj.browse(cr, uid, uid)

        for statement in self.browse(cr, uid, ids):
            if statement.invoice_id:
                # user's mail server
                user_server_mail = False
                serv_ids = False
                if statement.contract_id.manager_id and \
                    statement.contract_id.manager_id.email:
                    serv_ids = server_mail_obj.search(cr, uid,
                        [('smtp_user', '=',
                          statement.contract_id.manager_id.email)],
                          order='sequence', limit=1)
                if not serv_ids:
                    serv_ids = server_mail_obj.search(cr, uid, [('id', '>', 0)],
                                                  order='sequence', limit=1)
                if serv_ids:
                    user_server_mail = server_mail_obj.browse(cr, uid,
                                                              serv_ids[0])
                # user's mail server
                if statement.contract_id.manager_id.email:
                    serv_ids = server_mail_obj.search(cr, uid,
                            [('smtp_user', '=',
                              statement.contract_id.manager_id.email)],
                            order='sequence', limit=1)
                    if serv_ids:
                        user_server_mail = server_mail_obj.browse(cr, uid,
                                                                  serv_ids[0])
                # Recipients
                recipient_ids = []
                # ... partner
                recipient_ids.append(statement.invoice_id.partner_id.id)
                # ... followers
                ## >>>> non gestito x ora
                #for follower in statement.message_follower_ids:
                #    recipient_ids.append(follower.id)

                # Compose E-mail
                for recipient in partner_obj.browse(cr, uid, recipient_ids):
                    mail_subject = email_compose_message_obj.render_template(
                                        cr,uid,_(email_template.subject),
                                    'account.invoice', statement.invoice_id.id)
                    mail_body = email_compose_message_obj.render_template(
                                        cr,uid,_(email_template.body_html),
                                    'account.invoice', statement.invoice_id.id)
                    mail_to = email_compose_message_obj.render_template(
                                        cr,uid,_(email_template.email_to),
                                    'account.invoice', statement.invoice_id.id)
                    reply_to = email_compose_message_obj.render_template(
                                        cr,uid,_(email_template.reply_to),
                                    'account.invoice', statement.invoice_id.id)
                    if statement.contract_id.manager_id.email:
                        mail_from = statement.contract_id.manager_id.email
                    else:
                        mail_from = email_compose_message_obj.render_template(
                                        cr,uid,_(email_template.email_from),
                                        'sale.recursive.statement',
                                        statement.invoice_id.id)
                    if not mail_from:
                        continue

                    # body with data line
                    message_vals = {
                            'email_from': mail_from,
                            'subject' : mail_subject,
                            'model' : self._name,
                            'res_id' : statement.id,
                            'partner_ids' : recipient_ids
                                            and [(6,0,recipient_ids)] or False,
                            'notified_partner_ids' : recipient_ids
                                            and [(6,0,recipient_ids)] or False,
                            'type' : 'comment',
                            'body' : mail_body,
                            }
                    message_id = mail_message_obj.create(cr, uid, message_vals)

                    mail_id = mail_obj.create(cr, uid, {
                            'mail_message_id' : message_id,
                            'mail_server_id' : user_server_mail
                                and user_server_mail.id
                                or email_template.mail_server_id
                                and email_template.mail_server_id.id
                                or False,
                            'state' : 'outgoing',
                            'auto_delete' : email_template.auto_delete,
                            'email_from' : mail_from,
                            'email_to' : recipient.email,
                            #'reply_to' : reply_to,
                            'body_html' : mail_body,
                            })
                    if mail_id:
                        mail_obj.send(cr, uid, [mail_id])
                #mail_ids += [mail_id,]
        #if mail_ids:
        #    mail_obj.send(cr, uid, mail_ids)
        #    _logger.info('[SUBSCRIPTION SEND MAIL] Sended %s mails for %s template' % (len(mail_ids), email_template.name))
                '''
                print "xxx"
                if not template_id:
                    ir_model_data = self.pool.get('ir.model.data')
                    try:
                        template_id = ir_model_data.get_object_reference(cr,
                                    uid, 'of_sale_recursive_statement',
                                    'of_recursive_statement_email_template')[1]
                    except ValueError:
                        template_id = False
                '''
                #try:
                #    compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form')[1]
                #except ValueError:
                #    compose_form_id = False

        return res

    def statement_mail_send(self, cr, uid, ids, email_template):

        res ={}

        mail_obj = self.pool.get('mail.mail')
        mail_message_obj = self.pool.get('mail.message')
        email_template_obj = self.pool.get('email.template')
        email_compose_message_obj = self.pool.get('mail.compose.message')
        partner_obj = self.pool['res.partner']
        user_obj = self.pool['res.users']
        server_mail_obj = self.pool.get('ir.mail_server')

        user = user_obj.browse(cr, uid, uid)

        for statement in self.browse(cr, uid, ids):

            # user's mail server
            user_server_mail = False
            serv_ids = False
            if statement.contract_id.manager_id \
                and statement.contract_id.manager_id.email:
                serv_ids = server_mail_obj.search(cr, uid, [('smtp_user', '=',
                                statement.contract_id.manager_id.email)],
                                order='sequence', limit=1)
            if not serv_ids:
                serv_ids = server_mail_obj.search(cr, uid, [('id', '>', 0)],
                                                  order='sequence', limit=1)
            if serv_ids:
                user_server_mail = server_mail_obj.browse(cr, uid,
                                                          serv_ids[0])
            # user's mail server
            if statement.contract_id.manager_id.email:
                serv_ids = server_mail_obj.search(cr, uid,
                        [('smtp_user', '=',
                          statement.contract_id.manager_id.email)],
                        order='sequence', limit=1)
                if serv_ids:
                    user_server_mail = server_mail_obj.browse(cr, uid,
                                                              serv_ids[0])
            # Recipients
            recipient_ids = []
            # ... partner
            recipient_ids.append(statement.partner_id.id)
            # ... followers
            ## >>>> non gestito x ora
            #for follower in statement.message_follower_ids:
            #    recipient_ids.append(follower.id)

            # Compose E-mail
            for recipient in partner_obj.browse(cr, uid, recipient_ids):
                mail_subject = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.subject),
                                self._name, statement.id)
                mail_body = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.body_html),
                                self._name, statement.id)
                mail_to = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.email_to),
                                self._name, statement.id)
                reply_to = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.reply_to),
                                self._name, statement.id)
                if statement.contract_id.manager_id.email:
                    mail_from = statement.contract_id.manager_id.email
                else:
                    mail_from = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.email_from),
                                    'sale.recursive.statement',
                                    statement.id)
                if not mail_from:
                    continue

                # body with data line
                message_vals = {
                        'type': 'email',
                        'email_from': mail_from,
                        'subject' : mail_subject,
                        'model' : self._name,
                        'res_id' : statement.id,
                        'partner_ids' : recipient_ids
                                        and (6,0,recipient_ids) or False,
                        'notified_partner_ids' : recipient_ids
                                        and (6,0,recipient_ids) or False,
                        'type' : 'comment',
                        'body' : mail_body,
                        }
                message_id = mail_message_obj.create(cr, uid, message_vals)

                mail_id = mail_obj.create(cr, uid, {
                        'mail_message_id' : message_id,
                        'mail_server_id' : user_server_mail
                            and user_server_mail.id
                            or email_template.mail_server_id
                            and email_template.mail_server_id.id
                            or False,
                        'state' : 'outgoing',
                        'auto_delete' : email_template.auto_delete,
                        'email_from' : mail_from,
                        'email_to' : recipient.email,
                        #'reply_to' : reply_to,
                        'body_html' : mail_body,
                        })
                if mail_id:
                    mail_obj.send(cr, uid, [mail_id])
                    wf_service = netsvc.LocalService("workflow")
                    wf_service.trg_validate(uid,
                                            'sale.recursive.statement',
                                            statement.id,
                                            'statement_send', cr)

    def recompute_statement(self, cr, uid, ids, context=None):
        '''
        '''
        user_obj = self.pool['res.users']
        statement_product_obj = self.pool['sale.recursive.statement.product']
        statement_line_obj = self.pool['sale.recursive.statement.line']

        if not context:
            context = {}
        contract_id = context.get('contract_id',False)
        user = user_obj.browse(cr, uid, uid, context=context)
        # Existint statement to unlink
        domain = [('state', '=', 'draft')]
        if contract_id:
            domain.append(('contract_id', '=', contract_id))
        st_exists_ids = self.search(cr, uid, domain)
        if st_exists_ids:
            self.unlink(cr, uid, st_exists_ids)

        # Ricreate lines form statement product
        statement_product_obj.compute_statement_line(cr, uid, False, context)
        # Group lines under statement with same contract/product/data
        domain = [('statement_id','=',False)]
        if contract_id:
            domain.append(('contract_id', '=', contract_id))
        st_line_ids = statement_line_obj.search(cr, uid, domain)
        for st_line in statement_line_obj.browse(cr, uid, st_line_ids):
            # Search statement to add line
            domain = [('date', '=', st_line.date),
                      ('contract_id', '=', st_line.contract_id.id),
                      ('state', '=', 'draft')]
            statement_ids = self.search(cr, uid, domain)
            # Create new statement
            if not statement_ids:
                val = {
                    'date' : st_line.date,
                    'contract_id' : st_line.contract_id.id,
                    'partner_id' : st_line.contract_id.partner_id.id,
                    'payment_term' : st_line.order_id.payment_term.id or \
                        st_line.contract_id.partner_id.property_payment_term\
                        and \
                        st_line.contract_id.partner_id.property_payment_term.id,
                    'company_id' :
                        st_line.order_id.company_id.id
                        or user.company_id.id,
                    'user_id' :
                        st_line.order_id.user_id.id
                        or user.id
                    }
                statement_id = self.create(cr, uid, val)
            else:
                statement_id = statement_ids[0]
            # ... link line
            val = {
                'statement_id' :statement_id
                }
            statement_line_obj.write(cr, uid, [st_line.id], val)

        return True


class sale_recursive_statement_reminder(orm.Model):
    _name="sale.recursive.statement.reminder"
    _columns = {
        'days': fields.integer('Days', help="Use negative number for date \
                    before deadline"),
        'email_tmpl_id': fields.many2one(
                    'email.template', 'E-Mail template to send',
                    help="Template to use when it sends mail with  \
                     statement"
                    ),
        'valid_state': fields.selection([
            ('draft', 'Draft'),
            ('sent', 'Sent'),
            ('paid', 'Paid'),
            ('invoiced', 'Invoiced'),
            ], 'Valid Status', help="Statement State for mail send"),
        'description': fields.text('Note'),
        'active': fields.boolean('Active'),
        'task_create': fields.boolean(
            'Create Task'),
        'task_user_id': fields.many2one('res.users', 'User Task',),
        'task_deadline_days': fields.integer(
            'Task deadline Days'),
        'task_description': fields.text(
            'Note Task'),
        'recurring': fields.boolean('Recurring'),
    }
    _defaults = {
        'active': True
    }

    _order = 'days'

    def _prepare_statement_to_remind(self, cr, uid, statement_ids, context=None):
        '''
        '''
        return True

    def execute_all(self, cr, uid, ids, context=None):
        domain = [('active', '=', True)]
        role_ids = self.search(cr, uid, domain, order='days')
        self.execute(cr, uid, role_ids, context)

        return True

    def execute(self, cr, uid, ids, context=None):
        '''
        Excution of ALL ROLES
        '''
        statement_obj = self.pool['sale.recursive.statement']
        project_obj = self.pool['project.project']
        project_task_obj = self.pool['project.task']
        user_obj = self.pool['res.users']

        user = user_obj.browse(cr, uid, uid, context=context)

        for role in self.browse(cr, uid, ids):
            # Compute Date
            now_obj = datetime.now()
            now = datetime.strftime(now_obj, "%Y-%m-%d")
            date_competence = datetime.strptime(now,"%Y-%m-%d")\
                    + timedelta(days=role.days * -1)
            # Statement to send
            domain = [('date', '=', date_competence)]
            if role.valid_state:
                domain.append( ('state', '=', role.valid_state) )
            statement_ids = statement_obj.search(cr, uid, domain)
            self._prepare_statement_to_remind(cr, uid, statement_ids, context)

            for statement in statement_obj.browse(cr, uid, statement_ids):
                statement_obj.statement_mail_send(cr, uid, [statement.id],
                                              role.email_tmpl_id)
                # Create Task
                if role.task_create:
                    # Find realtive project
                    project = False
                    domain = [('analytic_account_id', '=',
                                                statement.contract_id.id)]
                    project_ids = project_obj.search(cr, uid, domain)
                    if project_ids:
                        project = project_obj.browse(cr, uid, project_ids[0])
                    # setting data
                    if role.task_description:
                        description = ' %s - %s' % (statement.number,
                                                    role.task_description)
                    else:
                        description += ' %s - %s' % (statement.number,
                                                    role.description)
                    deadline_date_obj = datetime.now()
                    deadline_date =False
                    if role.task_deadline_days:
                        deadline_date = deadline_date_obj \
                            + timedelta(days=role.task_deadline_days)
                    val = {
                        'name': description,
                        'project_id' : project and project.id
                             or False,
                        'user_id' : role.task_user_id.id,
                        'reviewer_id' : user.id,
                        'date_deadline' : deadline_date or False,
                        'date_start' : datetime.now(),
                        'description' : description,
                        'partner_id' : statement.partner_id.id,
                        #'sale_line_id' : line.id,
                    }
                    task_id = project_task_obj.create(cr, uid, val)

        return True



class account_analytic_account(orm.Model):
    _inherit="account.analytic.account"

    _columns = {
        'recursive_product_ids': fields.one2many('sale.recursive.statement.product',
            'contract_id', 'Recursive Products'),
        'recursive_statement_ids': fields.one2many('sale.recursive.statement',
            'contract_id', 'Statements'),
    }

    def compute_recursive_statement(self, cr, uid, ids, context=None):
        '''
        Compute for fiscal year active
        '''
        rec_statement_obj = self.pool['sale.recursive.statement']
        if not context:
            context = {}
        for contract in self.browse(cr, uid, ids):
            context.update({'contract_id': contract.id})
            rec_statement_obj.recompute_statement(cr, uid, False, context)
            #>>> controllo ricalcola solo se appartiene a statement in draft!!
        return True

    def unlink(self, cr, uid, ids, *args, **kwargs):

        # Avoid unlink of contract if there are statement not in draft
        for contract in self.browse(cr, uid, ids):
            for st in contract.recursive_statement_ids:
                if not st.state in ['draft']:
                    raise orm.except_orm(_('Error!'),
                        _('All statements of contract must be in Draft. \
                        See Statemnt %s') % \
                        (st.number,))
        return super(account_analytic_account,self).unlink(cr, uid, ids,*args, **kwargs)


class sale_order(orm.Model):
    _inherit="sale.order"

    def action_button_confirm(self, cr, uid, ids, context=None):
        '''
        Create recursive statement product linked to contract
        '''
        user_obj = self.pool['res.users']
        sale_order_line_obj = self.pool['sale.order.line']
        contract_obj = self.pool['account.analytic.account']
        # Config from user
        user = user_obj.browse(cr, uid, uid, context=context)

        res = super(sale_order, self).action_button_confirm(cr, uid, ids, context=context)

        # Creation of contract
        if user.company_id.of_rec_statement_sale_order_contract:
            contract_ids = self.generate_contract(cr, uid, ids, context)

        # Creation of recursive product
        contracts_to_recompute = []
        for order in self.browse(cr, uid, ids):
            if order.project_id:
                domain = [('order_id', '=', order.id)]
                line_ids = sale_order_line_obj.search(cr, uid, domain)
                sale_order_line_obj.create_recursive_statement_product(cr, uid, line_ids)
                contracts_to_recompute.append(order.project_id.id)
        # Recompute contracts
        if contracts_to_recompute:
            contract_obj.compute_recursive_statement(cr, uid, contracts_to_recompute)

        return res

    def generate_contract(self, cr, uid, ids, context=None):
        '''
        Create Contract from order
        '''
        contract_ids = []
        user_obj = self.pool['res.users']
        contract_obj = self.pool['account.analytic.account']
        user = user_obj.browse(cr, uid, uid, context=context)

        date_today_obj = datetime.today()
        for order in self.browse(cr, uid, ids):
            # No if contract exists
            if order.project_id:
                continue
            # Create Contract
            val = {
                'name' : order.partner_id.name,
                'use_timesheets': True,
                'use_tasks': True,
                'use_issues': True,
                'partner_id': order.partner_id.id,
                'manager_id': user.id,
                'date_start': datetime.strftime(date_today_obj, "%Y-%m-%d"),
                'pricelist_id': order.partner_id.property_product_pricelist.id
                }
            contract_id = contract_obj.create(cr, uid, val)
            contract_ids.append(contract_id)
            # Set contract on sale order
            val = {
                'project_id': contract_id
                }
            self.write(cr, uid, [order.id], val)

        return contract_ids

    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        '''
        Retrive the most recent contract
        '''
        contract_obj = self.pool['account.analytic.account']

        res = super(sale_order, self).onchange_partner_id(cr, uid, ids, part, context=context)
        if not part:
            return res
        date_today_obj = datetime.today()
        domain = [('partner_id', '=', part)]
        contract_ids = contract_obj.search(cr, uid, domain, order = 'date')
        if not contract_ids:
            return res
        contract_id = False
        for contract in contract_obj.browse(cr, uid, contract_ids):
            # Active
            if contract.date and contract.date < datetime.strftime(date_today_obj, "%Y-%m-%d"):
                continue
            contract_id = contract.id
            break
        if contract_id:
            res['value']['project_id'] = contract_id
        return res


class sale_order_line(orm.Model):
    _inherit="sale.order.line"

    def _check_rec_statement_frequency_uom(self, cr, uid, ids, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        for element in self.browse(cr, uid, ids, context=context):
            if not element.recursive_statement_frequency_uom.category_id.id ==\
                    user.company_id.of_rec_statement_frequency_uom_categ_id.id:
                return False
        return True

    def _check_rec_statement_product_uom(self, cr, uid, ids, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context)
        for element in self.browse(cr, uid, ids, context=context):
            if not element.recursive_statement:
                continue
            if not element.product_id.uom_id.category_id.id ==\
                    user.company_id.of_rec_statement_frequency_uom_categ_id.id:
                return False
        return True

    _columns = {
        'recursive_statement': fields.boolean(
            'Recursive Statement',
            help="""Enable Recursive Statement""" ),
        'recursive_statement_frequency': fields.integer('Frequency',
                    readonly=True, states={'draft': [('readonly', False)]}),
        'recursive_statement_frequency_uom': fields.many2one('product.uom',
                    'Frequency UOM'),
        'recursive_statement_date_start': fields.date('Date Start',
                    states={'draft': [('readonly', False)]}),
        'recursive_statement_date_stop': fields.date('Date Stop',
                    states={'draft': [('readonly', False)]}),
        'recursive_statement_line_policy': fields.selection([
            ('fwoh', 'First Whole, Other hook'),
            ('fwsroh', 'First Whole, Second proportional refund, Other hook'),
            ('indi', 'Indipendent'),
            ], 'Line Policy', required=True),
        'recursive_statement_discount': fields.float(
            'Discount (%)', digits_compute= dp.get_precision('Discount')),
        'recursive_statement_discount_number_lines': fields.integer(
            'Discount Nr Lines'),
    }

    _defaults = {
        'recursive_statement_line_policy' :'fwsroh'
    }

    _constraints = [
        (_check_rec_statement_frequency_uom, 'Error! Category of Frequency UOM \
                is not valid', ['recursive_statement_frequency_uom']),
        (_check_rec_statement_product_uom, 'Error! For Product with recursive\
                statement, The Category of UOM is not valid', \
                ['product_id']),
    ]

    def _prepare_recursive_statement_product(self, cr, uid, line, context=None):
        date_today_obj = datetime.today()
        vals = {
                'contract_id' : line.order_id.project_id.id or False,
                'partner_id' : line.order_id.partner_id.id or False,
                'product_id' : line.product_id.id or False,
                'line_order_id' : line.id,
                'name' : line.name or False,
                'price_unit' : line.price_unit or False,
                'qty' : line.product_uos_qty or line.product_uom_qty or False,
                'uos' : line.product_uos.id or line.product_uom.id or False,
                'discount' :
                    line.recursive_statement_discount or False,
                'discount_number_lines' :
                    line.recursive_statement_discount_number_lines or False,
                'frequency' :
                    line.recursive_statement_frequency or False,
                'frequency_uom' :
                    line.recursive_statement_frequency_uom.id or False,
                'line_policy' :
                    line.recursive_statement_line_policy or False,
                'date_start' :
                    line.recursive_statement_date_start or
                    datetime.strftime(date_today_obj, "%Y-%m-%d")
                    or False,
                'date_stop' :
                    line.recursive_statement_date_stop or False,
                'tax_id': [(6, 0, [x.id for x in line.tax_id])]
            }

        return vals

    def create_recursive_statement_product(self, cr, uid, ids, context=None):
        recursive_statement_product_obj =\
            self.pool['sale.recursive.statement.product']

        for line in self.browse(cr, uid, ids):
            if not line.recursive_statement:
                continue
            vals = self._prepare_recursive_statement_product(cr, uid, line,
                                                             context)
            recursive_statement_product_obj.create(cr, uid, vals)
        return True


    def product_id_change(self, cr, uid, ids, pricelist, product_id, qty=0,
            uom=False, qty_uos=0, uos=False, name='', partner_id=False,
            lang=False, update_tax=True, date_order=False, packaging=False,
            fiscal_position=False, flag=False, context=None):
        product_obj = self.pool['product.product']
        user_obj = self.pool['res.users']
        res = super(sale_order_line, self).product_id_change(cr, uid,
                ids, pricelist, product_id, qty, uom, qty_uos,
                uos, name, partner_id, lang, update_tax, date_order,
                packaging, fiscal_position, flag, context)

        if product_id:
            product = product_obj.browse(cr, uid, product_id)
            user = user_obj.browse(cr, uid, uid, context)
            company = user.company_id
            res['value'].update({'recursive_statement':
                                  product.recursive_statement})
            # Default frequency
            if product.recursive_statement:
                if product.recursive_statement_line_policy:
                    res['value'].update({'recursive_statement_line_policy':
                        product.recursive_statement_line_policy})
                else:
                    res['value'].update({'recursive_statement_line_policy':
                        company.of_rec_statement_line_policy})
                if product.recursive_statement_frequency:
                    res['value'].update({'recursive_statement_frequency':
                        product.recursive_statement_frequency})
                else:
                    res['value'].update({'recursive_statement_frequency':
                        company.of_rec_statement_default_frequency})

                if product.recursive_statement_frequency_uom:
                    res['value'].update({'recursive_statement_frequency_uom':
                        product.recursive_statement_frequency_uom.id})
                else:
                    res['value'].update({'recursive_statement_frequency_uom':
                        company.of_rec_statement_default_frequency_uom_id.id})
        return res

    def onchange_rec_discount(self, cr, uid, ids, discount, context=None):
        '''
        Control limit discount
        '''
        user = self.pool['res.users'].browse(cr, uid, uid, context)
        company = user.company_id
        if discount and company.of_rec_statement_limit_discount:
            if discount > company.of_rec_statement_limit_discount:
                raise orm.except_orm(_('Error!'),
                                         _('The discount exceeds the limit'))

        return True

    def onchange_rec_discount_number_lines(self, cr, uid, ids,
                                           nr_lines, context=None):
        '''
        Control limit discount nr lines
        '''
        user = self.pool['res.users'].browse(cr, uid, uid, context)
        company = user.company_id

        if nr_lines and company.of_rec_statement_limit_discount_number_lines:
            if nr_lines > company.of_rec_statement_limit_discount_number_lines:
                raise orm.except_orm(_('Error!'),
                                         _('The lines of discount exceeds the \
                                         limit'))
        return True

class product_template(orm.Model):
    _inherit="product.template"

    _columns = {
        'recursive_statement': fields.boolean(
            'Recursive Statement',
            help="""Enable Recursive Statement"""),
        'recursive_statement_line_policy': fields.selection([
            ('fwoh', 'First Whole, Other hook'),
            ('fwsroh', 'First Whole, Second proportional refund, Other hook'),
            ('indi', 'Indipendent'),
            ], 'Line Policy', required=True),
        'recursive_statement_frequency': fields.integer('Frequency',
                    states={'draft': [('readonly', False)]}),
        'recursive_statement_frequency_uom': fields.many2one('product.uom',
                    'Frequency UOM'),
    }

    _defaults = {
        'recursive_statement_line_policy' :'fwoh'
    }

    def default_get(self, cr, uid, fields, context=None):
        res = super(product_template, self).default_get(cr, uid, fields,
                                                        context=context)
        of_config_obj = self.pool['openforce.config.settings']
        recursive_statement = of_config_obj.get_default_recursive_statement(
                                                        cr, uid, fields,
                                                        context=None)
        res.update({
            'recursive_statement':
                recursive_statement.get(
                    'of_product_default_recursive_statement', False),
            'recursive_statement_frequency':
                recursive_statement.get(
                    'of_rec_statement_default_frequency', False),
            'recursive_statement_frequency_uom':
                recursive_statement.get(
                    'of_rec_statement_default_frequency_uom_id', False),
            'recursive_statement_line_policy':
                recursive_statement.get(
                    'of_rec_statement_line_policy', False),
        })
        return res
