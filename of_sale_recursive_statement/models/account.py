# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2014
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, orm
from openerp.tools.translate import _

class account_fiscalyear(orm.Model):
    
    _inherit="account.fiscalyear"
    
    def compute_recursive_statement(self, cr, uid, ids, context=None):
        '''
        Recompute for fiscal year 
        '''
        rec_statement_obj = self.pool['sale.recursive.statement']
        if not context:
            context = {}
        domain = [('id', '>', 0)]
        contract_ids = self.pool['account.analytic.account'].search(cr, uid, domain)
        for contract in self.browse(cr, uid, contract_ids):
            context.update({'contract_id': contract.id})
            rec_statement_obj.recompute_statement(cr, uid, False, context)
        return True
    
    def create_period(self, cr, uid, ids, context=None, interval=1):
        res_id = super(account_fiscalyear,self).create_period(
            cr, uid, ids, context, interval)
        self.compute_recursive_statement(cr, uid, [res_id], context)
        return res_id 
    