# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@yahoo.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, orm
import openerp.addons.decimal_precision as dp


class res_company(orm.Model):
    
    _inherit = "res.company"
    
    _columns = {
        'of_product_default_recursive_statement': fields.boolean('Default \
                    Recursive Statement', 
                    help="Default Recursive Statement"),
        'of_rec_statement_frequency_uom_categ_id': fields.many2one(
                    'product.uom.categ', 'Category UOM frequency'),
        'of_rec_statement_ref_frequency_uom_id': fields.many2one(
                    'product.uom', 'Frequency UOM Ref Maturity',
                    help="UOM Ref for compute date maturity"),  
        'of_rec_statement_default_frequency': fields.float('Default \
                    frequency'),
        'of_rec_statement_default_frequency_uom_id': fields.many2one(
                    'product.uom', 'Default frequency UOM'),
        'of_rec_statement_sale_order_contract': fields.boolean('Create \
                    contract from sale order', 
                    help="Create Contract from sale order"),
        'of_rec_statement_line_policy': fields.selection([
            ('fwoh', 'First Whole, Other hook'),
            ('fwsroh', 'First Whole, Second proportional refund, Other hook'),
            ('indi', 'Indipendent'),
            ], 'Line Policy', required=True),
        'of_rec_statement_acc_payment_journal_id': fields.many2one(
                    'account.journal', 'Journal Payment',
                    help="Journal for Payment Move"), 
        'of_rec_statement_email_tmpl_statement': fields.many2one(
                    'email.template', 'E-Mail template to send Statement',
                    help="Template to use when it sends mail with  \
                     statement"
                    ),
        'of_rec_statement_email_tmpl_invoice': fields.many2one(
                    'email.template', 'E-Mail template to send Invoice',
                    help="Template to use when it sends mail with invoice \
                    form statement"
                    ),
        'of_rec_statement_limit_discount': fields.float(
            'Discount (%)', digits_compute= dp.get_precision(
            'Discount Limit ')),
        'of_rec_statement_limit_discount_number_lines': fields.integer(
            'Discount Nr Lines Limit'),
        }
    