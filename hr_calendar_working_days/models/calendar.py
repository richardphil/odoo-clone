# -*- coding: utf-8 -*-
#
##############################################################################
#
#    Author(s): Alessandro Camilli (alessandrocamilli@openforce.it)
#               Andrea Colangelo (andreacolangelo@openforce.it)
#
#    Copyright © 2016 Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see:
#    http://www.gnu.org/licenses/agpl-3.0.txt.
#
##############################################################################

from openerp.osv import fields, orm, osv


class hr_calendar_working_days(orm.Model):
    _name = "hr.calendar.working.days"
    
    def _compute(self, cr, uid, ids, fields, arg, context=None):
        res = {}
        if not isinstance(fields, list):
            fields = [fields]
        for meeting in self.browse(cr, uid, ids, context=context):
            meeting_data = {}
            res[meeting.id] = meeting_data
            attendee = self._find_my_attendee(cr, uid, [meeting.id], context)
            for field in fields:
                if field == 'start':
                    meeting_data[field] = meeting.start_date if meeting.allday else meeting.start_datetime
                elif field == 'stop':
                    meeting_data[field] = meeting.stop_date if meeting.allday else meeting.stop_datetime
        return res
    
    def _set_date(self, cr, uid, values, id=False, context=None):

        if context is None:
            context = {}

        if values.get('start_datetime') or values.get('start_date') or values.get('start') \
                or values.get('stop_datetime') or values.get('stop_date') or values.get('stop'):
            allday = values.get("allday", None)

            if allday is None:
                if id:
                    allday = event.allday
                else:
                    allday = False
                    _logger.warning("Calendar - All day is not specified, arbitrarily set to False")
                    #raise osv.except_osv(_('Error!'), ("Need to know if it's an allday or not..."))

            key = "date" if allday else "datetime"
            notkey = "datetime" if allday else "date"

            for fld in ('start', 'stop'):
                if values.get('%s_%s' % (fld, key)) or values.get(fld):
                    values['%s_%s' % (fld, key)] = values.get('%s_%s' % (fld, key)) or values.get(fld)
                    values['%s_%s' % (fld, notkey)] = None
                    if fld not in values.keys():
                        values[fld] = values['%s_%s' % (fld, key)]

            diff = False
            if allday and (values.get('stop_date') or values.get('start_date')):
                stop_date = values.get('stop_date') or False
                start_date = values.get('start_date') or False
                if stop_date and start_date:
                    diff = openerp.fields.Date.from_string(stop_date) - openerp.fields.Date.from_string(start_date)
            elif values.get('stop_datetime') or values.get('start_datetime'):
                stop_datetime = values.get('stop_datetime') or False
                start_datetime = values.get('start_datetime') or False
                if stop_datetime and start_datetime:
                    diff = openerp.fields.Datetime.from_string(stop_datetime) - openerp.fields.Datetime.from_string(start_datetime)
            if diff:
                duration = float(diff.days) * 24 + (float(diff.seconds) / 3600)
                values['duration'] = round(duration, 2)
    
    _columns = {
        'date': fields.date('Date'),
        'type': fields.selection([('working', 'Working Day'),
                                  ('holiday','Holiday')],
                                 'Type', required=True),
        'allday': fields.boolean('All Day'),
        'start': fields.function(_compute, string='Calculated start', 
                                 type="datetime", multi='attendee', store=True),
        'stop': fields.function(_compute, string='Calculated stop', 
                                type="datetime", multi='attendee', store=True),
        'start_date': fields.date('Start Date'),
        'start_datetime': fields.datetime('Start DateTime'),
        'stop_date': fields.date('End Date'),
        'stop_datetime': fields.datetime('End Datetime'),
        'duration': fields.float('Duration'),
    }
