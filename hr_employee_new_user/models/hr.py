# -*- coding: utf-8 -*-
#
##############################################################################
#
#    Author(s): Alessandro Camilli (alessandrocamilli@openforce.it)
#               Andrea Colangelo (andreacolangelo@openforce.it)
#
#    Copyright © 2016 Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see:
#    http://www.gnu.org/licenses/agpl-3.0.txt.
#
##############################################################################


from openerp.osv import fields, orm


class res_users(orm.Model):
    _inherit = "res.users"

    def create(self, cr, uid, vals, context=None):
        employee_obj = self.pool['hr.employee']
        user_id = super(res_users, self).create(cr, uid, vals, context)
        user = self.browse(cr, uid, user_id, context)
        
        domain = [('work_email', '=', user.login)]
        employee_ids = employee_obj.search(cr, uid, domain)
        if employee_ids:
            vals = {
                'user_id' : user_id
            }
            employee_obj.write(cr, uid, [employee_ids[0]], vals)
            
        return user_id