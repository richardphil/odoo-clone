# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (alessandrocamilli@openforce.it)
#
#    Copyright (C) 2016
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, osv, orm


class account_move_reconcile(orm.Model):
    _inherit = "account.move.reconcile"
    _columns = {
        'competence_line_ids': fields.one2many(
            'account.move.reconcile.competence', 'reconcile_id', 
            'Competence Lines'),
    }
    
    def create(self, cr, uid, vals, context=None):
        res_id = super(account_move_reconcile, self).create(cr, uid, vals, 
                                                            context)
        rec_move = self.browse(cr, uid, res_id)
        for line in rec_move.line_id:
            if line.journal_id.type in ['cash', 'bank']:
                context.update({'reconcile_type' : 'full'})
                self.register_payment_competence(cr, uid, rec_move, 
                                                 line.move_id, context)
        for line in rec_move.line_partial_ids:
            if line.journal_id.type in ['cash', 'bank']:
                context.update({'reconcile_type' : 'partial'})
                self.register_payment_competence(cr, uid, rec_move, 
                                                 line.move_id, context)
        return res_id
    
    def register_payment_competence(self, cr, uid, rec_move, pay_move, 
                                    context=None):
        decimal_precision_obj = self.pool['decimal.precision']
        competence_obj = self.pool['account.move.reconcile.competence']
        payment_data = self.get_payment_data(cr, uid, pay_move, context)
        competence_data = self.get_competence_data(cr, uid, pay_move, 
                                                      context)
        payment_total = payment_data['payment_total']
        payment_accounts  = payment_data['payment_accounts']
        competence_accounts  = competence_data['competence_accounts']
        # Split payment on competence
        for item in competence_accounts.items():
            competence_account_id = item[0] 
            competence_account_amount = item[1]
            amount = round(competence_account_amount * 
                (payment_total / competence_account_amount),
                decimal_precision_obj.precision_get(cr, uid, 'Account'))
            # split for more payments
            payment_residual = payment_total
            payment_nr = len(payment_accounts.items())
            p = 1
            for item in payment_accounts.items():
                payment_account_id = item[0] 
                payment_account_amount = item[1]
                if p == payment_nr:
                    payment_account_amount = payment_residual
                val = {
                    'reconcile_id': rec_move.id, 
                    'competence_account_id': competence_account_id, 
                    'payment_account_id': payment_account_id, 
                    'amount': payment_account_amount, 
                }
                comp_id = competence_obj.create(cr, uid, val)
                payment_residual -= payment_account_amount
                p += 1
                
        return True
        
    def get_payment_data(self, cr, uid, pay_move, context=None):
        res = {
            'payment_total' : 0,
            'payment_accounts' : {},
        }
        payment_accounts = {}
        for line in pay_move.line_id:
            if line.account_id.type in ['liquidity']:
                if line.credit:
                    amount = -1 * line.credit
                else:
                    amount = line.debit
                res['payment_total'] += amount
                if not line.account_id.id in payment_accounts:
                    payment_accounts[line.account_id.id] = amount
                else:
                    payment_accounts[line.account_id.id] += amount
        res['payment_accounts'] = payment_accounts
        return res
    
    def get_competence_data(self, cr, uid, pay_move, context=None):
        
        def _add_line_competence(competence_accounts, line):
            if line.credit:
                amount = line.credit
            else:
                amount = -1 * line.debit
            if not line.account_id.id in competence_accounts:
                competence_accounts[line.account_id.id] = amount
            else:
                competence_accounts[line.account_id.id] += amount
            return competence_accounts
            
        move_line_obj = self.pool['account.move.line']
        res = {
            'competence_accounts' : {},
        }
        # Competence data
        competence_accounts = {}
        for line in pay_move.line_id:
            if line.account_id.type in ['liquidity']:
                continue
            # Search chained lines
            reconcile_type = context.get('reconcile_type', False)
            domain = [('move_id', '!=', line.move_id.id)]
            if reconcile_type == 'partial':
                domain.append(('reconcile_partial_id', '=', 
                               line.reconcile_partial_id.id))
            else:
                domain.append(('reconcile_id', '=', 
                               line.reconcile_id.id))
            line_chain_ids = move_line_obj.search(cr, uid, domain)
            if line_chain_ids:
                for line_chain in move_line_obj.browse(cr, uid, line_chain_ids):
                    # Search invoice for accounts in lines
                    if line_chain.account_id.type in ['payable', 'receivable']:
                        domain = [('move_id', '=', line_chain.move_id.id), 
                                  ('id', '!=', line_chain.id)]
                        comp_line_ids = move_line_obj.search(cr, uid, domain)
                        for comp_line in move_line_obj.browse(cr, uid, 
                                                              comp_line_ids):
                            competence_accounts = _add_line_competence(
                                competence_accounts, comp_line)
            else:
                # for not chained lines -> lines of same move 
                competence_accounts = _add_line_competence(
                    competence_accounts, line)
        res['competence_accounts'] = competence_accounts
        return res
        
    
class account_move_reconcile_competence(orm.Model):
    _name = "account.move.reconcile.competence"
    _columns = {
        'reconcile_id': fields.many2one('account.move.reconcile', 
                                        readonly=True, required=True, 
                                        ondelete="cascade"),
        'competence_account_id': fields.many2one('account.account',
                                                 'Account Competence', 
                                                 required=True),
        'payment_account_id': fields.many2one('account.account', 
                                              'Account Payment', 
                                              required=True),
        'amount': fields.float('Amount'),
        'invoice_id': fields.many2one('account.invoice', 'Invoice')
    }