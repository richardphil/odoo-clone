# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (alessandrocamilli@openforce.it)
#
#    Copyright (C) 2016
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _
import string


class account_voucher(models.Model):
    _inherit = "account.voucher"
    
    @api.multi
    def cancel_voucher(self):
        competence_obj = self.env['account.move.reconcile.competence']
        for voucher in self:
            for line in voucher.move_ids:
                # refresh to make sure you don't unreconcile an already 
                # unreconciled entry
                line.refresh()
                if line.reconcile_id and line.reconcile_id.competence_line_ids:
                    for comp in line.reconcile_id.competence_line_ids:
                        comp.unlink()
                if line.reconcile_partial_id and \
                    line.reconcile_partial_id.competence_line_ids:
                    for comp in line.reconcile_partial_id.competence_line_ids:
                        comp.unlink()
        return super(account_voucher, self).cancel_voucher()


class account_move_reconcile(models.Model):
    _inherit = "account.move.reconcile"
    
    competence_line_ids = fields.One2many('account.move.reconcile.competence', 
                                          'reconcile_id', 
                                          string='Competence Lines')
    
    @api.multi
    def unlink(self):
        for rec in self:
            for comp in rec.competence_line_ids:
                comp.unlink()
        super(account_move_reconcile, self).unlink()
        
    @api.model
    def create(self, vals):
        rec_move = super(account_move_reconcile, self).create(vals)
        self.register_payment_competence(rec_move)
        return rec_move
    
    @api.model
    def register_payment_competence(self, rec_move):
        competence_obj = self.env['account.move.reconcile.competence']
        digit = self.env['decimal.precision'].precision_get('Account')
        
        # for line in rec_move.line_id:
        if rec_move.line_id:
            line = rec_move.line_id[0]
            if line.journal_id.type in ['cash', 'bank']:
                self = self.with_context(reconcile_type = 'full')
                competence_data = self.register_competence_data(rec_move, 
                                                                line.move_id)
        # for line in rec_move.line_partial_ids:
        if rec_move.line_partial_ids:
            line = rec_move.line_partial_ids[0]
            if line.journal_id.type in ['cash', 'bank']:
                self = self.with_context(reconcile_type = 'partial')
                competence_data = self.register_competence_data(rec_move, 
                                                                line.move_id)
        return True
    
    @api.model
    def register_competence_data(self, rec_move, pay_move):
        move_line_obj = self.env['account.move.line']
        competence_obj = self.env['account.move.reconcile.competence']
        digit = self.env['decimal.precision'].precision_get('Account')
        
        # Competence data
        reconcile_type = self._context.get('reconcile_type',False)
        if reconcile_type == 'partial':
            rec_lines = rec_move.line_partial_ids
        else:
            rec_lines = rec_move.line_id
        rec_lines_ids = [x.id for x in rec_lines]
        
        # Payment data
        payment_account_id = False
        payment_date = False
        for p_line in pay_move.line_id:
            if p_line.account_id.type in ['liquidity']:
                payment_account_id = p_line.account_id.id
                payment_date = p_line.date
        
        # Remove Competence chained by account move.
        for line in rec_lines:
            # Delete competence for move if exists
            domain = [('account_move_id', '=', line.move_id.id)]
            competence_obj.search(domain).unlink()
        
        for line in rec_lines:
            # Start from payment line
            if line.journal_id.type not in ['cash', 'bank']:
                continue
            line_payment = line 
            # Payments lines
            # ... search liquidy lines
            domain = [('move_id', '=', line.move_id.id),
                      ('account_id.type', 'in', ['liquidity'])]
            payment_lines = move_line_obj.search(domain)
            if not payment_lines:
                continue
            payment_amount = 0
            for payment_line in payment_lines:
                if payment_line.credit:
                    payment_amount += payment_line.credit
                else:
                    payment_amount += -1 * payment_line.debit
            
            # Counterpart lines
            domain = [('journal_id.type', 'not in', ['cash', 'bank'])]
            if reconcile_type == 'partial':
                domain.append(('reconcile_partial_id', '=', 
                               line_payment.reconcile_partial_id.id))
            else:
                domain.append(('reconcile_id', '=', 
                               line_payment.reconcile_id.id))
            counterpart_lines = move_line_obj.search(domain)
            
            # ... For not counterpart >> # TO DO
            if not counterpart_lines:
                print "xxx"
            
            # Register competences
            c_partner = False
            for pr_line in counterpart_lines:
                if pr_line.partner_id:
                    c_partner = pr_line.partner_id
                # Amount competence total
                c_amount_total = 0
                if pr_line.credit:
                    c_amount_total = pr_line.credit
                else:
                    c_amount_total = pr_line.debit
                if not c_amount_total:
                    continue
                # Lines whithin payable receivable move
                domain = [('move_id', '=', pr_line.move_id.id), 
                          ('id', '!=', pr_line.id)]
                c_lines = move_line_obj.search(domain)
                
                c_amount_residual = payment_amount
                ln = 1
                for c_line in c_lines:
                    # Coeff payment divide
                    c_amount = 0
                    if c_line.credit:
                        c_amount = c_line.credit
                    else:
                        c_amount = c_line.debit
                    c_coeff = (c_amount / c_amount_total)
                    
                    if ln == len(c_lines):
                        amount = c_amount_residual
                    else:
                        amount = round(payment_amount * c_coeff, digit)
                    
                    period = payment_lines[0].period_id
                    if not period:
                        period = self.env['account.period'].find(\
                                    payment_lines[0].date)[:1]
                    val = {
                        'reconcile_id': rec_move.id, 
                        'competence_account_id': c_line.account_id.id, 
                        'competence_move_line_id': c_line.id, 
                        'competence_name': c_line.name, 
                        'competence_partner_id': c_partner and c_partner.id
                            or False, 
                        'payment_account_id': payment_lines[0].account_id.id, 
                        'payment_date': payment_lines[0].date, 
                        'payment_period_id': payment_lines[0].period_id.id, 
                        'amount': amount, 
                    }
                    comp_move = competence_obj.create(val)
                    
                    c_amount_residual -= amount
                    ln += 1    
        return True
    
    
class account_move(models.Model):
    _inherit = "account.move"
    
    competence_line_ids = fields.One2many('account.move.reconcile.competence', 
                                          'account_move_id', 
                                          string='Competence Lines')
    
    @api.multi
    def validate(self):
        res = super(account_move, self).validate()
        for move in self:
            move.register_payment_competence()
        return res
    
    @api.model
    def register_payment_competence(self):
        """
        2 Conditions for register competence:
        - 1. Any lines with reconcile
        - 2. One account which type "liquidity"
        """
        digit = self.env['decimal.precision'].precision_get('Account')
        move_line_obj = self.env['account.move.line']
        competence_obj = self.env['account.move.reconcile.competence']
        to_register = True
        cr = self.env.cr
        # Delete competence for move if exists
        domain = [('account_move_id', '=', self.id)]
        competence_obj.search(domain).unlink()
        # Verify condition 1: Any lines with reconcile
        if to_register:
            cr.execute("SELECT id FROM account_move_line WHERE move_id = %s \
                AND (reconcile_id != 0 OR reconcile_partial_id != 0)", 
                (str(self.id),))
            if cr.fetchall():
                to_register = False
        # Verify condition 2: One account which type "liquidity"
        liquidity_line_ids = []
        if to_register:
            cr.execute("SELECT ml.id from account_move_line ml \
                LEFT JOIN account_account a ON a.id = ml.account_id \
                WHERE move_id = %s AND a.type='liquidity'", 
                (str(self.id),))
            liquidity_line_ids = [r[0] for r in cr.fetchall()]
            if not liquidity_line_ids:
                to_register = False
        
        # Register competence
        if to_register:
            # ... total payment
            p_amount_total = 0
            for ml in move_line_obj.browse(liquidity_line_ids):
                if ml.credit:
                    amount = -1 * ml.credit
                else: 
                    amount = ml.debit
                p_amount_total += amount
            # ... partner competence
            c_partner = False
            cr.execute("SELECT ml.id from account_move_line ml \
                WHERE move_id = %s AND ml.partner_id != 0", 
                (str(self.id),))
            line_ids = [r[0] for r in cr.fetchall()]
            if line_ids:
                c_partner = move_line_obj.browse(line_ids[0]).partner_id
            # lines compentence
            for c_line in self.line_id:
                if c_line.account_id.type not in ['other']:
                    continue
                c_amount = 0
                if c_line.credit:
                    c_amount = c_line.credit
                else:
                    c_amount = -1 * c_line.debit
                # Division by nr of account liquidity
                ln = 1
                c_amount_residual = c_amount
                for l_line in move_line_obj.browse(liquidity_line_ids):
                    if ml.credit:
                        p_amount = -1 * l_line.credit
                    else: 
                        p_amount = l_line.debit
                    p_coeff = (p_amount / p_amount_total)
                    if ln == len(liquidity_line_ids):
                        amount = c_amount_residual
                    else:
                        amount = round(c_amount * p_coeff, digit)
                    period = l_line.period_id
                    if not period:
                        period = self.env['account.period'].find(\
                                    l_line.date)[:1]
                    val = {
                        'account_move_id': self.id, 
                        'competence_account_id': c_line.account_id.id, 
                        'competence_move_line_id': c_line.id, 
                        'competence_name': c_line.name, 
                        'competence_partner_id': c_partner and c_partner.id
                            or False, 
                        'payment_account_id': l_line.account_id.id, 
                        'payment_date': l_line.date, 
                        'payment_period_id': l_line.period_id.id, 
                        'amount': amount, 
                    }
                    comp_move = competence_obj.create(val)
                    ln += 1
                
        return True
        
    
class account_move_reconcile_competence(models.Model):
    _name = "account.move.reconcile.competence"
    
    reconcile_id = fields.Many2one('account.move.reconcile', readonly=True, 
                                   ondelete="cascade")
    account_move_id = fields.Many2one('account.move', readonly=True, 
                                      ondelete="cascade")
    competence_account_id = fields.Many2one('account.account', 
                                            strong='Account Competence',
                                            required=True)
    competence_move_line_id = fields.Many2one('account.move.line', 
                                   strong='Move Line Competence', 
                                   required=True)
    competence_name = fields.Char(string='Description')
    competence_partner_id = fields.Many2one('res.partner', 
                                            string='Partner Competence')
    payment_account_id = fields.Many2one('account.account', 
                                         string='Account Payment')
    amount = fields.Float(string='Amount')
    payment_date = fields.Date(string='Date Payment')
    payment_period_id = fields.Many2one('account.period', string='Payment Period')
    
    @api.model
    def _initialing_payment_competence(self):
        to_migrate = True
        self.env.cr.execute("SELECT id FROM account_move_reconcile_competence \
                    WHERE id > 0")
        if (self.env.cr.dictfetchall()):
            to_migrate = False
        if to_migrate:
            # Compute from reconciliation
            self.env.cr.execute(
            """
                SELECT id 
                FROM account_move_reconcile 
                ;
            """)
            items = self.env.cr.dictfetchall()
            for it in items:
                rec_move = self.env['account.move.reconcile'].browse(it['id'])
                rec_move.register_payment_competence(rec_move)
            # Compute from account move without reconciliation
            self.env.cr.execute(
            """
                SELECT ml.id from account_move_line ml 
                LEFT JOIN account_account a ON a.id = ml.account_id 
                WHERE  a.type='liquidity' 
                ;
            """)
            items = self.env.cr.dictfetchall()
            for it in items:
                move = self.env['account.move'].browse(it['id'])
                move.register_payment_competence()
                
        return True
