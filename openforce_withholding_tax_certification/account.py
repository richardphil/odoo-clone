# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (alessandrocamilli@openforce.it)
#    Copyright (C) 2015
#    Openforce (<http://www.openforce.it>)
#    
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import orm, fields
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp import netsvc
from datetime import datetime

class withholding_tax(orm.Model):
    _inherit = 'withholding.tax'
    
class withholding_tax_certification(orm.Model):
    _name = 'withholding.tax.certification'
    _description = 'Withholding Tax Certification'
    
    def _compute_total(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            tot_wt_tax = 0
            tot_wt_amount = 0
            tot_wt_paid = 0
            for wt_cert_line in line.line_ids:
                tot_wt_tax += wt_cert_line.tot_wt_tax
                tot_wt_amount += wt_cert_line.tot_wt_amount
                tot_wt_paid += wt_cert_line.tot_wt_paid
            res[line.id] = {
                'tot_wt_tax': tot_wt_tax,
                'tot_wt_amount': tot_wt_amount,
                'tot_wt_paid': tot_wt_paid,
            }
        return res

    _columns = {
            'company_id': fields.many2one('res.company', 'Azienda', required=True),
            'name': fields.char('Name', size=256),
            'year': fields.integer('Year', required=True),
            'line_ids': fields.one2many('withholding.tax.certification.line', 
                                'certification_id', 'Lines'),
            
            'tot_wt_tax': fields.function(_compute_total, string='Tot WT tax',
                                          multi='total'), 
            'tot_wt_amount': fields.function(_compute_total, string='Tot WT tax',
                                          multi='total'), 
            'tot_wt_paid': fields.function(_compute_total, string='Tot WT tax',
                                          multi='total'), 
        }
    _order = 'year desc, name'
    
    def generate_lines(self, cr, uid, ids, context=None):
        wt_move_obj = self.pool['withholding.tax.move']
        wt_cert_line_obj = self.pool['withholding.tax.certification.line']
        wt_cert_move_obj = self.pool['withholding.tax.certification.move']
        account_invoice_obj = self.pool['account.invoice']
        account_move_obj = self.pool['account.move']
        account_move_line_obj = self.pool['account.move.line']
        
        for certification in self.browse(cr, uid, ids):
            
            date_start = datetime.strptime(str(certification.year) + '-01-01', 
                                        "%Y-%m-%d")
            date_stop = datetime.strptime(str(certification.year) + '-12-31', 
                                        "%Y-%m-%d")   
            
            certification_moves = []
            
            # Lines from WT moves
            domain = [('date', '>=', date_start),
                      ('date', '<=', date_stop),]
            wt_move_ids = wt_move_obj.search(cr, uid, domain)
            for wt_move in wt_move_obj.browse(cr, uid, wt_move_ids):
                
                # Compentence line by partner
                domain = [('certification_id', '=', certification.id),
                          ('partner_id', '=', wt_move.partner_id.id)]
                cert_line_ids = wt_cert_line_obj.search(cr, uid, domain)
                if not cert_line_ids:
                    val = {
                        'certification_id': certification.id,       
                        'partner_id': wt_move.partner_id.id or False,       
                    }
                    wt_cert_line_id = wt_cert_line_obj.create(cr, uid, val)
                else:
                    wt_cert_line_id = cert_line_ids[0]
                
                # Certification Move
                # Verify existign move created by another move with same ref
                wt_cert_move_ids = []
                if wt_move.statement_id:
                    domain = [('certification_line_id', '=', wt_cert_line_id),
                            ('move_id', '=', wt_move.statement_id.move_id.id)]
                    wt_cert_move_ids = wt_cert_move_obj.search(cr, uid, domain)
                if not wt_cert_move_ids:
                    move_val = {
                        'certification_line_id': wt_cert_line_id,
                        'move_id': wt_move.statement_id \
                            and wt_move.statement_id.move_id \
                            and wt_move.statement_id.move_id.id 
                            or False,
                        'description': wt_move.statement_id \
                            and wt_move.statement_id.move_id \
                            and wt_move.statement_id.move_id.name 
                            or wt_move.account_move_id\
                            and wt_move.account_move_id.name 
                            or False, 
                        'amount': wt_move.statement_id \
                            and wt_move.statement_id.invoice_id \
                            and wt_move.statement_id.invoice_id.amount_total\
                            or False,
                        'tax_base': wt_move.statement_id \
                            and wt_move.statement_id.invoice_id \
                            and wt_move.statement_id.invoice_id.amount_untaxed\
                            or False,
                        'tax_amount': wt_move.statement_id \
                            and wt_move.statement_id.invoice_id \
                            and wt_move.statement_id.invoice_id.amount_tax\
                            or False,
                        'wt_id': wt_move.statement_id \
                            and wt_move.statement_id.withholding_tax_id \
                            and wt_move.statement_id.withholding_tax_id.id\
                            or wt_move.withholding_tax_id \
                            and wt_move.withholding_tax_id.id\
                            or False,    
                        'wt_base': wt_move.statement_id \
                            and wt_move.statement_id.base \
                            or False,
                        'wt_tax': wt_move.statement_id \
                            and wt_move.statement_id.tax \
                            or False
                        }    
                    wt_cert_move_id = wt_cert_move_obj.create(cr, uid, 
                                                              move_val)
                else:
                    wt_cert_move_id = wt_cert_move_ids[0]
                    
                wt_cert_move = wt_cert_move_obj.browse(cr, uid, 
                                                       wt_cert_move_id)
                # Amount values
                amount_paid = 0
                if wt_move.state in ['paid']:
                    amount_paid = wt_move.amount
                move_val = {
                    'date': wt_move.date,
                    'wt_amount': wt_cert_move.wt_amount + wt_move.amount,
                    'wt_paid': wt_cert_move.wt_paid + amount_paid
                    }
                wt_cert_move_obj.write(cr, uid, [wt_cert_move.id], move_val)
            
        return True
    
    def certification_create(self, cr, uid, parms, context=None):
        account_move_line_obj = self.pool['account.move.line']
        
        if not context:
            context = {}
        
        # Create Head
        val = {
            'company_id': parms['company_id'],
            'year': parms['year'],
        }
        certification_id = self.create(cr, uid, val)
        
        # Create Certification lines
        self.generate_lines(cr, uid, [certification_id], context)
        return certification_id
        
class withholding_tax_certification_line(orm.Model):
    _name = "withholding.tax.certification.line"
    _inherit = ['mail.thread']
    
    def _get_current_certification(self, cr, uid, ids, name, context=None):
        cert_lines = []
        cert_move_obj = self.pool['withholding.tax.certification.move']
        for move in cert_move_obj.browse(cr, uid, ids, context=context):
            cert_lines.append(move.certification_line_id.id)
        return cert_lines
    
    def _compute_total(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            tot_wt_moves = 0
            tot_wt_tax = 0
            tot_wt_amount = 0
            tot_wt_paid = 0
            for wt_move in line.move_ids:
                tot_wt_moves +=1
                tot_wt_tax += wt_move.wt_tax
                tot_wt_amount += wt_move.wt_amount
                tot_wt_paid += wt_move.wt_paid
            res[line.id] = {
                'tot_wt_moves': tot_wt_moves,
                'tot_wt_tax': tot_wt_tax,
                'tot_wt_amount': tot_wt_amount,
                'tot_wt_paid': tot_wt_paid,
            }
        return res
    
    _columns = {
            'state': fields.selection([
                ('draft', 'Draft'),
                ('sent', 'Sent'),
                ], 'Status', readonly=True, copy=False, select=True, 
                                      track_visibility='onchange',),
                
            'certification_id': fields.many2one(
                            'withholding.tax.certification', 'Certification', 
                            readonly=True, ondelete='cascade'),
            'partner_id': fields.many2one('res.partner', 'Partner'),
            'move_ids': fields.one2many('withholding.tax.certification.move', 
                                'certification_line_id', 'Moves'),
            'tot_wt_moves': fields.function(_compute_total, string='Nr Moves', 
                                        multi='total', 
                    store={'withholding.tax.certification.move': (
                                    _get_current_certification, 
                                    ['wt_tax','wt_amount','wt_paid',], 
                                    20),
                        },),
            'tot_wt_tax': fields.function(_compute_total, string='Tot WT tax', 
                                        multi='total', 
                    store={'withholding.tax.certification.move': (
                                    _get_current_certification, 
                                    ['wt_tax','wt_amount','wt_paid',], 
                                    20),
                        },),
            'tot_wt_amount': fields.function(_compute_total, 
                    string='Tot WT amount', multi='total',
                    store={'withholding.tax.certification.move': (
                                    _get_current_certification, 
                                    ['wt_tax','wt_amount','wt_paid',], 
                                    20),
                        },),
            'tot_wt_paid': fields.function(_compute_total, 
                    string='Tot WT paid', multi='total',
                    store={'withholding.tax.certification.move': (
                                    _get_current_certification, 
                                    ['wt_tax','wt_amount','wt_paid',], 
                                    20),
                        },),
        }
    _defaults = {
            'state': 'draft'
        }
    
    def action_send_mail(self, cr, uid, ids, context=None):
        for pt in self.browse(cr, uid, ids):
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, self._name, 
                                    pt.id, 'send', cr)
        return True
    
    def action_set_to_draft(self, cr, uid, ids, context=None):
        for pt in self.browse(cr, uid, ids):
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, self._name, 
                                    pt.id, 'cancel', cr)
        return True
    
    
    def send_mail(self, cr, uid, ids, context=None):
        
        self.write(cr, uid, ids, {'state':'sent'})
        res ={}
        
        ir_model_obj = self.pool.get('ir.model')
        mail_obj = self.pool.get('mail.mail')
        mail_message_obj = self.pool.get('mail.message')
        email_tmpl_obj = self.pool.get('email.template')
        email_compose_message_obj = self.pool.get('mail.compose.message')
        partner_obj = self.pool['res.partner']
        user_obj = self.pool['res.users']
        server_mail_obj = self.pool.get('ir.mail_server')
        
        # Search Template
        template_id = False
        domain = [('model', '=', 'withholding.tax.certification.line')]
        model_ids = ir_model_obj.search(cr, uid, domain)
        if model_ids:
            domain = [('model_id', '=', model_ids[0])]
            et_ids = email_tmpl_obj.search(cr, uid, domain, order='id desc')
            if et_ids:
                template_id = et_ids[0]
        
        if not template_id:
            raise orm.except_orm(_('Error!'),_("Nessun Template specificato"))
            
        email_template = email_tmpl_obj.browse(cr, uid, template_id)
        user = user_obj.browse(cr, uid, uid)
            
        for cer_line in self.browse(cr, uid, ids):
            # user's mail server
            user_server_mail = False
            serv_ids = server_mail_obj.search(cr, uid, [('smtp_user', '=', 
                                user.email)], 
                                order='sequence', limit=1)
            if serv_ids:
                user_server_mail = server_mail_obj.browse(cr, uid, 
                                                          serv_ids[0])
            # Recipients
            recipient_ids = []
            # ... partner
            recipient_ids.append(cer_line.partner_id.id)
            # ... followers
            ## >>>> non gestito x ora
            #for follower in statement.message_follower_ids:
            #    recipient_ids.append(follower.id)
            
            # Compose E-mail
            for recipient in partner_obj.browse(cr, uid, recipient_ids):
                mail_subject = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.subject),
                                self._name, cer_line.id)
                mail_body = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.body_html),
                                self._name, cer_line.id)
                mail_to = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.email_to),
                                self._name, cer_line.id)
                reply_to = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.reply_to),
                                self._name, cer_line.id)
                if user.email:
                    mail_from = user.email
                else:
                    mail_from = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.email_from),
                                    self._name, 
                                    cer_line.id)
                if not mail_from:
                    continue
                
                # body with data line
                message_vals = {
                        'email_from': mail_from,
                        'subject' : mail_subject,
                        'model' : self._name,
                        'res_id' : cer_line.id,
                        'partner_ids' : recipient_ids 
                                        and (6,0,recipient_ids) or False,
                        'notified_partner_ids' : recipient_ids 
                                        and (6,0,recipient_ids) or False,
                        'type' : 'comment',
                        'body' : mail_body,
                        }
                message_id = mail_message_obj.create(cr, uid, message_vals)
                
                mail_id = mail_obj.create(cr, uid, {
                        'mail_message_id' : message_id,
                        'mail_server_id' : user_server_mail 
                            and user_server_mail.id 
                            or email_template.mail_server_id 
                            and email_template.mail_server_id.id 
                            or False,
                        'state' : 'outgoing',
                        'auto_delete' : email_template.auto_delete,
                        'email_from' : mail_from,
                        'email_to' : recipient.email,
                        #'reply_to' : reply_to,
                        'body_html' : mail_body,
                        })
                if mail_id:
                    mail_obj.send(cr, uid, [mail_id])
        
        return True

class withholding_tax_certification_move(orm.Model):
    _name = "withholding.tax.certification.move"

    _columns = {
            'certification_line_id': fields.many2one(
                                'withholding.tax.certification.line',
                                'Move', readonly=True, ondelete='cascade'),
            'move_id': fields.many2one('account.move', 'Account Move', 
                                       readonly=True),
            'date': fields.date('Date competence'),
            'description': fields.char('Description'),
            'amount': fields.float('Amount Total'),
            'tax_base': fields.float('VAT Base'),
            'tax_amount': fields.float('VAT Amount'),
            'wt_id': fields.many2one('withholding.tax', 'Withholding Tax'),
            'wt_base': fields.float('WT Base'),
            'wt_tax': fields.float('WT Tax'),
            'wt_amount': fields.float('WT Amount'),
            'wt_paid': fields.float('WT Paid'),
        }
    _order = 'date'