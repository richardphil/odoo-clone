# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@yahoo.it)
#    Copyright (C) 2014
#    Associazione OpenERP Italia (<http://www.openerp-italia.org>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields,osv, orm
from openerp.tools.translate import _
import time 

class wizard_certification_create(orm.TransientModel):
    
    def default_get(self, cr, uid, fields, context=None):
        user_obj = self.pool['res.users']
        user = user_obj.browse(cr, uid, uid)
        res = super(wizard_certification_create, self).default_get(cr, uid, fields, context=context)
        res['year'] = int(time.strftime('%Y')) -1
        res['company_id'] = user.company_id.id
        return res

    _name = "wizard.certification.create"
    
    _columns = {
        'company_id': fields.many2one('res.company', 'Azienda', required=True ),
        'year' : fields.integer('Year', size=4),
        }
    
    def certification_create(self, cr, uid, ids, context=None):
        certification_obj = self.pool['withholding.tax.certification']
        obj_model = self.pool.get('ir.model.data')
        
        wizard = self.read(cr, uid, ids)[0]
        params ={
               'company_id': wizard['company_id'][0],
               'year': wizard['year'], 
               }
        cert_id = certification_obj.certification_create(cr, uid, params, context=None)
        
        return {
            #'view_type': 'tree',
            'view_mode': 'tree,form',
            'res_model': 'withholding.tax.certification',
            'view_id': False,
            'type': 'ir.actions.act_window',
            #'target': 'new',
            'context': context,
        }
        
        #return {
        #    'type': 'ir.actions.act_window_close',
        #}
