# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (alessandrocamilli@openforce.it)
#    Copyright (C) 2015
#    Openforce (<http://www.openforce.it>)
#    
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _
from openerp.exceptions import except_orm, ValidationError
import openerp.addons.decimal_precision as dp
from openerp import netsvc
from datetime import datetime


class withholding_tax_certification(models.Model):
    _name = 'withholding.tax.certification'
    _description = 'Withholding Tax Certification'
    
    @api.model
    def _default_company(self):
        company_id = self._context.get('company_id',
                                       self.env.user.company_id.id)
        return company_id
    
    @api.one
    @api.depends('line_ids.tot_wt_tax', 'line_ids.tot_wt_amount', 
                 'line_ids.tot_wt_paid')
    def _compute_total(self):
        tot_wt_tax = 0
        tot_wt_amount = 0
        tot_wt_paid = 0
        for line in self.line_ids:
            tot_wt_tax = sum(
                wt_cert_move.wt_tax for wt_cert_move in line.move_ids)
            tot_wt_amount = sum(
                wt_cert_move.wt_amount for wt_cert_move in line.move_ids)
            tot_wt_paid = sum(
                wt_cert_move.wt_paid for wt_cert_move in line.move_ids)
        self.tot_wt_tax = tot_wt_tax
        self.tot_wt_amount = tot_wt_amount
        self.tot_wt_paid = tot_wt_paid

    company_id = fields.Many2one('res.company', 'Company', 
                                 default=_default_company, required=True)
    name = fields.Char('Name', size=256)
    year = fields.Integer('Year', required=True)
    line_ids = fields.One2many('withholding.tax.certification.line', 
                                'certification_id', 'Lines')
    tot_wt_tax = fields.Float(string='Tot WT tax', compute='_compute_total', 
                              store=True)
    tot_wt_amount = fields.Float(string='Tot WT amount', 
                                 compute='_compute_total', store=True)
    tot_wt_paid = fields.Float(string='Tot WT paid', compute='_compute_total',
                               store=True)
    
    _order = 'year desc, name'
    
    @api.one
    def generate_lines(self):
        wt_move_obj = self.env['withholding.tax.move']
        wt_cert_line_obj = self.env['withholding.tax.certification.line']
        wt_cert_move_obj = self.env['withholding.tax.certification.move']
        # Clear existing lines
        for line in self.line_ids:
            line.unlink()
        # Setting params
        date_start = datetime.strptime(str(self.year) + '-01-01', 
                                    "%Y-%m-%d")
        date_stop = datetime.strptime(str(self.year) + '-12-31', 
                                    "%Y-%m-%d")   
        certification_moves = []
        # Lines from WT moves
        domain = [('date', '>=', date_start),
                  ('date', '<=', date_stop),]
        wt_moves = wt_move_obj.search(domain)
        for wt_move in wt_moves:
            # Compentence line by partner
            domain = [('certification_id', '=', self.id),
                      ('partner_id', '=', wt_move.partner_id.id)]
            cert_line_ids = wt_cert_line_obj.search(domain)
            if not cert_line_ids:
                val = {
                    'certification_id': self.id,       
                    'partner_id': wt_move.partner_id.id or False,       
                }
                wt_cert_line = wt_cert_line_obj.create(val)
            else:
                wt_cert_line = cert_line_ids[0]
            
            # Certification Move
            # Verify existign move created by another move with same ref
            wt_cert_move_ids = []
            if wt_move.statement_id:
                domain = [('certification_line_id', '=', wt_cert_line.id),
                        ('move_id', '=', wt_move.statement_id.move_id.id)]
                wt_cert_move_ids = wt_cert_move_obj.search(domain)
            if not wt_cert_move_ids:
                move_val = {
                    'certification_line_id': wt_cert_line.id,
                    'move_id': wt_move.statement_id \
                        and wt_move.statement_id.move_id \
                        and wt_move.statement_id.move_id.id 
                        or False,
                    'description': wt_move.statement_id \
                        and wt_move.statement_id.move_id \
                        and wt_move.statement_id.move_id.name 
                        or wt_move.account_move_id\
                        and wt_move.account_move_id.name 
                        or False, 
                    'amount': wt_move.statement_id \
                        and wt_move.statement_id.invoice_id \
                        and wt_move.statement_id.invoice_id.amount_total\
                        or False,
                    'tax_base': wt_move.statement_id \
                        and wt_move.statement_id.invoice_id \
                        and wt_move.statement_id.invoice_id.amount_untaxed\
                        or False,
                    'tax_amount': wt_move.statement_id \
                        and wt_move.statement_id.invoice_id \
                        and wt_move.statement_id.invoice_id.amount_tax\
                        or False,
                    'wt_id': wt_move.statement_id \
                        and wt_move.statement_id.withholding_tax_id \
                        and wt_move.statement_id.withholding_tax_id.id\
                        or wt_move.withholding_tax_id \
                        and wt_move.withholding_tax_id.id\
                        or False,    
                    'wt_base': wt_move.statement_id \
                        and wt_move.statement_id.base \
                        or False,
                    'wt_tax': wt_move.statement_id \
                        and wt_move.statement_id.tax \
                        or False
                    }    
                wt_cert_move = wt_cert_move_obj.create(move_val)
            else:
                wt_cert_move = wt_cert_move_ids[0]
                
            # Amount values
            amount_paid = 0
            if wt_move.state in ['paid']:
                amount_paid = wt_move.amount
            move_val = {
                'date': wt_move.date,
                'wt_amount': wt_cert_move.wt_amount + wt_move.amount,
                'wt_paid': wt_cert_move.wt_paid + amount_paid
                }
            wt_cert_move.write(move_val)
        return True
    
     
class withholding_tax_certification_line(models.Model):
    _name = "withholding.tax.certification.line"
    _inherit = ['mail.thread']
    
    @api.one
    @api.depends('move_ids.wt_tax', 'move_ids.wt_amount', 'move_ids.wt_paid')
    def _compute_total(self):
        res = {}
        tot_wt_moves = 0
        tot_wt_tax = 0
        tot_wt_amount = 0
        tot_wt_paid = 0
        for wt_move in self.move_ids:
            tot_wt_moves +=1
            tot_wt_tax += wt_move.wt_tax
            tot_wt_amount += wt_move.wt_amount
            tot_wt_paid += wt_move.wt_paid
        
        self.tot_wt_moves = tot_wt_moves
        self.tot_wt_tax = tot_wt_tax
        self.tot_wt_amount = tot_wt_amount
        self.tot_wt_paid = tot_wt_paid
        
    state = fields.Selection([('draft', 'Draft'),('sent', 'Sent'),], 
                             'Status', readonly=True, copy=False, select=True, 
                             track_visibility='onchange', default='draft')
    certification_id = fields.Many2one('withholding.tax.certification', 
                                       'Certification', readonly=True, 
                                       ondelete='cascade')
    partner_id = fields.Many2one('res.partner', 'Partner')
    partner_vat = fields.Char(string='Vat',
                               store=True,
                               related='partner_id.vat')
    partner_fiscalcode = fields.Char(string='Fiscalcode',
                                     store=True, 
                                     related='partner_id.fiscalcode')
    partner_address = fields.Char(string='Address',
                                     store=True, 
                                     related='partner_id.contact_address')
    move_ids = fields.One2many('withholding.tax.certification.move', 
                               'certification_line_id', 'Moves')
    tot_wt_moves = fields.Float(string='Nr Moves', compute='_compute_total')
    tot_wt_tax = fields.Float(string='Tot WT tax', compute='_compute_total')
    tot_wt_amount = fields.Float(string='Tot WT amount', 
                                 compute='_compute_total')
    tot_wt_paid = fields.Float(string='Tot WT paid', compute='_compute_total')
    
    
    def _get_email_template(self):
        '''
        Extend this method for change default email template
        '''
        ir_model_data = self.env['ir.model.data']
        template_id = False
        try:
            template_id = ir_model_data.get_object_reference( 
                'l10n_it_withholding_tax_certification', 
                'wt_certification_line_email_template')[1]
        except ValueError:
            template_id = False
        return template_id
    
    @api.multi
    def action_send_mail(self):
        """
        This function opens a window to compose an email, with the 
        template message loaded by default
        """
        assert len(self) == 1, 'This option should only be used for a single \
            id at a time.'
        import pdb
        pdb.set_trace()
        ir_model_data = self.env['ir.model.data']
        template_id = self._get_email_template()
        compose_form = self.env.ref('mail.email_compose_message_wizard_form', 
                                    False)
        ctx = dict()
        ctx.update({
            'default_model': 'withholding.tax.certification.line',
            'default_res_id': self.id,
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True
        })
        # TODO: workflow implementation
        '''
        for statement in self.browse(cr, uid, ids):
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, 'sale.recursive.statement', statement.id, 'statement_send', cr)
        '''
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': ctx,
        }
        
        return True
    
    """                
    def action_send_mail(self, cr, uid, ids, context=None):
        for pt in self.browse(cr, uid, ids):
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, self._name, 
                                    pt.id, 'send', cr)
        return True
    
    def action_set_to_draft(self, cr, uid, ids, context=None):
        for pt in self.browse(cr, uid, ids):
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(uid, self._name, 
                                    pt.id, 'cancel', cr)
        return True
    
    
    def send_mail(self, cr, uid, ids, context=None):
        
        self.write(cr, uid, ids, {'state':'sent'})
        res ={}
        
        ir_model_obj = self.pool.get('ir.model')
        mail_obj = self.pool.get('mail.mail')
        mail_message_obj = self.pool.get('mail.message')
        email_tmpl_obj = self.pool.get('email.template')
        email_compose_message_obj = self.pool.get('mail.compose.message')
        partner_obj = self.pool['res.partner']
        user_obj = self.pool['res.users']
        server_mail_obj = self.pool.get('ir.mail_server')
        
        # Search Template
        template_id = False
        domain = [('model', '=', 'withholding.tax.certification.line')]
        model_ids = ir_model_obj.search(cr, uid, domain)
        if model_ids:
            domain = [('model_id', '=', model_ids[0])]
            et_ids = email_tmpl_obj.search(cr, uid, domain, order='id desc')
            if et_ids:
                template_id = et_ids[0]
        
        if not template_id:
            raise orm.except_orm(_('Error!'),_("Nessun Template specificato"))
            
        email_template = email_tmpl_obj.browse(cr, uid, template_id)
        user = user_obj.browse(cr, uid, uid)
            
        for cer_line in self.browse(cr, uid, ids):
            # user's mail server
            user_server_mail = False
            serv_ids = server_mail_obj.search(cr, uid, [('smtp_user', '=', 
                                user.email)], 
                                order='sequence', limit=1)
            if serv_ids:
                user_server_mail = server_mail_obj.browse(cr, uid, 
                                                          serv_ids[0])
            # Recipients
            recipient_ids = []
            # ... partner
            recipient_ids.append(cer_line.partner_id.id)
            # ... followers
            ## >>>> non gestito x ora
            #for follower in statement.message_follower_ids:
            #    recipient_ids.append(follower.id)
            
            # Compose E-mail
            for recipient in partner_obj.browse(cr, uid, recipient_ids):
                mail_subject = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.subject),
                                self._name, cer_line.id)
                mail_body = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.body_html),
                                self._name, cer_line.id)
                mail_to = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.email_to),
                                self._name, cer_line.id)
                reply_to = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.reply_to),
                                self._name, cer_line.id)
                if user.email:
                    mail_from = user.email
                else:
                    mail_from = email_compose_message_obj.render_template(
                                    cr,uid,_(email_template.email_from),
                                    self._name, 
                                    cer_line.id)
                if not mail_from:
                    continue
                
                # body with data line
                message_vals = {
                        'email_from': mail_from,
                        'subject' : mail_subject,
                        'model' : self._name,
                        'res_id' : cer_line.id,
                        'partner_ids' : recipient_ids 
                                        and (6,0,recipient_ids) or False,
                        'notified_partner_ids' : recipient_ids 
                                        and (6,0,recipient_ids) or False,
                        'type' : 'comment',
                        'body' : mail_body,
                        }
                message_id = mail_message_obj.create(cr, uid, message_vals)
                
                mail_id = mail_obj.create(cr, uid, {
                        'mail_message_id' : message_id,
                        'mail_server_id' : user_server_mail 
                            and user_server_mail.id 
                            or email_template.mail_server_id 
                            and email_template.mail_server_id.id 
                            or False,
                        'state' : 'outgoing',
                        'auto_delete' : email_template.auto_delete,
                        'email_from' : mail_from,
                        'email_to' : recipient.email,
                        #'reply_to' : reply_to,
                        'body_html' : mail_body,
                        })
                if mail_id:
                    mail_obj.send(cr, uid, [mail_id])
        
        return True
        """

class withholding_tax_certification_move(models.Model):
    _name = "withholding.tax.certification.move"
    
    certification_line_id = fields.Many2one(
        'withholding.tax.certification.line', 'Move', readonly=True, 
        ondelete='cascade')
    move_id = fields.Many2one('account.move', 'Account Move', readonly=True)
    date = fields.Date('Date competence')
    description = fields.Char('Description')
    amount = fields.Float('Amount Total')
    tax_base = fields.Float('VAT Base')
    tax_amount = fields.Float('VAT Amount')
    wt_id = fields.Many2one('withholding.tax', 'Withholding Tax')
    wt_base = fields.Float('WT Base')
    wt_tax = fields.Float('WT Tax')
    wt_amount = fields.Float('WT Amount')
    wt_paid = fields.Float('WT Paid')
    
    _order = 'date'