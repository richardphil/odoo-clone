# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#
#    Copyright (C) 2016
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _
from openerp.exceptions import except_orm, ValidationError
from datetime import date, timedelta, datetime


class project_feed(models.Model):
    _name = 'project.feed'
    _inherit = ['mail.thread']
    
    name = fields.Char(string='Name', required=True, readonly=True, 
                       states={'draft': [('readonly', False)]})
    type_id = fields.Many2one('project.feed.type', string='Type', 
                              required=True)
    date = fields.Date(string='Date', readonly=True, 
                       states={'draft': [('readonly', False)]})
    state = fields.Selection([
            ('draft','Draft'),
            ('to_approve','To Approve'),
            ('approved','Approved'),
        ], string='Status', readonly=True, default='draft',
        track_visibility='onchange', copy=False,
        states={'draft': [('readonly', False)]})
    question_ids = fields.One2many('project.feed.question', 'feed_id', 
                                   string='Lines', readonly=True, 
                                   states={'draft': [('readonly', False)]})
    task_ids = fields.One2many('project.feed.task', 'feed_id', string='Tasks',
                               readonly=True, 
                               states={'draft': [('readonly', False)]})
    attendant_ids = fields.One2many('project.feed.attendant', 'feed_id', 
                                    string='Attendants', readonly=True, 
                                    states={'draft': [('readonly', False)]})
    project_id = fields.Many2one('project.project', readonly=True, 
                                 string='Project', copy=False)
    sequence = fields.Integer(string='Sequence')
    partner_id = fields.Many2one('res.partner', string='Partner',  
                                 related='project_id.partner_id', 
                                 readonly=True)
    
    @api.multi
    def name_get(self):
        result = []
        for feed in self:
            result.append((feed.id, "%s [%s]" % (feed.name or '', feed.project_id and feed.project_id.name or '')))
        return result
    
    @api.multi
    def feed_to_draft(self):
        result = []
        for feed in self:
            feed.state = 'draft'
    
    @api.multi
    def feed_to_approve(self):
        result = []
        for feed in self:
            for feed_task in feed.task_ids:
                feed_task.create_project_task()
            feed.state = 'to_approve'
            
    @api.multi
    def feed_approved(self):
        result = []
        for feed in self:
            feed.state = 'approved'

    @api.model
    def generate_document_from_template(self, feed_source_id):
        project_id = False
        if self._context.get('active_model', False) == 'project.project':
            project_id = self._context.get('active_id', False)
        feed_source = self.browse(feed_source_id)
        vals = {
            'project_id': project_id,
            'date': fields.Date.context_today(self),
            }
        new_feed = feed_source.copy(vals)
        # copy questions
        for question in feed_source.question_ids:
            vals = {
                'feed_id': new_feed.id,
            }
            question_new = question.copy(vals)
        # copy tasks
        for task in feed_source.task_ids:
            vals = {
                'feed_id': new_feed.id,
            }
            task_new = task.copy(vals)
            
    @api.multi
    def feed_print(self):
        assert len(self) == 1, 'This option should only be used for a single \
            id at a time.'
        return self.env['report'].get_action(self, 'project_feed.report_feed')
    
    def has_question(self, type):
        domain = []
        domain.append(('feed_id', '=', self.id))
        if type == 'yes':
            domain.append(('answer_yes_type', '=', True))
        if type == 'no':
            domain.append(('answer_no_type', '=', True))
        if type == 'undecided':
            domain.append(('answer_undecided_type', '=', True))
        if type == 'text':
            domain.append(('answer_text_type', '=', True))
        if type == 'risk':
            domain.append(('answer_risk_type', '=', True))
        if type in ('attach', 'attachment'):
            domain.append(('answer_attachment_ids_type', '=', True))
        questions = self.env['project.feed.question'].search(domain)
        if questions:
            return True
        else:
            return False


class project_feed_task(models.Model):
    _name = 'project.feed.task'        
    
    feed_id = fields.Many2one('project.feed', readonly=True, 
                              string='Feed', copy=False, ondelete="cascade")
    sequence = fields.Integer(string='Sequence')
    name = fields.Char(string='Name', required=True)
    user_id = fields.Many2one('res.users', string='Assigned to')
    reviewer_id = fields.Many2one('res.users', string='Reviewer')
    deadline = fields.Datetime(string='Deadline')
    days = fields.Float(string='Work Days')
    date_start = fields.Datetime(string='Date Start')
    date_stop = fields.Datetime(string='Date Stop')
    description = fields.Text(string='Description')
    task_id = fields.Many2one('project.task', string='Task')
    
    @api.onchange('days')
    def change_days(self):
        # Recompute date start and stop
        if self.deadline:
            self.date_stop = self.deadline
            self.date_start = datetime.strptime(self.date_stop,"%Y-%m-%d %H:%M:%S") - \
                timedelta(days=self.days)
    
    @api.onchange('deadline')
    def change_deadline(self):
        self.change_days()
    
    @api.one
    def create_project_task(self):
        if not self.task_id:
            val = {
                'project_id' : self.feed_id.project_id.id,
                'name' : self.name,
                'date_deadline' : self.deadline,
                'date_start' : self.date_start,
                'date_end' : self.date_stop,
                'partner_id' : self.feed_id.partner_id and 
                    self.feed_id.partner_id.id or False,
            }
            task = self.env['project.task'].create(val)
            self.task_id = task.id
    
    @api.multi
    def open_project_task(self):
        self.ensure_one() 
        return {
            'view_mode': 'form',
            'res_model': 'project.task',
            'view_id': False,
            'type': 'ir.actions.act_window',
            # 'nodestroy': True,
            'res_id': self.task_id.id,
            # 'domain': [('id', 'in', aml_ids)],
        }


class project_feed_attendant(models.Model):
    _name = 'project.feed.attendant'        
    
    feed_id = fields.Many2one('project.feed', readonly=True, 
                              string='Feed', copy=False, ondelete="cascade")
    sequence = fields.Integer(string='Sequence')
    name = fields.Char(string='Name', required=True)
    follower = fields.Boolean(string='Follower')
    email = fields.Char(string='E-mail')
    

class project_feed_type(models.Model):
    _name = 'project.feed.type'        
    
    name = fields.Char(string='Name', required=True)
    sequence = fields.Integer(string='Sequence', required=True)
    mobile_create = fields.Boolean(string='Mobile can create')
    mobile_write = fields.Boolean(string='Mobile can edit')
    mobile_read = fields.Boolean(string='Mobile can read')
    mobile_delete = fields.Boolean(string='Mobile can delete')
    
    _order = 'sequence'


class project_feed_question(models.Model):
    _name = 'project.feed.question'
    
    @api.model
    def _default_type_id(self):
        domain = [('is_default', '=', True)]
        type = self.env['project.feed.question.type'].search(
                    domain, limit=1, order='id desc')
        if not type:
            raise ValidationError(
                _('One default question type must be configured'))
        return type.id
    
    feed_id = fields.Many2one('project.feed', readonly=True, 
                              string='Feed', copy=False, ondelete="cascade")
    sequence = fields.Integer(string='Sequence')
    type_id = fields.Many2one('project.feed.question.type', string='Type', 
                              required=True, default=_default_type_id)
    question = fields.Text(string='Question', required=True)
    answer_yes = fields.Boolean(string='Yes')
    answer_yes_type = fields.Boolean(string='Yes', 
                                     related='type_id.answer_yes')
    answer_no = fields.Boolean(string='No')
    answer_no_type = fields.Boolean(string='No', 
                                    related='type_id.answer_no')
    answer_undecided = fields.Boolean(string='Undecided')
    answer_undecided_type = fields.Boolean(string='Undecided', 
                                           related='type_id.answer_undecided')
    answer_text = fields.Text(string='Text')
    answer_text_type = fields.Boolean(string='Text', 
                                      related='type_id.answer_text')
    answer_risk_id = fields.Many2one('project.feed.question.risk', 
                                     string='Risk')
    answer_risk_type = fields.Boolean(string='Risk', 
                                      related='type_id.answer_risk')
    answer_risk_color = fields.Char(string='Risk Color', 
                                    related='answer_risk_id.color', 
                                    readonly=True)
    answer_attachment_ids = fields.Many2many('ir.attachment', 
                                             string="Attachments")
    answer_attachment_ids_type = fields.Boolean(string='Attachments', 
                                      related='type_id.answer_attachment_ids')
    
    _order = 'sequence'
    
    @api.multi
    def action_advanced_detail(self):
        context = {}
        return { 
            'name':_("Question"),
            'view_mode': 'form',
            'view_id': False,
            'view_type': 'form',
            'res_model': 'project.feed.question',
            'res_id': self[0].id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            #'target': 'new',
            'domain': '[]',
            'context': context
        }
 

class project_feed_question_type(models.Model):
    _name = 'project.feed.question.type'
    
    name = fields.Char(string='Name', required=True)
    is_default = fields.Boolean(string='Default Type')
    answer_yes = fields.Boolean(string='Yes')
    answer_no = fields.Boolean(string='No')
    answer_undecided = fields.Boolean(string='Undecided')
    answer_text = fields.Boolean(string='Text')
    answer_risk = fields.Boolean(string='Risk')
    answer_attachment_ids = fields.Boolean(string='Attachments')
    

class project_feed_question_risk(models.Model):
    _name = 'project.feed.question.risk'
    
    name = fields.Char(string='Name', required=True)
    color = fields.Char(string='Color')

