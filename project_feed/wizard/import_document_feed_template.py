# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#
#    Copyright (C) 2016
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _


class wizard_import_document_feed_template(models.TransientModel):
    _name = "wizard.import.document.feed.template"
    _description = "Wizard to import project feed template"
    
    
    feed_id = fields.Many2one('project.feed', string="Document Feed Template", 
                              required=True)

    @api.multi
    def execute(self):
        for wiz_obj in self:
            self.env['project.feed'].generate_document_from_template(
                wiz_obj.feed_id.id)
        
        return {'type': 'ir.actions.act_window_close'}

