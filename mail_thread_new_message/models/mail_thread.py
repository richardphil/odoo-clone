# -*- coding: utf-8 -*-
#################################################################################
#    Author: Alessandro Camilli a.camilli@yahoo.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
import time
import base64
from openerp.addons.mail.mail_message import decode

class mail_thread(osv.AbstractModel):
    
    _inherit = 'mail.thread'
    
    def message_new(self, cr, uid, msg_dict, custom_values=None, context=None):
        """Called by ``message_process`` when a new message is received
           for a given thread model, if the message did not belong to
           an existing thread.
           The default behavior is to create a new record of the corresponding
           model (based on some very basic info extracted from the message).
           Additional behavior may be implemented by overriding this method.

           :param dict msg_dict: a map containing the email details and
                                 attachments. See ``message_process`` and
                                ``mail.message.parse`` for details.
           :param dict custom_values: optional dictionary of additional
                                      field values to pass to create()
                                      when creating the new thread record.
                                      Be careful, these values may override
                                      any other values coming from the message.
           :param dict context: if a ``thread_model`` value is present
                                in the context, its value will be used
                                to determine the model of the record
                                to create (instead of the current model).
           :rtype: int
           :return: the id of the newly created thread object
        """
        model = context.get('thread_model') or self._name
        if model == 'mail.thread':
            res_id = self.message_new_mail_thread(cr, uid, msg_dict, 
                                                  custom_values, context)
        else:
            res_id = super(mail_thread, self).message_new(cr, uid, msg_dict, 
                                            custom_values=None, context=None)
        return res_id

    
    def message_new_mail_thread(self, cr, uid, msg_dict, custom_values=None, context=None):
        """Called by ``message_process`` when a new message is received
           for a given thread model, if the message did not belong to
           an existing thread.
           The default behavior is to create a new record of the corresponding
           model (based on some very basic info extracted from the message).
           Additional behavior may be implemented by overriding this method.

           :param dict msg_dict: a map containing the email details and
                                 attachments. See ``message_process`` and
                                ``mail.message.parse`` for details.
           :param dict custom_values: optional dictionary of additional
                                      field values to pass to create()
                                      when creating the new thread record.
                                      Be careful, these values may override
                                      any other values coming from the message.
           :param dict context: if a ``thread_model`` value is present
                                in the context, its value will be used
                                to determine the model of the record
                                to create (instead of the current model).
           :rtype: int
           :return: the id of the newly created thread object
        """
        if context is None:
            context = {}
        partner_obj = self.pool['res.partner']
        mail_message_obj = self.pool['mail.message']
        mail_mail_obj = self.pool['mail.mail']   
        
        # Partner IDS:
        recipients = False
        email_to_text = msg_dict['to']
        email_to = ""
        if not email_to:
            for text in email_to_text.split('<'):
                if '@' in text:
                    email_to = text.replace('<', '').replace('>','')
                    break
        if not email_to:
            for text in email_to_text.split('"'):
                if '@' in text:
                    email_to = text
                    break
        
        if email_to:
            msg_dict['to'] = email_to
        domain = [('email', '=', decode(msg_dict['to']))]
        partner_ids = partner_obj.search(cr, uid, domain)
        if partner_ids:
            recipients = [(6, 0, partner_ids)]
        val = {
               'type': 'email',
               'body' : msg_dict['body'],
               'email_from': msg_dict['email_from'],
               'message_id': msg_dict['message_id'],
               'subject': 'subject' in msg_dict and msg_dict['subject'] or False,
               'date': msg_dict['date'],
               'author_id': msg_dict['author_id'],
               'partner_ids': recipients,
               #'attachment_ids': attachments, >> after create
        }
        message_id = mail_message_obj.create(cr, uid, val)
        
        # Attachement pre process
        attachments = msg_dict['attachments']
        attachment_ids = []
        if attachments:
            attach_model = 'mail.message'
            attach_res_id = message_id
            attachment_ids = self._message_preprocess_attachments(cr, uid, 
                                                        attachments,
                                                        attachment_ids, 
                                                        attach_model, 
                                                        attach_res_id, 
                                                        context=None)
        # Link attachments to message
        if attachment_ids:
            mail_message_obj.write(cr, uid, [message_id],{'attachment_ids': attachment_ids})
            
            # Remove attachments to avoid duplicates
            if 'attachments' in msg_dict:
                msg_dict['attachments'] = []
        
       
       
       
       
        res_id = False
        '''
        data = {}
        if isinstance(custom_values, dict):
            data = custom_values.copy()
        model = context.get('thread_model') or self._name
        model_pool = self.pool[model]
        fields = model_pool.fields_get(cr, uid, context=context)
        if 'name' in fields and not data.get('name'):
            data['name'] = msg_dict.get('subject', '')
        res_id = model_pool.create(cr, uid, data, context=context)
        '''
        return res_id
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: