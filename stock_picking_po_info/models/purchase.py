# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _

class purchase_order(models.Model):
    _inherit = 'purchase.order'

    def action_picking_create(self, cr, uid, ids, context=None):
        """
        Override Method v7
        """
        picking_id = super(purchase_order, self).action_picking_create(cr, uid, ids, context=None)

        stock_obj = self.pool.get('stock.picking')

        for order in self.browse(cr, uid, ids):
            stock_obj.write(
                cr, uid, [picking_id], {'po_date_order': order.date_order, 'po_currency_id': order.currency_id.id})

        return picking_id