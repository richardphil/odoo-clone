# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@yahoo.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, orm


class account_invoice(orm.Model):
    
    _inherit = "account.invoice"
    _columns = {
        'cig_ref': fields.char('CIG Code', size=15, readonly=True ,states={'draft': [('readonly', False)]}),
        'cup_ref': fields.char('CUP Code', size=15, readonly=True ,states={'draft': [('readonly', False)]}),
        }

class sale_order(orm.Model):
    
    _inherit = "sale.order"
    _columns = {
        'cig_ref': fields.char('CIG Code', size=15, readonly=True ,states={'draft': [('readonly', False)]}),
        'cup_ref': fields.char('CUP Code', size=15, readonly=True ,states={'draft': [('readonly', False)]}),
        }
    
    def _make_invoice(self, cr, uid, order, lines, context={}):
        invoice_obj = self.pool['account.invoice']
        inv_id = super(sale_order, self)._make_invoice(cr, uid, order, lines, context)
        invoice_obj.write(cr, uid, inv_id, {
            'cig_ref': order.cig_ref,
            'cup_ref': order.cup_ref,
            })
        return inv_id
    
    def action_ship_create(self, cr, uid, ids, *args):
        picking_out_obj = self.pool['stock.picking.out']
        super(sale_order, self).action_ship_create(cr, uid, ids, *args)
        for order in self.browse(cr, uid, ids, context={}):
            picking_ids = picking_out_obj.search(cr, uid, [('sale_id', '=', order.id)])
            for picking_id in picking_ids:
                picking_out_obj.write(cr, uid, picking_id, {
                    'cig_ref': order.cig_ref,
                    'cup_ref': order.cup_ref,
                    })
        return True

    
class sale_advance_payment_inv(osv.osv_memory):
    '''
    Create invoice from advanced wizard (Percentage).
    '''
    _inherit = "sale.advance.payment.inv"
    
    def open_invoices(self, cr, uid, ids, invoice_ids, context=None):
        '''
        It intercept invoices just created before to open a view
        '''
        sale_ids = context.get('active_ids', [])
        if sale_ids:
            for so in self.pool['sale.order'].browse(cr, uid, sale_ids):
                if so.cig_ref or so.cup_ref:
                    val = {
                        'cig_ref': so.cig_ref, 
                        'cup_ref': so.cup_ref 
                    }
                    self.pool['account.invoice'].write(cr, uid, 
                                                       invoice_ids, val)
                    
        return super(sale_advance_payment_inv, self).open_invoices(cr, uid, 
                                                ids, 
                                                invoice_ids, context=context)
    
class stock_picking_out(orm.Model):
    
    _inherit = "stock.picking.out"
    _columns = {
        'cig_ref': fields.char('CIG Code', size=15, readonly=True ,states={'draft': [('readonly', False)]}),
        'cup_ref': fields.char('CUP Code', size=15, readonly=True ,states={'draft': [('readonly', False)]}),
        }
    
class stock_picking(orm.Model):
    
    _inherit = "stock.picking"
    _columns = {
        'cig_ref': fields.char('CIG Code', size=15, readonly=True ,states={'draft': [('readonly', False)]}),
        'cup_ref': fields.char('CUP Code', size=15, readonly=True ,states={'draft': [('readonly', False)]}),
        }
    
    def _invoice_hook(self, cr, uid, picking, invoice_id):
        invoice_obj = self.pool['account.invoice']
        
        invoice_obj.write(cr, uid, invoice_id, {
                    'cig_ref': picking.cig_ref,
                    'cup_ref': picking.cup_ref,
                    })
         
        return super(stock_picking, self)._invoice_hook(cr, uid, picking, invoice_id)
    