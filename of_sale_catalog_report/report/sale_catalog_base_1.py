# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2005-2006 CamptoCamp
# Copyright (c) 2006-2010 OpenERP S.A
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsibility of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# guarantees and support are strongly advised to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#
##############################################################################

import time
from openerp.osv import osv, fields, orm
from openerp.report import report_sxw
from common_report_header import common_report_header
from compiler.pyassem import order_blocks
#from numpy.oldnumeric.ma import domain_check_interval

from openerp.addons.web.http import request

class Report(orm.Model):
    _inherit = "report"
    

class sale_catalog_base(report_sxw.rml_parse, common_report_header):
    _name = 'report.sale.catalog.base'

    def set_context(self, objects, data, ids, report_type=None):
        if (data['model'] == 'ir.ui.menu'):
            wiz_obj = self.pool['sale.catalog.report.base']
            objects = wiz_obj.browse(self.cr, self.uid, ids)
        #return super(sale_catalog_base, self).set_context(objects, data, new_ids, report_type=report_type)
        return super(sale_catalog_base, self).set_context(objects, data, ids, report_type=report_type)

    def __init__(self, cr, uid, name, context=None):
        if context is None:
            context = {}
        super(sale_catalog_base, self).__init__(cr, uid, name, context=context)
        self.query = ""
        self.tot_currency = 0.0
        self.period_sql = ""
        self.sold_accounts = {}
        self.sortby = 'sort_date'
        self.localcontext.update( {
            'time': time,
            'lines': self.lines,
            'lines_grouped': self.lines_grouped,
            'get_sortby': self._get_sortby,
            'get_journal': self._get_journal,
            'get_price': self._get_price,
            'get_pricelist': self._get_pricelist,
            'get_valid_to': self._get_valid_to,
            'get_product_group': self.get_product_group,
        })
        self.context = context
   
    def lines(self, data):
        product_obj = self.pool['product.template']
        domain = [('print_on_sale_catalog', '=', True),
                  ('sale_ok', '=', True),
                  ('active', '=', True)]
        order_by = 'name'
        if data['form']['sortby'] == 'sort_code':
            order_by = 'default_code'
        product_ids = product_obj.search(self.cr, self.uid, domain, 
                                                order = order_by)
        res = []
        for product in product_obj.browse(self.cr, self.uid, product_ids, self.context):
            ##if product.sale_catalog_is_linked_to:
            ##    continue
            data = {
                'name': product.name,
                'product': product,
                'group': False
            }
            res.append(product)
        return res
    '''
    def lines_grouped_old(self, product_parent):
        product_obj = self.pool['product.template']
        res =[]
        if product_parent.print_on_sale_catalog:
            if len(product_parent.sale_catalog_child_consol_ids) > 0:
                order = 'default_code'
                product_ordered = product_parent.get_catalog_ordered_child_ids(
                                                            order,
                                                            context = None)
            # Add product childs
            for product_child in product_ordered:
                # To avoid the same product parent-child
                if not product_child.print_on_sale_catalog:
                    continue
                # To avoid the same product parent-child
                #if product_child.id == product_parent.id:
                #    continue
                res.append(product_child)
        return res'''
    
    def lines_grouped(self, product):
        product_group = self.get_product_group(product)
        res =[]
        for product in product_group.product_child_ids:
            if not product.print_on_sale_catalog:
                    continue
            res.append(product)
        return res
    
    def get_product_group(self, product):
        group = False
        product_group_obj = self.pool['sale.catalog.product.group']
        if product:
            domain = [('product_child_ids', 'ilike', product.id)]
            group_ids = product_group_obj.search(self.cr, self.uid, domain, 
                                               order='id desc', limit=1)
            if group_ids :
                group = product_group_obj.browse(self.cr, self.uid, group_ids[0])
        return group
       
    def _get_account(self, data):
        if data['model'] == 'account.account':
            return self.pool.get('account.account').browse(self.cr, self.uid, data['form']['id']).company_id.name
        return super(sale_catalog_base ,self)._get_account(data)

    def _get_sortby(self, data):
        if self.sortby == 'sort_date':
            return self._translate('Date')
        elif self.sortby == 'sort_journal_partner':
            return self._translate('Journal & Partner')
        return self._translate('Date')


class report_salecatalogbase(orm.AbstractModel):
    _name = 'report.of_sale_catalog_report.report_salecatalogbase'
    #_name = 'of_sale_catalog_report.report_salecatalogbase'
    _inherit = 'report.abstract_report'
    _template = 'of_sale_catalog_report.report_salecatalogbase'
    _wrapped_report_class = sale_catalog_base

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
