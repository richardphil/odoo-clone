# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2014
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, orm
from openerp.tools.translate import _
from datetime import datetime, time, timedelta
from openerp import netsvc
from openerp import tools
from operator import itemgetter
#from account_vat_period_end_statement.report import vat_period_end_statement
#from of_sale_catalog_report.tools import image

class sale_catalog_product_group(orm.Model):
    _name="sale.catalog.product.group"
    
    def _compute_total(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = {
                'tot_products': len(line.product_child_ids) or 0,
            }
        return res
    
    def get_sorted_childs(self, cr, uid, group_id, context=None):
        if not context :
            context = {}
        order_by = context.get('order', 'name')
        lines = []
        group = self.browse(cr, uid, group_id, context)
        for product in group.product_child_ids:
            if not product.print_on_sale_catalog:
                    continue
            data = {
                'product_id': product.id,
                'name': product.name,
            }
            if data:
                lines.append(data)
         # Sort by name
        lines_sorted = sorted(lines, key=itemgetter(order_by))
        res = lines_sorted  
        return res
    
    _columns = {
        'name': fields.char('Name'),
        'product_ref_id': fields.many2one('product.product', 'Product Ref'),
        'product_child_ids': fields.many2many(
            'product.product', 'sale_catalog_child_group_rel', 'group_id',
            'product_id', 'Product childs'),
        'tot_products': fields.function(_compute_total, string='Tot Products',
                                          multi='total'), 
    }
                
class sale_catalog_child_group_rel(orm.Model):
    _name="sale.catalog.child.group.rel"
    
    _columns = {
        'name': fields.char('Name'),
        'group_id': fields.many2one('sale.catalog.product.group', 'Group_id'),
        'product_id': fields.many2one('product.product', 'Product'),
        'sequence': fields.integer('Sequence'),
    }
class product_template(orm.Model):
    _inherit="product.template"
    
    def image_resize_image_catalog(self, base64_source, size=(250, 250), encoding='base64', filetype=None, avoid_if_small=False):
        """ Wrapper on image_resize_image, to resize to the standard for catalog 
            image size: 400x400.
            :param size, encoding, filetype, avoid_if_small: refer to image_resize_image
        """
        return tools.image_resize_image(base64_source, size, encoding, filetype, avoid_if_small)

    def image_get_resized_images_catalog(self, base64_source, return_big=False, return_medium=True, return_small=True,
        big_name='image', medium_name='image_medium', small_name='image_small',
        avoid_resize_big=True, avoid_resize_medium=False, avoid_resize_small=False):
        """ Standard tool function that returns a dictionary containing the
            big, medium and small versions of the source image. This function
            is meant to be used for the methods of functional fields for
            models using images.
    
            Default parameters are given to be used for the getter of functional
            image fields,  for example with res.users or res.partner. It returns
            only image_medium and image_small values, to update those fields.
    
            :param base64_source: base64-encoded version of the source
                image; if False, all returnes values will be False
            :param return_{..}: if set, computes and return the related resizing
                of the image
            :param {..}_name: key of the resized image in the return dictionary;
                'image', 'image_medium' and 'image_small' by default.
            :param avoid_resize_[..]: see avoid_if_small parameter
            :return return_dict: dictionary with resized images, depending on
                previous parameters.
        """
        return_dict = dict()
        return_dict['image_catalog'] = self.image_resize_image_catalog(base64_source, avoid_if_small=avoid_resize_big)
        '''
        if return_big:
            return_dict[big_name] = image_resize_image_big(base64_source, avoid_if_small=avoid_resize_big)
        if return_medium:
            return_dict[medium_name] = image_resize_image_medium(base64_source, avoid_if_small=avoid_resize_medium)
        if return_small:
            return_dict[small_name] = image_resize_image_small(base64_source, avoid_if_small=avoid_resize_small)
        '''
        return return_dict
    
    def _get_image_catalog(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = obj.image_get_resized_images_catalog(obj.image, 
                                                    avoid_resize_medium=False)
        return result

    def _set_image_catalog(self, cr, uid, id, name, value, args, context=None):
        return self.write(cr, uid, [id], 
                    {'image_catalog': self.image_resize_image_catalog(value)}, 
                                                                context=context)
    
    def _sale_catalog_is_linked_to(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for record in self.browse(cr, uid, ids):
            cr.execute("""SELECT *
                    FROM product_sale_catalog_rel 
                    WHERE child_id = %s 
                    LIMIT 1 """, (record.id,))
            rel = cr.fetchone()
            if (rel) and len(record.sale_catalog_child_consol_ids) == 0:
                res[record.id] = True
            else:
                res[record.id] = False
        return res    
    
    def _get_sale_catalog_child_ids(self, cr, uid, ids, field_name, arg, context=None):
        result = {}
        for record in self.browse(cr, uid, ids, context=context):
            #if record.child_parent_ids:
            if record.sale_catalog_child_parent_ids:
                result[record.id] = [x.id for x in record.sale_catalog_child_parent_ids]
            else:
                result[record.id] = []

            if record.sale_catalog_child_consol_ids:
                for acc in record.sale_catalog_child_consol_ids:
                    if acc.id not in result[record.id]:
                        result[record.id].append(acc.id)
        return result
    
    _columns = {
        'print_on_sale_catalog': fields.boolean(
            'Print on sale catalog',
            help="Print on sale catalog"
        ),
        'image_catalog': fields.function(_get_image_catalog, fnct_inv=_set_image_catalog,
            string="Catalog-sized image", type="binary", multi="_get_image_catalog",
            store={
                'product.template': (lambda self, cr, uid, ids, c={}: ids, ['image','description_purchase'], 10),
            },
            help="Small-sized image of the product. It is automatically "\
                 "resized as a 400x400px image, with aspect ratio preserved. "\
                 "Use this field anywhere a small image is required."),
    }
    _defaults = {
        'print_on_sale_catalog': True,
    }
    
    def get_catalog_ordered_child_ids(self, cr, uid, ids, order, context=None):
        if not order:
            order = 'id'
        res = []
        ids_to_order = []
        for product in self.browse(cr, uid, ids, context=context):
            for product_child in product.sale_catalog_child_consol_ids:
                ids_to_order.append(product_child.id)
                
            if ids_to_order:
                domain = [('id', 'in', ids_to_order)]
                self.search(cr, uid, domain, order=order)
            # New order
            for product_child in self.browse(cr, uid, ids_to_order):
                res.append(product_child)
        return res
