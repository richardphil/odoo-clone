# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime, time, timedelta

class sale_catalog_report_base(osv.osv_memory):
    _inherit = "sale.catalog.common.report"
    _name = "sale.catalog.report.base"
    _description = "Sale Catalog Report Base"
    
    _columns = {
        'landscape': fields.boolean("Landscape Mode"),
        'amount_currency': fields.boolean("With Currency", help="It adds the currency column on report if the currency differs from the company currency."),
        'sortby': fields.selection([('sort_name', 'Name'), ('sort_code', 'Code'), ('sort_category', 'Category')], 'Sort by', required=True),
        'product_category_ids': fields.many2many('product.category', 'sale_catalog_report_base_category_rel', 'product_id', 'category_id', 'Product Categories'),
    }
    
    def _pricelist_get(self, cr, uid, context=None):
        pricelist_obj = self.pool.get('product.pricelist')
        domain = [('type', '=', 'sale'),('active', '=', True)]
        pricelist_ids = pricelist_obj.search(cr, uid, domain, order='id')
        if pricelist_ids:
            return pricelist_ids[0]
        else:
            return False
    
    _defaults = {
        'landscape': False,
        'amount_currency': True,
        'sortby': 'sort_name',
        'valid_to': lambda *a:(datetime.now() + timedelta(days=(15))).strftime('%Y-%m-%d'),
        'product_pricelist_id': _pricelist_get
    }

    def onchange_fiscalyear(self, cr, uid, ids, fiscalyear=False, context=None):
        res = {}
        if not fiscalyear:
            res['value'] = {'initial_balance': False}
        return res

    def _print_report(self, cr, uid, ids, data, context=None):
        #>>> riallineare immagini x catalogo
        #pt_obj = self.pool['product.template']
        #domain = [('image','!=', False)]
        #pt_ids = pt_obj.search(cr, uid, domain)
        #if pt_ids:
        #    pt_obj.write(cr, uid, pt_ids, {'description_purchase': 'x'})
        #    pt_obj.write(cr, uid, pt_ids, {'description_purchase': ''})
        #<<<<
        
        if context is None:
            context = {}
        data = self.pre_print_report(cr, uid, ids, data, context=context)
        #data['form'].update(self.read(cr, uid, ids, ['display_account','landscape',  'initial_balance', 'amount_currency', 'sortby'])[0])
        data['form'].update(self.read(cr, uid, ids, ['landscape', 'amount_currency', 'sortby'])[0])
        #if not data['form']['fiscalyear_id']:# GTK client problem onchange does not consider in save record
        #    data['form'].update({'initial_balance': False})
        data['name'] = 'Listino'
        
        if data['form']['landscape'] is False:
            data['form'].pop('landscape')
        else:
            context['landscape'] = data['form']['landscape']

        return self.pool['report'].get_action(cr, uid, [], 'of_sale_catalog_report.report_salecatalogbase', data=data, context=context)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
