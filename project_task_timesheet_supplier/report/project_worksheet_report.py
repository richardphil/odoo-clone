# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (alessndrocamilli@openforce.it)
#
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, osv
from openerp import tools


class report_project_worksheet_project(osv.osv):
    _name = "report.project.worksheet"
    _description = "Worksheet by project"
    _auto = False
    _columns = {
        'nbr': fields.integer('# of Tasks', readonly=True),
        'date': fields.date('Date', readonly=True),
        'type': fields.selection([('material', 'Material'), 
                                  ('human', 'Human')], 
                                 string='Type', readonly=True),
        'product_id': fields.many2one('product.product', 'Product', 
                                      readonly=True),
        'description': fields.char('Description', readonly=True),
        'quantity': fields.float('Quantity', readonly=True),
        'uom_id': fields.many2one('product.uom', 'Uom', readonly=True),
        'price_supplier': fields.float('Price Supplier', readonly=True),
        'amount_costs': fields.float('Amount Costs', readonly=True),
        'list_price': fields.float('Price', readonly=True),
        'amount_to_invoice': fields.float('Amount To Invoice', readonly=True),
        'amount_mark_up': fields.float('Amount Mark Up', readonly=True),
        'mark_up': fields.float('Mark Up', readonly=True),
        'name': fields.char('Task Summary', readonly=True),
        'user_id': fields.many2one('res.users', 'Assigned To', readonly=True),
        'project_id': fields.many2one('project.project', 'Project', 
                                      readonly=True),
        'partner_id': fields.many2one('res.partner', 'Partner', readonly=True),
        'country_id': fields.many2one('res.country', 'Country', readonly=True),
        'stage_id': fields.many2one('project.task.type', 'Stage', 
                                    readonly=True),
        'date_deadline': fields.date('Deadline', readonly=True),
        
        'partner_id': fields.many2one('res.partner', 'Partner', readonly=True),
        'issue_ticket_ref': fields.char('Issue Ticket Ref', readonly=True),
        'intervention_type_id': fields.many2one('project.intervention.type', 
                                                'Intervention Type', 
                                                readonly=True),
        'supplier_invoice_receipt': fields.char('Supplier Invoice Receipt', 
                                                readonly=True),
        'quote_amount_costs': fields.float('Quote Amount Costs', 
                                           readonly=True),
        'quote_amount_to_invoice': fields.float('Quote Amount To Invoice', 
                                                readonly=True),
        'quote_amount_mark_up': fields.float('Quote Amount Mark Up', 
                                             readonly=True),
    }
    _order = 'project_id'
    
    def init(self, cr):
        tools.sql.drop_view_if_exists(cr, 'report_project_worksheet')
        cr.execute("""
            CREATE view report_project_worksheet as
              SELECT
                    (select 1 ) AS nbr,
                    ws.id as id,
                    ws.date as date,
                    ws.type as type,
                    ws.product_id as product_id,
                    ws.description as description,
                    ws.quantity as quantity,
                    ws.uom_id as uom_id,
                    ws.price_supplier as price_supplier,
                    /* Quote */
                    CASE WHEN ws.is_quote is true 
                        THEN ws.subtotal_supplier
                        ELSE 0
                        END as quote_amount_costs,
                    CASE WHEN ws.is_quote is true 
                        THEN ws.subtotal
                        ELSE 0
                        END as quote_amount_to_invoice,
                    CASE WHEN ws.is_quote is true 
                        THEN ws.amount_mark_up
                        ELSE 0
                        END as quote_amount_mark_up,
                    /* NO quote */
                    CASE WHEN ws.is_quote is not true 
                        THEN ws.subtotal_supplier
                        ELSE 0
                        END as amount_costs,
                    CASE WHEN ws.is_quote is not true 
                        THEN ws.subtotal
                        ELSE 0
                        END as amount_to_invoice,
                    CASE WHEN ws.is_quote is not true 
                        THEN ws.amount_mark_up
                        ELSE 0
                        END as amount_mark_up,
                    ws.list_price as list_price,          
                    ws.to_invoice as to_invoice,
                    CASE WHEN SUM(ws.subtotal_supplier) > 0 
                        THEN round(SUM(ws.subtotal) / SUM(ws.subtotal_supplier), 2)
                        ELSE 1
                        END as mark_up,
                    t.name as name,
                    t.user_id as user_id,
                    t.project_id as project_id,
                    t.partner_id as partner_id,
                    p.country_id as country_id,
                    t.date_deadline as date_deadline,
                    t.issue_ticket_ref as issue_ticket_ref,
                    t.intervention_type_id as intervention_type_id,
                    t.stage_id as stage_id,
                    t.supplier_invoice_receipt as supplier_invoice_receipt
                    
              FROM project_task_work_supplier ws
                LEFT JOIN project_task t on t.id = ws.task_id
                LEFT JOIN res_partner p on p.id = t.partner_id
                WHERE t.active = 'true'
                GROUP BY
                    ws.id,
                    ws.date,
                    ws.type,
                    ws.product_id,
                    ws.description,
                    ws.quantity,
                    ws.uom_id,
                    ws.price_supplier,
                    ws.subtotal_supplier,
                    ws.list_price,
                    ws.subtotal,
                    ws.to_invoice,
                    ws.amount_mark_up,
                    t.name,
                    t.user_id,
                    t.project_id,
                    t.partner_id,
                    p.country_id,
                    t.date_deadline,
                    t.issue_ticket_ref,
                    t.intervention_type_id,
                    t.stage_id,
                    t.supplier_invoice_receipt
        """)