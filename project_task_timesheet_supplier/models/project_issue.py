# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#            Stefano Del Gobbo Acciarretti
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _
from openerp.exceptions import ValidationError


class project_issue(models.Model):
    _inherit = 'project.issue'
    
    @api.one
    @api.depends('task_ids.amount_costs', 
                 'task_ids.amount_to_invoice',
                 'task_ids.amount_total',
                 'task_ids.amount_markup',
                 'task_ids.date_end')
    def _amount_total_invoice(self):
        # Stage from to show values
        sequence_from = False
        domain = [('update_date_end', '=', True)]
        type = self.env['project.task.type'].search(domain, order='id')
        if type:
            sequence_from = type.sequence
        # Compute values
        digit = self.env['decimal.precision'].precision_get('Account')
        amount_costs = 0
        amount_to_invoice = 0
        amount_total = 0
        amount_markup = 0
        if sequence_from and (sequence_from < self.stage_id.sequence):
            for task in self.task_ids:
                amount_costs += task.amount_costs
                amount_to_invoice += task.amount_to_invoice
                amount_total += task.amount_total
                amount_markup += task.amount_markup
        self.amount_costs = round(amount_costs, digit)
        self.amount_to_invoice = round(amount_to_invoice, digit)
        self.amount_total = round(amount_total, digit)
        self.amount_markup = round(amount_markup, digit)

    amount_costs = fields.Float(string='Amount Costs', store=True, 
                                   readonly=True, 
                                   compute='_amount_total_invoice')
    amount_to_invoice = fields.Float(string='Amount',store=True, 
                                     readonly=True, 
                                     compute='_amount_total_invoice')
    amount_total = fields.Float(string='Amount Total',store=True,
                                     readonly=True,
                                     compute='_amount_total_invoice')
    amount_markup = fields.Float(string='Amount Mark Up', store=True,
                                 readonly=True, 
                                 compute='_amount_total_invoice')
   
    @api.multi 
    def recompute_all_invoicing_values(self):
        '''
        Recompute all values from tasks
        '''
        for issue in self.search([]):
            if issue.task_ids:
                issue._amount_total_invoice()
        
        return True