# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (alessndrocamilli@openforce.it)
#            Walter Antolini (walterantolini@openforce.it)
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import ValidationError


class project_task_work_supplier(models.Model):
    _name = 'project.task.work.supplier'

    task_id = fields.Many2one('project.task', string='Task', 
                              ondelete='cascade')
    sequence = fields.Integer(string='Sequence')
    date = fields.Date(string='Date', required=True)
    type = fields.Selection(selection=[('material', 'Material'),
                                       ('human', 'Human'),
                                       ('misc', 'Sum Lump')],
                            string="Type", required=True)
    product_id = fields.Many2one('product.product', string='Product',
                                 required=True)
    description = fields.Char(string='Description')
    quantity = fields.Float(string='quantity', 
                            digits=dp.get_precision('Product UoS'))
    uom_id = fields.Many2one('product.uom', string='Uom')
    price_supplier = fields.Float(string='Price Supplier', 
                            digits=dp.get_precision('Product Price'))
    subtotal_supplier = fields.Float(string='Subtotal Supplier', store=True, 
                                     compute='_compute_subtotal_supplier', 
                                     digits=dp.get_precision('Account'))
    list_price = fields.Float(string='Price', 
                            digits=dp.get_precision('Product Price'))
    subtotal = fields.Float(string='Subtotal', store=True,
                            digits=dp.get_precision('Account'), 
                            compute='_compute_subtotal')
    to_invoice = fields.Boolean(string='To Invoice', default=True)
    product_to_invoice_id = fields.Many2one('product.product', string='Product')
    description_to_invoice = fields.Char(string='Description To invoice')
    mark_up = fields.Float(string='Mark Up', digits=dp.get_precision('Account'),
                           store=True, 
                           compute='_compute_mark_up')
    amount_mark_up = fields.Float(string='Amount Mark Up', 
                                  digits=dp.get_precision('Account'), 
                                  store=True, 
                                  compute='_compute_mark_up')
    is_quote = fields.Boolean(string='Quote', default=False)

    @api.one
    @api.depends('product_id', 'quantity', 'price_supplier')
    def _compute_subtotal_supplier(self):
        self.subtotal_supplier = self.price_supplier * self.quantity

    @api.one
    @api.depends('product_id', 'quantity', 'list_price')
    def _compute_subtotal(self):
        self.subtotal = self.list_price * self.quantity
    
    @api.one
    @api.depends('subtotal_supplier', 'subtotal')
    def _compute_mark_up(self):
        if not self.subtotal_supplier or not self.subtotal:
            self.mark_up = 0
        else:
            self.mark_up = round(self.subtotal / self.subtotal_supplier, 2)
        self.amount_mark_up = round(self.subtotal - self.subtotal_supplier, 2)
            
    @api.one
    def _compute_values_from_mark_up(self, mark_up):
        self.list_price = self.price_supplier * mark_up

    @api.one
    @api.onchange('product_id')
    def change_product_id(self):
        self.description = self.product_id.product_tmpl_id.name
        self.uom_id = self.product_id.product_tmpl_id.uom_id.id
        self.quantity = 1
        
    def _get_price(self, cr, uid, pricelist_id, product_id, qty, 
                            partner_id, uom_id, date):
        pricelist = self.pool['product.pricelist']
        if not pricelist_id or not product_id or not partner_id \
            or not uom_id or not date:
            return False
        price = pricelist.price_get(cr, uid, [pricelist_id], product_id, 
                                    qty or 1.0, partner_id or False, 
                                    {'uom': uom_id, 'date': date})\
                                    [pricelist_id]
        return price
        
    @api.onchange('product_id', 'quantity')
    def compute_price_supplier(self):
        '''
        Compute The price from pricelist supplier.
        The supplier is the partner on the user_id of the task
        '''
        price_supplier = 0
        if self.product_id:
            # Controls
            if not self.task_id.user_id:
                raise ValidationError(_('Missing User assing to'))
            
            supplier = self.task_id.user_id.commercial_partner_id or \
                self.task_id.user_id.partner_id or False
            pricelist = supplier.property_product_pricelist_purchase
            if not supplier:
                raise ValidationError(_('User assined not linked to a valid \
                partner'))
            # Compute
            price = self._get_price(pricelist.id, self.product_id.id, 
                                    self.quantity or 1.0, supplier.id or False, 
                                    self.product_id.uom_id.id, self.date)
            price_supplier = price
        self.price_supplier = price_supplier
        
    @api.onchange('product_id', 'quantity', 'to_invoice')
    def compute_price_to_invoice(self):
        '''
        Compute The price from pricelist customer.
        It will be token first the pricelist of analytic account of the project
        '''
        price_to_invoice = 0
        if not self.to_invoice:
            price_to_invoice = 0
        if self.product_id:
            # Controls
            if not self.task_id.project_id.analytic_account_id:
                raise ValidationError(_('Project without contract!'))
            if not self.task_id.project_id.analytic_account_id.partner_id:
                raise ValidationError(_('Contract without partner!'))
            partner = self.task_id.project_id.analytic_account_id.partner_id
            pricelist = partner.property_product_pricelist
            # Compute
            if not self.to_invoice:
                price_to_invoice = 0
            else:
                price = self._get_price(pricelist.id, self.product_id.id, 
                                        self.quantity or 1.0, 
                                        partner.id or False, 
                                        self.product_id.uom_id.id, 
                                        self.date)
                price_to_invoice = price
        self.list_price = price_to_invoice


class project_task(models.Model):
    """
    Il nome è il nome del model che andremo a fare un'ovveride, aggiungendo la nostra relazione e fields
    """
    _inherit = 'project.task'
    
    @api.one
    @api.depends('work_supplier_ids.subtotal_supplier', 
                 'work_supplier_ids.subtotal')
    def _amount_total_invoice(self):
        digit = self.env['decimal.precision'].precision_get('Account')
        subtotal_costs = 0
        subtotal_to_invoice = 0
        subtotal = 0
        markup = markup_line = 0
        for line in self.work_supplier_ids:
            # Cost Supplier
            subtotal_costs += line.subtotal_supplier
            # Amount to Invoice
            if line.to_invoice:
                subtotal_to_invoice += line.subtotal
            # Amount Total
            subtotal += line.subtotal
            # Markup
            markup_line = (line.subtotal - line.subtotal_supplier)
            markup_line = round(markup_line, digit) 
            markup += markup_line
        self.amount_costs = round(subtotal_costs, digit)
        self.amount_to_invoice = round(subtotal_to_invoice, digit)
        self.amount_total = round(subtotal, digit)
        self.amount_markup = round(markup, digit)

    work_supplier_ids = fields.One2many('project.task.work.supplier',
                                        'task_id', string='Works Supplier')
    amount_costs = fields.Float(string='Amount Costs', store=True, 
                                   readonly=True, 
                                   compute='_amount_total_invoice')
    amount_to_invoice = fields.Float(string='Amount To Invoice',store=True, 
                                     readonly=True, 
                                     compute='_amount_total_invoice')
    amount_total = fields.Float(string='Amount Total',store=True,
                                     readonly=True,
                                     compute='_amount_total_invoice')
    amount_markup = fields.Float(string='Amount Mark Up', store=True,
                                 readonly=True, 
                                 compute='_amount_total_invoice')
    supplier_invoice_receipt = fields.Boolean(string="Supplier Invoice Receipt")
    
    @api.multi
    def action_worksheet_quote_sent(self):
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        # template = self.env.ref('account.email_template_edi_invoice', False)
        template = self.env.ref(
            'project_task_timesheet_supplier.email_template_worksheet_quote', 
            False)
        compose_form = self.env.ref('mail.email_compose_message_wizard_form', False)
        ctx = dict(
            default_model='project.task',
            default_res_id=self.id,
            default_use_template=bool(template),
            default_template_id=template.id,
            default_composition_mode='comment',
            mark_invoice_as_sent=True,
        )
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': ctx,
        }
    
    
