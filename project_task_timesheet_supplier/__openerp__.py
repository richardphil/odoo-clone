# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (alessndrocamilli@openforce.it)
#            Walter Antolini (walterantolini@openforce.it)
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro - www.openforce.it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': "Project Task Timesheet Supplier",

    'summary': """
        Manage the Timesheet of supplier for the task""",

    'description': """
        In the Task it's possible to add lines of products(service or materials)
        used from the supplier
    """,

    'author': "Openforce di Camilli Alessandro",
    'website': "http://www.openforce.it",
    'category': 'Project',
    'version': '0.1',
    "depends" : ['project', 'purchase', 'account', 'mail', 
                 'project_issue_task',
                 'project_intervention_type',
                 'project_task_invoice_from_company'],
    'data': [
        'wizard/compute_from_markup_view.xml',
        'security/ir.model.access.csv',
        'security/security.xml',
        'views/project.xml',
        'views/project_issue.xml',
        'views/report_worksheet.xml',
        'report/project_report_view.xml',
        'report/project_worksheet_report_view.xml',
        'worksheet_report.xml',
        'data/email_template.xml',
    ],
    'demo': [
    ],
}