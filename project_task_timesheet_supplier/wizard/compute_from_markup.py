# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2013 Alessandro Camilli
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _


class wizard_compute_from_mark_up(models.TransientModel):
    _name = "wizard.pt.supplier.compute.from.mark.up"
    _description = "Wizard to recompute invoice values from mark up"

    mark_up = fields.Float(string='Mark Up', required=True)

    @api.multi
    def compute_markup(self):
        for wiz_obj in self:
            line_ids = self.env.context.get('active_ids')
            for line in self.env['project.task.work.supplier'].browse(line_ids):
                line._compute_values_from_mark_up(wiz_obj.mark_up)
        
        return {'type': 'ir.actions.act_window_close'}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
