# -*- coding: utf-8 -*-
##############################################################################
#    
#    Copyright (C) 2013 Alessandro Camilli 
#    (<http://www.openforce.it>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api
from openerp.tools.translate import _

class project_issue(models.Model):
    _inherit="project.issue"
    
    def _get_default_partner(self):
        partner = False
        user = self.env['res.users'].browse(
                                     self.env.uid)
        partner = user.partner_id
        if user.partner_id.parent_id:
            partner = user.partner_id.parent_id 
        return partner
    
    def _get_default_project(self):
        domain = []
        if self.partner_id.parent_id:
            domain.append(('partner_id', '=', self.partner_id.parent_id.id))
        elif self.partner_id:
            domain.append(('partner_id', '=', self.partner_id.id))
        else:
            partner = self._get_default_partner()
            domain.append(('partner_id', '=', partner.id))
            
        project_ids = self.env['project.project'].sudo().search(domain)
        return project_ids
    
    partner_id = fields.Many2one('res.partner', 'Contact', default=_get_default_partner)
    project_id = fields.Many2one('project.project', 'Project', default=_get_default_project)
    