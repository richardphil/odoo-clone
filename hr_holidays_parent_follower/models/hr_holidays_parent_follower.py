# -*- coding: utf-8 -*-
#
##############################################################################
#
#    Author(s): Alessandro Camilli (alessandrocamilli@openforce.it)
#               Andrea Colangelo (andreacolangelo@openforce.it)
#
#    Copyright © 2016 Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see:
#    http://www.gnu.org/licenses/agpl-3.0.txt.
#
##############################################################################

from openerp.osv import orm
from openerp.tools.translate import _


class HRHolidaysParentFollower(orm.Model):
    _inherit = "hr.holidays"

    def add_follower(self, cr, uid, ids, employee_id, context=None):
        employee = self.pool['hr.employee'].browse(cr, uid, employee_id,
                                                   context=context)
        if employee.user_id:
            self.message_subscribe(cr, uid, ids,
                                   [employee.user_id.partner_id.id],
                                   context=context)

    def create(self, cr, uid, values, context=None):
        hr_holiday_id = super(HRHolidaysParentFollower,
                              self).create(cr, uid, values, context=None)

        employee_id = values.get('employee_id', False)
        employee = self.pool("hr.employee").browse(cr, uid, employee_id,
                                                   context=context)
        department_id = employee.department_id.id

        if not department_id:
            raise orm.except_orm(_('Missing departament!'),
                                 _("Please, add the employee to a department "
                                   "before requesting an holiday leave."))

        department = self.pool("hr.department").browse(cr, uid, department_id,
                                                       context=context)
        while department.parent_id:
            self.add_follower(cr, uid, [hr_holiday_id],
                              department.parent_id.manager_id.id,
                              context=context)
            department = department.parent_id

        self.add_follower(cr, uid, [hr_holiday_id],
                          department.manager_id.id,
                          context=context)

        self.add_follower(cr, uid, [hr_holiday_id],
                          employee.id,
                          context=context)

        return hr_holiday_id
