# -*- coding: utf-8 -*-
##############################################################################
#    
#    Author: Alessandro Camilli (a.camilli@openforce.it)
#    Copyright (C) 2015
#    Openforce di Camilli Alessandro (www.openforce.it)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import orm, fields
from openerp.tools.translate import _
from datetime import datetime, time, timedelta

class wizard_account_move_validate_multiple(orm.TransientModel):
    
    _name = "wizard.account.move.validate.multiple"
    
    _columns = {
    }
    
    _defaults = {
    }

    def execute(self, cr, uid, ids, context=None):
        account_move_obj = self.pool['account.move']
        move_ids = context.get('active_ids')
        moves_to_validate = []
        # Selection moves
        if move_ids:
            for move in account_move_obj.browse(cr, uid, move_ids):
                if not move.state == 'draft':
                    continue  
                moves_to_validate.append(move.id)
        # Validation
        if moves_to_validate:
            account_move_obj.button_validate(cr, uid, moves_to_validate)
        
        return {
            'type': 'ir.actions.act_window_close',
        }

